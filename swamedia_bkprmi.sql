-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 23, 2019 at 11:09 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swamedia_bkprmi`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_banner`
--

CREATE TABLE `t_banner` (
  `id` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `foto` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL COMMENT 'kalau ada http / https ngambil dari luar, kalau tidak ada nganbil dari nama controller nya',
  `tanggal` datetime NOT NULL,
  `publish` int(1) NOT NULL COMMENT '0=Tidak Aktif, 1=Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_banner`
--

INSERT INTO `t_banner` (`id`, `judul`, `foto`, `url`, `tanggal`, `publish`) VALUES
(3, 'exoclick', 'foto-20190515135244.gif', 'http://www.exoclick.com/?login=nahkodanews', '2019-05-15 13:52:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_komentar`
--

CREATE TABLE `t_komentar` (
  `id` int(11) NOT NULL,
  `konten_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `komentar` varchar(500) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_komentar`
--

INSERT INTO `t_komentar` (`id`, `konten_id`, `parent_id`, `nama`, `email`, `website`, `komentar`, `tanggal`) VALUES
(1, 4, 0, 'asep', 'asep@gmail.com', '', 'Keren\r\n\r\nPDI Cepet mati', '2019-05-21 07:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `t_kontak`
--

CREATE TABLE `t_kontak` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `pesan` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_konten`
--

CREATE TABLE `t_konten` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `sub_menu1_id` int(11) DEFAULT NULL,
  `sub_menu2_id` int(11) DEFAULT NULL,
  `judul` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `tanggal` datetime NOT NULL,
  `deskripsi` longtext NOT NULL,
  `foto` varchar(150) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  `keterangan_foto` varchar(100) DEFAULT NULL,
  `dibaca` int(11) NOT NULL DEFAULT '0',
  `publish` int(1) NOT NULL COMMENT '0=not publish, 1=publish'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_konten`
--

INSERT INTO `t_konten` (`id`, `user_id`, `menu_id`, `sub_menu1_id`, `sub_menu2_id`, `judul`, `slug`, `tanggal`, `deskripsi`, `foto`, `video`, `keterangan_foto`, `dibaca`, `publish`) VALUES
(1, 1, 2, 3, NULL, 'Sejarah BKPRMI', 'sejarah-bkprmi', '2019-05-07 11:32:40', '<p xss=removed><img class=\" wp-image-22 alignleft\" xss=removed src=\"https://bkprmingaglik.files.wordpress.com/2016/11/bkprmi1.gif?w=210&h=211\" alt=\"bkprmi1\" width=\"150\" height=\"151\" data-attachment-id=\"22\" data-permalink=\"https://bkprmingaglik.wordpress.com/sejarah/bkprmi1/\" data-orig-file=\"https://bkprmingaglik.files.wordpress.com/2016/11/bkprmi1.gif?w=210&h=211\" data-orig-size=\"993,1001\" data-comments-opened=\"1\" data-image-meta=\"{\" data-image-title=\"bkprmi1\" data-image-description=\"\" data-medium-file=\"https://bkprmingaglik.files.wordpress.com/2016/11/bkprmi1.gif?w=210&h=211?w=298\" data-large-file=\"https://bkprmingaglik.files.wordpress.com/2016/11/bkprmi1.gif?w=210&h=211?w=840\">Badan Komunikasi Pemuda Masjid Indonesia (BKPMI) berdiri pada tanggal 3 September 1977 19 Ramadhan 1397 Hijriyah di Masjid Istiqamah Bandung, Jawa Barat. Dengan terbentuknya kepengurusan periode 1977 – 1980 hasil Musyawarh Kerja Nasional dan dilantik oleh KH. EZ Muttaqien mewakili Ketua Umum Majelis Ulama Indonesia Pusat. Lahirnya BKPMI ini adalah pada forum Musyawarah Kerja Nasional I yang kemudian disepakati sebagai Musyawarah Nasional I yang dihadiri oleh BKPM wilayah dengan kepemimpinan model Presidium dan terpilih sebagai ketua umum Rakanda Toto Tasmara dengan Sekertaris Umum Rakanda Bambang Pranggono. Tercatat sebagai pendiri adalah : Rakanda Toto Tasmara, Rakanda Ahmad Mansur Suryanegara, Rakanda Syamsuddin Manaf, Rakanda Bambang Pranggono, masing-masing dari Jawa Barat, Rakanda Mustafid Amna, Rakanda Syaifuddin Donondjoyo, Rakanda Muhammad Anwar Ratnapa Syaifuddin Donondjoyo, Rakanda Muhammad Anwar Ratnaprawira, Rakanda Muchlis Ma’ruf masing-masing dari DKI Jakarta, Rakanda Nasir Budiman, Nurcholis Turmudzi masing-masing dari Jawa Tengah, Rakanda Mubayin dari Jawa Timur.</p>\r\n<p xss=removed>Pembentukannya dilatar belakangi sebagai berikut :</p>\r\n<ul xss=removed>\r\n<li>Sebagai reaksi terhadap gejala sosial yang berkembang di tanah air seperti konsep pembangunan nasional yang dinilai cenderung berorientasi pada pembentukan masyarakat sekuler, depolitisasi organisasi kepemudaan melalui konsep NKK dan BKK, isu kristenisasi dan pemahaman keagamaan berlangsung secara dinamis yang menimbulkan polemik antara paham tradisional dan paham modernis.</li>\r\n<li xss=removed>Isu kebangkitan Islam Abad XV Hijriyah yang ditandai dengan kesemarakan kegiatan keagamaan, pencerahan pemahaman keagamaan melalui kajian-kajian dalam berbagai bentuknya, kuatnya dorongan untuk membangun Ukhuwah Islamiyah dan negara</li>\r\n</ul>\r\n<p xss=removed>Tumbuhnya kesadaran beragama di kalangan muda Islam telah mendorong untuk mempelajari sekaligus untuk memperjuangkan Islam sebagai sebuah kebenaran mutlak.</p>\r\n<ul xss=removed>\r\n<li>Tumbuh kembangnya kajian-kajian Islam]] di berbagai belahan dunia di satu sisi dan di sisi lain semakin kuatnya semangat Generasi Muda Islam Indonesia untuk memantapkan posisi dan citra Indonesia tidak hanya sebagai pemeluk Islam terbesar di dunia, tetapi luk Islam terbesar di dunia, tetapi juga sebagai pusat syiar dan peradaban Islam</li>\r\n<li>Munculnya gerakan ummat Islam di seluruh dunia untuk kembali ke Masjid sebagai basis perjuangannya, di mana Masjid sebagai Lembaga dan Pranata, Masjid sebagai Baitullah dan Masjid sebagai milik Ummat, memberikan nuansa dan marwah BKPRMI sebagai alat perekat/katalisator Pemuda Remaja Islam, Ideologi dan emosi keagamaan sebagai motivasi instrinsik dalam memacu semangat juang “Tahan Banting”. independen dan sebagai Kader Ummat dan sekaligus sebagai Kader Bangsa</li>\r\n</ul>\r\n<p xss=removed>Rapat pembentukan dan pelantikan pengurus BKPMI periode I itu di lakukan di Masjid Istiqomah Bandung. Pada saat pelantikan pengurus tersebut, hadir beberapa tokoh pemuda Masjid dari Jakarta, Yogyakarta, dan Semarang.</p>\r\n<p xss=removed>Mengingat Pengurus Periode I ini berkedudukan di Bandung, maka Sekretariat BKPMI pertama kali terletak di Bandung, yakni di Gedung Sekretariat Majelis Ulama Indonesia, Jawa Barat. Kemudian berpindah mengikuti sekretariat MUI Pusat. Tahun 1986 di Masjid AL-Azhar, Jakarta , dan mulai tahun 1989 sampai sekarang di Masjid Istiqlal.</p>\r\n<p xss=removed><strong>Prestasi</strong><br>Salah satu prestasi BKPMI adalah di canangkannya pembentukan Taman Kanak-kanak Al-Qur’an (TKA) sebagai program nasional BKPMI dalam Musyawarah Nasional V BKPMI di Masjid Al-Falah Surabaya tahun 1989. Dalam MUNAS V ini, hadir memberi pengarahan beberapa pejabat tinggi negara, seperti Menteri Agama (Prof. DR. H. Munawir Sadzali) dan Menteri Penerangan (H. Harmoko). Program TKA ini kemudian dilanjutkan dengan pembentukan Lembaga Pembinaan dan pengembangan TKA (LPPTKA) BKPMI dalam rapat pleno DPP BKPMI di Jakarta.</p>\r\n<p xss=removed><strong>Dari BKPMI Ke BKPRMI</strong><br>Perubahan dari Badan Komunikasi Pemuda Masjid Indonesia (BKPMI) ke Badan Komunikasi Pemuda Remaja Masjid Indonesia (BKPRMI) dilakukan dalam Musyawarah Nasional VI tahun 1993 di Asrama Haji Pondok Gede, Jakarta, bersamaan dengan bergabungnya Forum Silaturahmi Remaja Masjid (FOSIRAMA) di bawah pimpinan DR. H. Idrus Marham, M.A. (Ketua Umum DPP BKPRMI yang lalu).</p>\r\n<p xss=removed>Bersamaan dengan perubahan nama organisasi, dalam MUNAS VI ini pula di sepakati, bahwa BKPRMI merupakan lembaga otonom dari organisasi Dewan Mesjid Indonesia (DMI). Selain itu, di bawah pengurus BKPRMI terbentuk beberapa Lembaga Pembinaan dan Pengembangan, seperti Da’wah dan Pengkajian Islam (LPP-DPI), Sumber Daya Manusia (LPP-SDM), Ekonomi Koperasi (LPP-EKOP), Dan Keluarga Sejahtera (LPP-KS). Lembaga Pembinaan dan Pengembangan Ketahanan Santri (LKS), terbentuk dalam suatu rapat pleno DPP pasca MUNAS VI.</p>', NULL, NULL, NULL, 0, 1);
INSERT INTO `t_konten` (`id`, `user_id`, `menu_id`, `sub_menu1_id`, `sub_menu2_id`, `judul`, `slug`, `tanggal`, `deskripsi`, `foto`, `video`, `keterangan_foto`, `dibaca`, `publish`) VALUES
(2, 1, 2, 4, NULL, 'AD/ART BKPRMI', 'adart-bkprmi', '2019-05-07 16:29:27', '<h3>AD ART</h3>\r\n<br>\r\n<p align=\"center\"><strong>ANGGARAN </strong><strong>D</strong><strong>ASAR</strong><br>\r\n<strong>BADAN KOMUNIKASI PEMUDA MASJID INDONESIA</strong><br>\r\n<strong>HASIL MUSYAWARAH NASIONAL XII BKPRMI</strong><br>\r\n<strong>M</strong><strong>AKASSAR</strong><strong>, 21 – 23 F</strong><strong>EBRUARI </strong><strong>2014</strong></p>\r\n<p dir=\"RTL\">????? ???????? ???????? ?????????? ?????? ????????????? ??</p>\r\n<p><em>“Dan Aku tidak menciptakan Jin dan Manusia melainkan supaya mereka menyembah-Ku” (Q.S. 51: 56)</em></p>\r\n\r\n<p dir=\"RTL\">?????? ????????????? ?????? ???????? ??????????????? ???</p>\r\n<p><em>“Dan tidaklah kami mengutus kamu, melainkan untuk menjadi rahmat bagi semesta alam” (Q.S. 21: 107)</em></p>\r\n\r\n<p dir=\"RTL\">??????? ?????? ??????? ?????????? ????????? ??????????? ?????????????? ???????????? ???? ?????????? ????????????? ??????????</p>\r\n\r\n<p><em>“Kamu adalah umat yang terbaik yang dilahirkan untuk manusia, menyuruh kepada yang ma’ruf dan mencegah dari yang munkar dan beriman kepada Allah SWT” (Q.S. 3: 110)</em></p>\r\n\r\n<p dir=\"RTL\">?????? ???????? ??????? ??????? ?????? ????? ??????? ???????? ???????? ??????? ???????? ???? ?????????????? ??</p>\r\n<p><em>“Siapakah yang lebih baik perkataannya dari pada orang yang menyeru kepada Allah, mengerjakan amal saleh dan berkata sesungguhnya aku adalah bagian dari orang-orang muslim” (Q.S. 41: 33)</em></p>\r\n\r\n<p dir=\"RTL\">??? ???? ?????? ????????? ???????? ?????? ?????????? ???????? ???????? ????????? ???? ????????????? ???</p>\r\n<p><em>“Berapa banyak terjadi golongan yang sedikit mengalahkan golongan yang banyak dengan izin Allah, dan Allah beserta orang-orang yang sabar” (Q.S. 2: 249)</em></p>\r\n\r\n<p dir=\"RTL\">???????????? ????????? ?????????? ??? ????????? ??????? ??? ???????? ???????? ??????? ??????? ???????? ??????????? ???????????????? ????????? ????? ?????????????? ????????? ????? ????????????? ???????????? ??? ??????? ??????? ????? ?????????? ???????? ????????? ??????? ?????? ??????? ????????? ??? ????????? ????????? ??????? ??????? ??</p>\r\n\r\n<p><em>“Hai orang-orang yang beriman, barang siapa di antara kamu yang murtad dari agama-Nya, maka kelak Allah akan mendatangkan suatu kaum yang Allah mencintai mereka dan merekapun mencintai-Nya, yang bersikap lemah lembut terhadap orang beriman, yang bersikap keras terhadap orang-orang kafir, yang berjihad di jalan Allah dan yang tidak takut kepada celaan orang yang suka mencela. Itulah karunia Allah, diberikan-Nya kepada siapa yang dikehendaki-Nya, dan Allah Maha Luas (pemberian-Nya) lagi Maha Mengetahui” (Q.S. 5: 54)</em></p>\r\n\r\n<p dir=\"RTL\">?????? ????? ????? ???????? ??? ??????? ????? ?????? ?????????? ???????????? ???????? ????? ?????????? ?????? ???????????? ????????? ??????????? ???? ?????????? ???????? ????? ?????????????? ?????? ?????? ????? ?????????????? ??? ??????????? ????????? ??????? ?????????? ???????? ??? ???????? ?????????? ???????? ??? ??????? ??</p>\r\n<p><em>“Dia telah mensyari’atkan bagi kamu tentang agama apa yang telah diwasiatkan-Nya kepada Nuh dan apa yang telah kami wahyukan kepada kamu dan apa yang telah Kami wasiatkan kepada Ibrahim, dan janganlah kamu berpecah belah tentangnya. Amat berat bagi orang-orang musyrik agama yang kamu seru mereka kepadanya. Allah menarik kepada Agama itu orang yag dikehendaki-Nya dan memberi petunjuk kepada (agama-Nya) orang yang kembali (kepada-Nya) ” (Q.S. 42: 13)</em></p>\r\n\r\n<p dir=\"RTL\">???????? ???????? ????????? ??????? ???? ??????? ????????? ??????????? ????????? ????????? ??????????? ???????? ??????????? ?????? ?????? ?????? ???????? ????????? ???????????? ??? ?????????? ???? ?????????????? (?? )</p>\r\n<p><em>“Hanyalah yang memakmurkan masjid-masjid Allah ialah orang-orang yang beriman kepada Allah dan hari kemudian, serta tetap mendirikan shalat dan menunaikan zakat dan tidak takut (kepada siapapun) selain kepada Allah. Maka merekalah orang-orang yang diharapkan termasuk golongan orang-orang yang mendapat petunjuk.” (Q.S. 9: 18)</em></p>\r\n\r\n<p dir=\"RTL\">???????????? ????????? ?????????? ???? ?????????? ??? ??? ??????????? (?) ?????? ??????? ????? ??????? ??? ?????????? ??? ??? ???????????(? )</p>\r\n<p><em>“Hai orang-orang yang beriman, mengapa kamu mengatakan apa yang tidak perbuat ? Amat besar kebencian di sisi Allah bahwa kamu mengatakan apa-apa yang tiada kamu kerjakan” (Q.S. 61: 2-3)</em></p>\r\n\r\n<p><em>“Ada tujuh golongan manusia yang Allah akan menaungi mereka (di hari kiamat) yang tiada naungan kecuali hanya naungan-Nya, yaitu pemimpin yang adil, anak muda yang tumbuh/menjadi dewasa dalam keadaan selalu mengabdi kepada Allah SWT, seorang yang hatinya terpaut di masjid, dua orang yang kasih mengasihi karena Allah, seorang laki-laki yang dirayu oleh seorang perempuan yang berpangkat/bangsawan lagi pula cantik tetapi menolak dan berkata sungguh aku takut kepada Allah, seseorang yang bersedekah kemudian merahasiakannya seolah-olah tangan kirinya tiada mengetahui apa yang diinfaqkan oleh tangan kanannya itu, seseorang yang selalu ingat kepada Allah dikala berkhalwat/</em><em> </em><em>sendiri hingga kedua matanya mencucurkan air mata.” (H.R. Bukhori dan Muslim)</em></p>\r\n<p>Bahwa sesungguhnya Pemuda dan Remaja Masjid Indonesia adalah bagian dari potensi generasi muda yang bertanggung jawab terhadap masa depan Agama Islam, Bangsa dan Negara Kesatuan Republik Indonesia yang berdasarkan Pancasila.</p>\r\n<p>Bahwa sesungguhnya Pemuda Remaja Masjid menjadikan keimanan dan ketaqwaan kepada Allah SWT sebagai landasan spiritual dan akhlak dalam rangka menggerakkan dan mengendalikan pembangunan bangsa.</p>\r\n<p>Bahwa sesungguhnya Pemuda dan Remaja Masjid Indonesia menjadikan Masjid sebagai pusat ibadah, kebudayaan dan perjuangan untuk membina generasi muda menjadi kader bangsa yang bertaqwa kepada Allah SWT, memiliki wawasan ke-Islaman yang utuh dan istiqomah, dan berakhlak mulia serta memiliki citra sebagai <em>muwahhid, mujahhid, musaddid, muaddib </em>serta<em>mujaddid.</em></p>\r\n<p>Bahwa sesungguhnya keberadaan Pemuda dan Remaja Masjid merupakan bagian yang tak terpisahkan dari Gerakan Kemasjidan di Indonesia, dalam berkhidmat kepada pembangunan bangsa untuk mencapai masyarakat adil dan makmur, material dan spiritual, dalam ampunan Allah.</p>\r\n<p>Atas dasar amanah mulia tersebut di atas serta sadar akan tanggung jawab sebagai generasi penerus tugas dakwah Islam, maka Pemuda Remaja Masjid Indonesia dengan ini membentuk Badan Komunikasi Pemuda Remaja Masjid Indonesia dengan dasar sebagai berikut:</p>\r\n\r\n<p align=\"center\"><strong>BAB 1</strong></p>\r\n<p align=\"center\"><strong>NAMA, WAKTU DAN KEDUDUKAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 1</strong></p>\r\n<p align=\"center\"><strong>Nama</strong></p>\r\n<p>Organisasi ini bernama Badan Komunikasi Pemuda Remaja Masjid Indonesia disingkat BKPRMI</p>\r\n<p align=\"center\"><strong>Pasal 2</strong></p>\r\n<p align=\"center\"><strong>Waktu dan Tempat</strong></p>\r\n<p>BKPRMI adalah kelanjutan yang semula bernama Badan Komunikasi Pemuda Masjid Indonesia (BKPMI), didirikan pada tanggal 19 Ramadhan 1397 Hijriyah bertepatan dengan 3 September 1977 Miladiah di Masjid Istiqomah Bandung, untuk jangka waktu yang tidak ditentukan.</p>\r\n\r\n\r\n<p align=\"center\"><strong>Pasal 3</strong></p>\r\n<p align=\"center\"><strong>Kedudukan</strong></p>\r\n<p>Badan Komunikasi Pemuda Remaja Masjid Indonesia berkedudukan hukum di Ibu Kota Negara Republik Indonesia.</p>\r\n<p align=\"center\"><strong>BAB II</strong></p>\r\n<p align=\"center\"><strong>ASAS, STATUS DAN SIFAT</strong></p>\r\n<p align=\"center\"><strong>Pasal 4</strong></p>\r\n<p align=\"center\"><strong>Asas</strong></p>\r\n<p>BKPRMI berasaskan Islam.</p>\r\n\r\n<p align=\"center\"><strong>Pasal 5</strong></p>\r\n<p align=\"center\"><strong>Status</strong></p>\r\n<p>BKPRMI adalah Organisasi Dakwah dan Pendidikan bagi Pemuda Remaja Masjid di seluruh Indonesia yang berstatus kemasyarakatan, kepemudaan,  dan independen serta memiliki hubungan kemitraan da’wah dengan Dewan Masjid Indonesia</p>\r\n<p align=\"center\"><strong>Pasal 6</strong></p>\r\n<p align=\"center\"><strong>Sifat</strong></p>\r\n<p>1.    BKPRMI bersifat kemasyarakatan, kepemudaan, keumatan, kemasjidan, ke-Islaman dan ke-Indonesiaan.</p>\r\n<p>2.    BKPRMI sebagai wahana komunikasi dari organisasi pemuda dan remaja masjid untuk pengembangan program secara komunikatif, informatif, konsultatif dan koordinatif.</p>\r\n\r\n\r\n<p align=\"center\"><strong>BAB III</strong></p>\r\n<p align=\"center\"><strong>TUJUAN DAN USAHA</strong></p>\r\n<p align=\"center\"><strong>Pasal 7</strong></p>\r\n<p align=\"center\"><strong>Tujuan</strong></p>\r\n<p>BKPRMI bertujuan memberdayakan dan mengembangkan potensi<strong> </strong>Pemuda Remaja Masjid agar bertaqwa kepada Allah SWT, memiliki wawasan ke-Islaman dan ke-Indonesiaan yang utuh dan kokoh, serta senantiasa memakmurkan masjid sebagai pusat ibadah, perjuangan dan kebudayaan dengan tetap berpegang teguh kepada prinsip aqidah, ukhuwah dan dakwah Islamiyah untuk mewujudkan masyarakat <em>marhamah</em> dalam bingkai Negara Kesatuan Republik Indonesia.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>Pasal 8</strong></p>\r\n<p align=\"center\"><strong>Usaha</strong></p>\r\n<p>Untuk tercapainya tujuan BKPRMI melakukan usaha-usaha sebagai berikut:</p>\r\n<p>1.    Terus menerus meningkatkan upaya pengembangan minat, kemampuan dan pemahaman Al Qur’an bagi seluruh masyarakat, pemuda, remaja, dan anak-anak serta jamaah masjid. Mendorong tumbuhnya organisasi Masyarakat, Pemuda Remaja Masjid dan mengkokohkan komunikasi di kalangan Masyarakat, Pemuda Remaja Masjid dalam rangka mengembangkan program dan gerakan dakwah Islam.</p>\r\n<p>2.    Meningkatkan kualitas masyarakat dan prestasi generasi muda bangsa melalui pendekatan keagamaan, kependidikan, kebudayaan dan ilmu pengetahuan sebagai wujud partisipasi dalam pembangunan bangsa.</p>\r\n<p>3.    Memantapkan wawasan ke-Islaman dan ke-Indonesiaan serta kesadaran Pemuda Remaja Masjid tentang cita-cita perjuangan bangsa, bela negara dan dakwah Islamiyah dalam arti luas.</p>\r\n<p>4.    Membina dan mengembangkan kemampuan manajemen dan kepemimpinan Pemuda Remaja Masjid yang berorientasi kepada kemasjidan, keumatan dan ke-Indonesiaan.</p>\r\n<p>5.    Meningkatkan Kesejahteraan dan kemampuan kewirausahaan pemuda dan remaja masjid melalui peningkatan ekonomi umat.</p>\r\n<p>6.    Meningkatkan hubungan dan kerjasama dengan pemerintah, organisasi keagamaan, kemasyarakatan, kepemudaan dan profesi lainnya, baik di tingkat nasional maupun internasional.</p>\r\n<p>7.    Usaha lain yang tidak bertentangan dengan ruh dan tujuan organisasi.</p>\r\n\r\n<p align=\"center\"><strong>BAB IV</strong></p>\r\n<p align=\"center\"><strong>KEANGGOTAAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 9</strong></p>\r\n<p align=\"center\"><strong>Keanggotaan</strong></p>\r\n<p>1.    Anggota BKPRMI terdiri atas:</p>\r\n<p>a.     Anggota Biasa</p>\r\n<p>b.    Anggota Fungsional</p>\r\n<p>c.     Anggota Kehormatan</p>\r\n<p>2.    Setiap Remaja dan Pemuda Islam Indonesia yang berusia minimal 15 tahun dan menyetujui Anggaran Dasar dan Anggaran Rumah Tangga Badan Komunikasi Pemuda Remaja Masjid Indonesia dapat diterima menjadi anggota BKPRMI.</p>\r\n<p>3.    Mekanisme keanggotaan diatur dalam Anggaran Rumah Tangga BKPRMI.</p>\r\n\r\n<p align=\"center\"><strong>Pasal 10</strong></p>\r\n<p align=\"center\"><strong>Kewajiban dan Hak Anggota</strong></p>\r\n<p>Kewajiban dan Hak anggota diatur dalam Anggaran Rumah Tangga BKPRMI.</p>\r\n\r\n<p align=\"center\"><strong>BAB V</strong></p>\r\n<p align=\"center\"><strong>STRUKTUR DAN TATA KERJA ORGANISASI</strong></p>\r\n<p align=\"center\"><strong>Pasal 11</strong></p>\r\n<p align=\"center\"><strong>Struktur Organisasi</strong></p>\r\n<p>1.    Di Tingkat Nasional Organisasi ini disebut Dewan Pengurus Pusat Badan Komunikasi Pemuda Remaja Masjid Indonesia yang disingkat DPP BKPRMI dan berkedudukan di Ibukota Negara.</p>\r\n<p>2.    Di Tingkat Propinsi organisasi ini disebut Dewan Pegurus Wilayah Badan Komunikasi Pemuda Remaja Masjid Indonesia yang disingkat DPW BKPRMI dan berkedudukan di Ibukota Propinsi.</p>\r\n<p>3.    Di Tingkat Kabupaten dan Kota organisasi ini disebut Dewan Pengurus Daerah Badan Komunikasi Pemuda Remaja Masjid Indonesia yang disingkat DPD BKPRMI dan berkedudukan di Ibukota Kabupaten atau Kota.</p>\r\n<p>4.    Di Tingkat Kecamatan organisasi ini disebut Dewan Pengurus Kecamatan Badan Komunikasi Pemuda Remaja Masjid Indonesia yang disingkat DPK BKPRMI dan berkedudukan di Ibukota Kecamatan.</p>\r\n<p>5.    Di Tingkat Kelurahan/Desa Organisasi ini disebut Dewan Pengurus Kelurahan/Desa Badan Komunikasi Pemuda Remaja Masjid Indonesia yang disingkat DP Kel/Des BKPRMI dan berkedudukan di Ibukota Kelurahan/Desa.</p>\r\n<p>6.    Struktur dan Tata Kerja organisasi dapat disesuaikan dengan kebutuhan daerah masing-masing</p>\r\n\r\n<p align=\"center\"><strong>BAB VI</strong></p>\r\n<p align=\"center\"><strong>KEPENGURUSAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 12</strong></p>\r\n<p align=\"center\"><strong>Pengurus Paripurna</strong></p>\r\n<p>Kepengurusan Paripurna BKPRMI terdiri dari Dewan Pengurus dan Majelis Pertimbangan.</p>\r\n<p align=\"center\"><strong>Pasal 13</strong></p>\r\n<p align=\"center\"><strong>Dewan Pegurus</strong></p>\r\n<p>1.    Dewan Pengurus Badan Komunikasi Pemuda Remaja Masjid Indonesia yaitu: Dewan Pengurus Pusat (DPP), Dewan Pengurus Wilayah (DPW), Dewan Pengurus Daerah (DPD), Dewan Pengurus Kecamatan (DPK), dan Dewan Pengurus Kelurahan / Desa (DP Kel/Des) .</p>\r\n<p>2.    Dewan Pengurus terdiri: Pengurus Harian, Departemen dan Lembaga BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 14</strong></p>\r\n<p align=\"center\"><strong>Pengurus Harian</strong></p>\r\n<p>1.    Pelaksanaan tugas, fungsi dan kewenangan organisasi BKPRMI.</p>\r\n<p>2.    Melaksanakan dan menandatangani kerjasama dan perjanjian organisasi, baik dalam negeri maupun luar negeri.</p>\r\n<p>3.    Pengaturan Pengurus Harian diatur lebih lanjut dalam ART BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 15</strong></p>\r\n<p align=\"center\"><strong>Departemen, Biro, Bidang dan Seksi</strong></p>\r\n<p>1.    Program organisasi yang bersifat umum dan temporer dilaksanakan oleh Departemen, Biro, Bidang dan Seksi.</p>\r\n<p>2.    Departemen adalah merupakan kelengkapan organisasi pada organisasi tingkat Pusat.</p>\r\n<p>3.    Biro adalah merupakan kelengkapan organisasi pada organisasi tingat Wilayah.</p>\r\n<p>4.     Bidang adalah merupakan kelengkapan organisasi pada organisasi tingat Daerah.</p>\r\n<p>5.    Seksi adalah merupakan kelengkapan organisasi pada organisasi tingkat Kecamatan, Kelurahan/Desa.</p>\r\n<p>6.    Hak, wewenang dan mekanisme Departemen, Biro, Bidang dan Seksi diatur dalam ART BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 16</strong></p>\r\n<p align=\"center\"><strong>Lembaga BKPRMI</strong></p>\r\n<p>1.    Program organisasi yang bersifat khusus dan berkelanjutan dilaksanakan oleh Lembaga BKPRMI.</p>\r\n<p>2.    Lembaga BKPRMI adalah merupakan bagian Kepengurusan Paripurna pada tingkat organisasi pada setiap tingkat organisasi.</p>\r\n<p>3.    Hak, wewenang dan mekanisme Lembaga diatur dalam Anggaran Rumah Tangga BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 1</strong><strong>7</strong></p>\r\n<p align=\"center\"><strong>Perwakilan</strong><strong> BKPRMI </strong><strong>Luar Negeri</strong></p>\r\n<p>1.    Perwakilan BKPRMI Luar Negeri dalam Struktur BKPRMI adalah perangkat organisasi yang memiliki peran dan fungsi untuk membantu melaksanakan kebijakan organisasi sebagai laboratorium kader dan penggerak serta pengemban program BKPRMI dalam perspektif profesionalitas yang didasarkan pada komitmen dan dedikasi demi kemaslahatan umat yang berbasis pada kemajuan dan peradaban umat melalui Masjid.</p>\r\n<p>2.    Perwakilan BKPRMI Luar Negeri dalam melakukan setiap aktifitas program, bersifat perpanjangan DPP BKPRMI yang berkedudukan di negara-negara sahabat Republik Indonesia dengan pendekatan fungsional.</p>\r\n<p>3.    Perwakilan BKPRMI Luar Negeri adalah perorangan (muslim) dan Unit-Unit Kedutaan Indonesia yang secara otomatis menjadi bagian dari keanggotaan BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 18</strong></p>\r\n<p align=\"center\"><strong>Majelis Pertimbangan</strong></p>\r\n<p>Majelis Pertimbangan Badan Komunikasi Pemuda Remaja Masjid Indonesia terdiri dari: Majelis Pertimbangan Pusat (MPP), Majelis Pertimbangan Wilayah (MPW), Majelis Pertimbangan Daerah (MPD), Majelis Pertimbangan Kecamatan (MPK) dan Majelis Pertimbangan Kelurahan/Desa (MP Kel/Des) .</p>\r\n<p align=\"center\"><strong>Pasal 19</strong></p>\r\n<p align=\"center\"><strong>Masa Bakti Kepengurusan</strong></p>\r\n<p>1.    Masa Bakti Kepengurusan Paripurna BKPRMI pada semua tingkat organisasi adalah selama 4 tahun, kecuali pada tingkat Kecamatan 2 tahun dan tingkat Kelurahan/Desa 1 tahun.</p>\r\n<p>2.    Ketua Umum BKPRMI pada semua tingkat organisasi dapat dipilih kembali hanya untuk satu periode berikut.</p>\r\n<p align=\"center\"><strong>Pasal 20</strong></p>\r\n<p align=\"center\"><strong>Pengurus Paripurna</strong></p>\r\n<p>1.    Susunan Kepengurusan Paripurna BKPRMI Tingkat Pusat/Nasional ditetapkan dan disahkan oleh Formatur/Ketua Umum terpilih selambat-lambat setelah 7 hari MUNAS BKPRMI</p>\r\n<p>2.    Susunan Kepengurusan Paripurna BKPRMI Tingkat Wilayah/Provinsi disahkan oleh Dewan Pengurus Pusat BKPRMI.</p>\r\n<p>3.    Susunan Kepengurusan Paripurna BKPRMI Tingkat Daerah/Kabupaten dan Kota disahkan oleh Dewan Pengurus Wilayah BKPRMI dengan memberikan salinan Surat Keputusan kepada Dewan Pengurus Pusat BKPRMI.</p>\r\n<p>4.    Susunan Kepengurusan Paripurna BKPRMI Tingkat Kecamatan disahkan oleh Dewan Pengurus Daerah BKPRMI dengan memberikan salinan Surat Keputusan kepada Dewan Pengurus Wilayah BKPRMI.</p>\r\n<p>5.    Susunan Kepengurusan Paripurna BKPRMI Tingkat Kelurahan/Desa disahkan oleh Dewan Pengurus Kecamatan BKPRMI dengan memberikan salinan Surat Keputusan kepada Dewan Pengurus Daerah BKPRMI.</p>\r\n<p>6.    Mekanisme pengesahan Kepengurusan Paripurna melalui jenjang organisasi.</p>\r\n\r\n<p align=\"center\"><strong>BAB VII</strong></p>\r\n<p align=\"center\"><strong>PEMBINA DAN PENASEHAT</strong></p>\r\n<p align=\"center\"><strong>Pasal 2</strong><strong>1</strong></p>\r\n<p align=\"center\"><strong>Pembina</strong></p>\r\n<p>Badan Komunikasi Pemuda Remaja Masjid Indonesia memiliki Pembina yaitu Pemerintah, Majelis Ulama Indonesia, Dewan Masjid Indonesia, dan Tokoh Masyarakat.</p>\r\n<p align=\"center\"><strong>Pasal 2</strong><strong>2</strong></p>\r\n<p align=\"center\"><strong>Penasehat</strong></p>\r\n<p>Badan Komunikasi Pemuda Remaja Masjid Indonesia memiliki Penasehat, yaitu Para AlumniPengurus BKPRMI dan para pakar yang relevan dan sesuai dengan kebutuhan struktur organisasi.</p>\r\n<p align=\"center\"><strong>Pasal 23</strong></p>\r\n<p align=\"center\"><strong>Pendiri</strong></p>\r\n<p>Pendiri adalah Organisasi Pemuda Remaja Masjid yang direpresentasikan oleh wakil-wakil mereka yang pertama kali mendirikan organisasi Badan Komunikasi Pemuda Masjid Indonesia (BKPMI) Tahun 1977 yang selanjutnya bernama Badan Komunikasi Pemuda Remaja Masjid Indonesia (BKPRMI) sesuai MUNAS VI BKPMI Tahun 1993 sebagai penggagas dan pencetus ide yang tergabung dalam Keluarga Besar Badan Komunikasi Pemuda Remaja Masjid Indonesia.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>BAB VIII</strong></p>\r\n<p align=\"center\"><strong>KEDAULATAN DAN PERMUSYAWARATAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 2</strong><strong>4</strong></p>\r\n<p align=\"center\"><strong>Kedaulatan</strong></p>\r\n<p>Kedaulatan BKPRMI berada di tangan anggota dan dilaksanakan sepenuhnya oleh MUNAS.</p>\r\n<p align=\"center\"><strong>Pasal 2</strong><strong>5</strong></p>\r\n<p align=\"center\"><strong>PERMUSYAWARATAN</strong></p>\r\n<p>1.    Bentuk Permusyawaratan dalam BKPRMI meliputi: Musyawarah, Rapat Pimpinan, Rapat Kerja, Silaturahmi Kerja dan Rapat-Rapat lain.</p>\r\n<p>2.    Status, fungsi, mekanisme permusyawaratan dan quorum diatur dalam Anggaran Rumah Tangga BKPRMI.</p>\r\n\r\n<p align=\"center\"><strong>BAB IX</strong></p>\r\n<p align=\"center\"><strong>ATRIBUT DAN KEKAYAAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 2</strong><strong>6</strong></p>\r\n<p align=\"center\"><strong>Atribut</strong></p>\r\n<p>1.    BKPRMI mempunyai lambang, lagu dan atribut lainnya.</p>\r\n<p>2.    Bentuk, fungsi dan tata pemakaian atribut diatur dalam Anggaran Rumah Tangga BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 27</strong></p>\r\n<p align=\"center\"><strong>Kekayaan</strong></p>\r\n<p>1.    Kekayaan BKPRMI adalah seluruh asset dan investasi kepengurusan di semua tingkat organisasi.</p>\r\n<p>2.    Kekayaan organisasi diperoleh dari:</p>\r\n<p>a.     Iuran dan sumbangan anggota organisasi</p>\r\n<p>b.    Zakat, infak, sodaqoh, wakaf, dan hibah umat Islam</p>\r\n<p>c.     Usaha lain yang halal dan tidak mengikat.</p>\r\n<p>3.    Jika BKPRMI dinyatakan bubar, maka seluruh kekayaan organisasi dihibahkan kepada lembaga da’wah sosial.</p>\r\n<p>4.    Mekanisme perolehan, pengadaan dan penghapusan/penghibahan kekayaan organisasi diatur lebih lanjut dalam ART BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 2</strong><strong>8</strong></p>\r\n<p align=\"center\"><strong>Penghargaan</strong></p>\r\n<p>1.    Penghargaan di lingkungan BKPRMI hanya boleh dikeluarkan dan ditetapkan oleh Dewan Pengurus Pusat BKPRMI.</p>\r\n<p>2.    Prosedur dan mekanisme penetapan penghargaan diatur dalam Anggaran Rumah Tangga BKPRMI.</p>\r\n\r\n<p align=\"center\"><strong>BAB X</strong></p>\r\n<p align=\"center\"><strong>PERUBAHAN DAN PEMBUBARAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 2</strong><strong>9</strong></p>\r\n<p align=\"center\"><strong>Perubahan</strong></p>\r\n<p>1.    Perubahan dan Penyempurnaan Anggaran Dasar ini, ditetapkan dalam MUNAS Badan Komunikasi Pemuda Remaja Masjid Indonesia atau MUNAS Istimewa (MUIS)</p>\r\n<p>2.    Tata cara dan mekanisme perubahan Anggaran Dasar dan Anggaran Rumah Tangga BKPRMI diatur dalam Anggaran Rumah Tangga.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>30</strong></p>\r\n<p align=\"center\"><strong>Pembubaran</strong></p>\r\n<p>1.    Pembubaran organisasi BKPRMI hanya dapat dilakukan oleh Musyawarah Nasional  dan atau oleh Musyawarah Nasional Istimewa yang diadakan khusus untuk hal tersebut.</p>\r\n<p>2.    Tata cara dan mekanisme pembubaran Organisasi BKPRMI diatur dalam Anggaran Rumah Tangga.</p>\r\n\r\n<p align=\"center\"><strong>BAB XI</strong></p>\r\n<p align=\"center\"><strong>ATURAN TAMBAHAN</strong></p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>31</strong></p>\r\n<p align=\"center\"><strong>Aturan Tambahan</strong></p>\r\n<p>Hal-hal   yang  belum  diatur   dalam  Anggaran Dasar  ini  akan   diatur dalam Anggaran Rumah Tangga.</p>\r\n<p align=\"center\"><strong>BAB XIV</strong></p>\r\n<p align=\"center\"><strong>KHATIMAH</strong></p>\r\n<p align=\"center\"><strong>Pasal 3</strong><strong>2</strong></p>\r\n<p>1.    Anggaran Dasar ini merupakan perubahan dan penyempurnaan dari Aggaran Dasar BKPRMI hasil Musyawarah Nasional XI Tahun 2009 di Jakarta</p>\r\n<p>2.    Anggaran Dasar ini mulai berlaku sejak tanggal ditetapkan.</p>\r\n\r\n<p>Ditetapkan di Makassar<br>Pada Tanggal 22 Rabiul Tsani 1435 H | 22 Februari 2014 M</p>\r\n\r\n<p align=\"center\"><strong>PRESIDIUM MUNAS XII</strong></p>\r\n<p align=\"center\"><strong>BADAN KOMUNIKASI PEMUDA REMAJA MASJID INDONESIA</strong></p>\r\n<p align=\"center\"> </p>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td valign=\"top\" width=\"234\">\r\n<p align=\"center\">Ketua,</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Dto</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Dr. H. Najamuddin Ramly, M.Si</p>\r\n<p align=\"center\">MPP BKPRMI</p>\r\n</td>\r\n<td valign=\"top\" width=\"161\"> </td>\r\n<td valign=\"top\" width=\"225\">\r\n<p align=\"center\">Sekretaris,</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Dto</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Halimah Watimena, SE</p>\r\n<p align=\"center\">DPD BKPRMI Kota Ambon</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td valign=\"top\" width=\"219\">\r\n<p align=\"center\">Anggota,</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Dto</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Dr. Daniel Mahmud Chaniago</p>\r\n<p align=\"center\">DPP BKPRMI</p>\r\n</td>\r\n<td valign=\"top\" width=\"219\">\r\n<p align=\"center\">Anggota,</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Dto</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Kurniadi Ilham, M.Si</p>\r\n<p align=\"center\">MPW BKPRMI Sumatera Barat</p>\r\n</td>\r\n<td valign=\"top\" width=\"219\">\r\n<p align=\"center\">Anggota,</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Dto</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\">Noval Adwan, SE</p>\r\n<p align=\"center\">DPW BKPRMI Banten</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n\r\n<p align=\"center\"><strong>ANGGARAN RUMAH TANGGA</strong></p>\r\n<p align=\"center\"><strong>BADAN KOMUNIKASI PEMUDA MASJID INDONESIA</strong></p>\r\n<p align=\"center\"><strong>HASIL MUSYAWARAH NASIONAL XII BKPRMI</strong></p>\r\n<p align=\"center\"><strong>MAKASSAR, 21 – 23 FEBRUARI 2014</strong></p>\r\n<p align=\"center\"><strong> </strong></p>\r\n<p align=\"center\"><strong>BAB 1</strong></p>\r\n<p align=\"center\"><strong>KETENTUAN UMUM</strong></p>\r\n<p align=\"center\"><strong>Pasal 1</strong></p>\r\n<p align=\"center\"><strong>Pengertian Umum</strong></p>\r\n<p>1.    Pada awal berdiri, organisasi ini bernama Badan Komunikasi Pemuda Masjid Indonesia dan disingkat BKPMI, kemudian dirobah menjadi Badan Komunikasi Pemuda Remaja Masjid Indonesia disingkat BKPRMI pada Musyarawah Nasional VI tahun 1993 di Jakarta.</p>\r\n<p>2.    BKPRMI adalah gerakan dakwah dan pendidikan bagi Pemuda Remaja Masjid seluruh Indonesia yang bersifat kemasyarakatan, dan kepemudaan.</p>\r\n<p>3.    BKPRMI adalah perhimpunan dan wahana komunikasi dari organisasi masyarakat, pemuda, remaja masjid untuk pengembangan dakwah sebagai sebuah sistem gerakan dalam pemberdayaan umat.</p>\r\n<p>4.    BKPRMI adalah organisasi yang independen, tidak terkait secara struktural dengan organisasi sosial kemasyarakatan dan organisasi sosial politik manapun, tetapi mempunyai hubungan kemitraan da’wah dan kader kepemimpinan yang berkelanjutan kader dengan Dewan Masjid Indonesia (DMI).</p>\r\n<p>5.    Organisasi Pemuda Remaja Masjid adalah perkumpulan atau perhimpunan atau ikatan pemuda-remaja masjid di tiap-tiap masjid atau mushallah, yang menjadikan masjid atau mushallah sebagai pusat kegiatan pembinaan aqidah, akhlaq, ukhuwah, keilmuan, keterampilan, kebudayaan dan peradaban umat.</p>\r\n<p align=\"center\"><strong>Pasal 2</strong></p>\r\n<p align=\"center\"><strong>Sifat Organisasi</strong></p>\r\n<p>1.    Ke-Islaman, yaitu mempunyai nilai dasar Islam dengan dakwah membawa kedamaian dan kebenaran untuk kesejahteraan umat.</p>\r\n<p>2.    Kemasjidan, yaitu berusaha menjadikan masjid sebagai pusat perjuangan, ibadah dan kebudayaan untuk mengembangkan umat dan bangsa.</p>\r\n<p>3.    Keumatan yaitu mempunyai arah dan perhatian kepada pengembangan potensi dan pemecahan permasalahan umat Islam dan kemanusiaan.</p>\r\n<p>4.    Ke-Indonesiaan yaitu berpijak pada nilai dasar bangsa, menjaga persatuan dan kesatuan, serta berwawasan nusantara untuk mencapai keadilan sosial bagi seluruh rakyat Indonesia.</p>\r\n<p>5.    Kemasyarakatan, yaitu segala hal yang menyangkut tata sosial dan budaya dalam interaksi kebangsaan.</p>\r\n<p>6.    Kepemudaan yaitu segala hal ihwal mengenai dan yang berhubungan dengan eksistensi, aktivitas, pembangunan, pengembangan dan cita-cita pemuda.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>Pasal 3</strong></p>\r\n<p align=\"center\"><strong>Sifat Pengembangan Program</strong></p>\r\n<p>BKPRMI mengembangkan program secara:</p>\r\n<p>1.    Komunikatif, adalah penyelanggaraan silaturahmi dan komunikasi program antar aktivis dan organisasi pemuda remaja masjid/mushallah, serta kepada umat dan bangsa.</p>\r\n<p>2.    Informatif, adalah pemberian pelayananan informasi tentang potensi, kegiatan dan program organisasi pemuda remaja masjid/mushallah kepada sesama pemuda remaja masjid, umat dan bangsa.</p>\r\n<p>3.    Konsultatif, adalah pemberian bimbingan dan penyamanan persepsi dalam rangka meningkatkan kuantitas dan kualitas kegiatan para aktivis dan perhimpunan organisasi pemuda remaja masjid/mushallah.</p>\r\n<p>4.    Koordinatif, adalah upaya terpadu dalam menumbuh-kembangkan aktivitas organisasi pemuda remaja masjid/mushallah sehingga tercipta suasana fungsionalisasi dan harmonisasi program.</p>\r\n<p>5.    Kemitraan adalah upaya membangun jaringan kerjasama dengan berbagai pihak yang bersifat halal, saling menguntungkan dan tidak mengikat.</p>\r\n\r\n<p align=\"center\"><strong>BAB II</strong></p>\r\n<p align=\"center\"><strong>KEANGGOTAAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 4</strong></p>\r\n<p align=\"center\"><strong>Keanggotaan</strong></p>\r\n<p>Anggota terdiri dari:</p>\r\n<p>1.    Anggota Biasa adalah organisasi pemuda/remaja masjid yang secara resmi menyatakan diri sebagai anggota kepada BKPRMI.</p>\r\n<p>2.    Anggota Fungsional adalah semua aktivis pengurus paripurna BKPRMI dari tingkat nasional sampai tingkat kelurahan/desa.</p>\r\n<p>3.    Anggota Kehormatan, adalah setiap orang dan organisasi yang dianggap telah berjasa kepada BKPRMI.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>Pasal 5</strong></p>\r\n<p align=\"center\"><strong>Kewajiban Anggota</strong></p>\r\n<p>Setiap Anggota BKPRMI mempunyai kewajiban:</p>\r\n<p>1.    Mematuhi Anggaran Dasar dan Anggaran Rumah Tangga serta peraturan dan ketentuan-ketentuan organisasi.</p>\r\n<p>2.    Menjaga dan menjunjung tinggi nama baik BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 6</strong></p>\r\n<p align=\"center\"><strong>Hak Anggota</strong></p>\r\n<p>1.    Setiap anggota berhak untuk berpartisipasi aktif dalam semua kegiatan BKPRMI.</p>\r\n<p>2.    Setiap Anggota mempunyai hak bicara dalam semua permusyawaratan BKPRMI pada semua tingkat organisasi.</p>\r\n<p>3.    Anggota Biasa mempunyai hak memilih dan dipilih dalam permusyawaratan BKPRMI maksimal sampai tingkat Kabupaten/Kota.</p>\r\n<p>4.    Anggota Fungsional mempunyai hak dipilih dalam permusyawaratan BKPRMI pada semua tingkat organisasi.</p>\r\n<p>5.    Setiap anggota berhak mendapatkan pelayanan dan memberikan saran dan usul.</p>\r\n<p align=\"center\"><strong>Pasal 7</strong></p>\r\n<p align=\"center\"><strong>Penerimaan Anggota</strong></p>\r\n<p>1.    Prosedur menjadi Anggota Biasa adalah:</p>\r\n<p>a.     Organisasi Pemuda Remaja Masjid yang resmi dan diakui oleh Pengurus Masjid bersangkutan.</p>\r\n<p>b.    Menyetujui Anggaran Dasar dan Anggaran Rumah Tangga dan Ketetapan-ketetapan BKPRMI.</p>\r\n<p>c.     Mengajukan permohonan tertulis bersedia menjadi Anggota Biasa kepada DPD BKPRMI setempat.</p>\r\n<p>d.    Setelah melakukan pertimbangan, DPD BKPRMI setempat mengeluarkan Surat Keputusan penerimaan dan menuangkannya ke dalam Sertifikat Anggota BKPRMI.</p>\r\n<p>2.    Prosedur menjadi Anggota Fungsional adalah:</p>\r\n<p>a.     Semua aktivis Pengurus Paripurna BKPRMI dari tingkat Nasional hingga Kelurahan/Desa, secara otomatis dinyatakan sebagai Anggota Fungsional.</p>\r\n<p>b.    DPD BKPRMI asal aktivis mengeluarkan Kartu Anggota Fungsional BKPRMI sebagai tanda Anggota Fungsional BKPRMI.</p>\r\n<p>3.    Prosedur menjadi Anggota Kehormatan adalah:</p>\r\n<p>a.     DPD BKPRMI atau DPW BKPRMI atau DPP BKPRMI melakukan penilaian terhadap orang atau organisasi yang dianggap telah berjasa kepada perkembangan BKPRMI.</p>\r\n<p>b.    Setelah melakukan pertimbangan, DPP BKPRMI mengeluarkan Surat Keputusan penerimaan dan menuangkannya ke dalam Sertifikat Anggota Kehormatan BKPRMI.</p>\r\n<p>Panduan tata cara pengelolaan administrasi penerimaan anggota, model sertifikat anggota dan kartu anggota diatur dalam Keputusan DPP BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 8</strong></p>\r\n<p align=\"center\"><strong>Representasi Keanggotaan</strong></p>\r\n<p>1.    Semua partisipasi Anggota Biasa dalam kegiatan BKPRMI direpresentasikan oleh Ketua Umum dan Sekretaris Umum dari organisasi Pemuda Remaja Masjid yang bersangkutan.</p>\r\n<p>2.    Semua partisipasi Anggota Fungsional dalam kegiatan BKPRMI pada prinsipnya merepresentasikan organisasi Pemuda Remaja Masjidnya dan dirinya sendiri.</p>\r\n<p>3.    Semua partisipasi Anggota Kehormatan dalam kegiatan BKPRMI adalah merepresentasikan dirinya sendiri.</p>\r\n<p align=\"center\"><strong>Pasal 9</strong></p>\r\n<p align=\"center\"><strong>Berakhirnya Keanggotaan</strong></p>\r\n<p>1.    Status Keanggotaan Anggota Biasa berakhir karena:</p>\r\n<p>a.     Bubarnya organisasi pemuda remaja masjid tersebut.</p>\r\n<p>b.    Menyatakan berhenti sebagai Anggota Biasa secara tertulis.</p>\r\n<p>c.     Dinyatakan berhenti keanggotaannya oleh BKPRMI.</p>\r\n<p>2.    Status keanggotaan Anggota Fungsional dan Anggota Kehormatan berakhir karena:</p>\r\n<p>a.     Meninggal Dunia</p>\r\n<p>b.    Menyatakan mengundurkan diri sebagai Anggota Fungsional atau Anggota Kehormatan, yang disampaikan secara tertulis.</p>\r\n<p>c.     Tidak lagi menjabat sebagai pengurus BKPRMI pada semua tingkat organisasi.</p>\r\n<p>d.    Dinyatakan berhenti keanggotaannya oleh BKPRMI.</p>\r\n<p>3.    Tata cara pemberhentian, pembelaan dan rehabilitasi anggota:</p>\r\n<p>a.     Pemberhentian Anggota Biasa, Anggota Fungsional dan Anggota Kehormatan dilakukan oleh DPP BKPRMI atas usulan DPW BKPRMI.</p>\r\n<p>b.    Pemberhentian keanggotaan hanya dapat dilakukan sesudah diberikan peringatan terlebih dahulu, sekurang-kurangnya 3 (tiga) kali oleh pengurus BKPRMI yang berwenang untuk itu.</p>\r\n<p>c.     Anggota yang dinyatakan berhenti keanggotaannya diberikan kesempatan membela diri dalam Musyawarah Daerah, Musyawarah Wilayah dan Musyawarah Nasional.</p>\r\n<p>d.    Apabila pembelaan dari anggota tersebut diterima, maka DPP BKPRMI harus mencabut keputusan tersebut.</p>\r\n<p>Prosedur lebih rinci mengenai pemberhentian, pembelaan dan rehabilitasi anggota akan diatur dalam Keputusan DPP BKPRMI.</p>\r\n\r\n<p align=\"center\"><strong>BAB III</strong></p>\r\n<p align=\"center\"><strong>KEPENGURUSAN PARIPURNA</strong></p>\r\n<p align=\"center\"><strong>Pasal 10</strong></p>\r\n<p align=\"center\"><strong>Kriteria Pengurus</strong></p>\r\n<p>Pengurus BKPRMI harus memenuhi kriteria pokok sebagai berikut:</p>\r\n<p>1.    Aktivis Pemuda Remaja Masjid dan atau Mushallah dan terdaftar sebagai anggota.</p>\r\n<p>2.    Mampu membaca dan mengamalkan Al-Qur’an dan sunnah secara benar.</p>\r\n<p>3.    Berakhlak mulia dan memiliki kepemimpinan Islam.</p>\r\n<p>4.    Mempunyai wawasan ke-Islaman dan ke-Indonesiaan yang kokoh dan integral.</p>\r\n<p>5.    Mempunyai sifat amanah, sidiq, fathonah dan tabligh.</p>\r\n<p>6.    Mengamalkan jiwa Muwahid, Mujahid, Muaddib, Musaddid, Mujaddid</p>\r\n<p align=\"center\"><strong>Pasal 11</strong></p>\r\n<p align=\"center\"><strong>Penyusunan Pengurus</strong></p>\r\n<p>1.    Ketua Umum bersama Formatur untuk pertama kali menyusun kelengkapan pengurus organisasi.</p>\r\n<p>2.    Mengenai kelengkapan Pengurus Lembaga diusulkan oleh Direktur dan dipertimbangkan untuk disahkan oleh Ketua Umum sesuai jenjang organisasi.</p>\r\n<p align=\"center\"><strong>Pasal 12</strong></p>\r\n<p align=\"center\"><strong>Dewan Pengurus</strong></p>\r\n<p>1.    Dewan Pengurus terdiri dari:</p>\r\n<p>a.     Seorang Ketua Umum dibantu maksimal 7 (tujuh) orang Ketua.</p>\r\n<p>b.    Seorang Sekretaris Jenderal/Umum maksimal 7 (tujuh) orang Wakil Sekretaris Jenderal/Umum.</p>\r\n<p>c.     Seorang Bendahara Umum dibantu minimal 3 (tiga) orang Wakil Bendahara.</p>\r\n<p>2.    Departemen/Biro/Bidang/Seksi terdiri dari seorang Koordinator dan minimal 2 (dua) anggota departemen yang melaksanakan program umum, sektoral dan temporer, terdiri dari:</p>\r\n<p>a.     Departemen Hubungan Antar Lembaga dan Luar Negeri.</p>\r\n<p>b.    Departemen Penelitian, dan Pemberdayaan Masjid.</p>\r\n<p>c.     Departemen Informasi, Iptek dan Kajian Strategis.</p>\r\n<p>d.    Departemen Kebudayaan dan Olah Raga.</p>\r\n<p>e.     Departemen Kajian Sosial dan Politik.</p>\r\n<p>f.     Departemen disesuaikan dengan kebutuhan daerah masing-masing</p>\r\n<p>3. Kepengurusan pada setiap jenjang disesuaikan dengan kebutuhan organisasi</p>\r\n<p>4. Pengurus atau Lembaga-lembaga BKPRMI terdiri dari:</p>\r\n<p>a.     Lembaga BKPRMI dipimpin oleh seorang Direktur dan dibantu Maksimal 5 (lima) orang Wakil Direktur  yang membidangi beberapa urusan; seorang  Sekretaris dibantu maksimal 2 (dua) Wakil Sekretaris dan seorang Bendahara dan satu wakil bendahara.</p>\r\n<p>b.    Kelengkapan pengurus lembaga-lembaga BKPRMI disusun dan diusulkan oleh Direktur kepada Ketua Umum untuk diteliti dan ditetapkan lebih lanjut dengan Keputusan Dewan Pengurus.</p>\r\n<p align=\"center\"><strong>Pasal 13</strong></p>\r\n<p align=\"center\"><strong>Tata Kerja</strong></p>\r\n<p>1.    Kesekretariatan dilakukan secara terpusat dan bertanggungjawab langsung kepada Sekretaris Jenderal/Umum sesuai dengan jenjang organisasi.</p>\r\n<p>2.    Kebendaharaan dilakukan terpusat oleh Bendahara Umum dan bendahara-bendahara sesuai dengan jenjang organisasi.</p>\r\n<p>3.    Hubungan kerja antar Direktur Lembaga antar tingkat organisasi secara vertikal dilakukan dengan berkoordinasi dengan Ketua yang membidangi bersama Sekretaris Jenderal/Umum.</p>\r\n<p>4.    Departemen berada di bawah koordinasi Ketua.</p>\r\n<p align=\"center\"><strong>Pasal 14</strong></p>\r\n<p align=\"center\"><strong>Majelis Pertimbangan</strong></p>\r\n<p>1.    Majelis Pertimbangan sebagai satu kesatuan kolektif yang terdiri dari seorang ketua, seorang sekretaris dan minimal 5 (lima) orang anggota.</p>\r\n<p>2.    Majelis Pertimbangan mempunyai kewenangan untuk memberikan usul, saran dan pengawasan serta teguran langsung kepada Ketua Umum.</p>\r\n<p>3.    Majelis Pertimbangan adalah tokoh pemuda masjid, alumni atau mantan pengurus Badan Komunikasi Pemuda Remaja Masjid.</p>\r\n<p align=\"center\"><strong>Pasal 15</strong></p>\r\n<p align=\"center\"><strong>Jabatan Ketua Umum</strong></p>\r\n<p>Jabatan  Ketua  Umum  Dewan  Pengurus Badan Komunikasi Pemuda Remaja Masjid Indonesia, dapat dipilih kembali hanya untuk satu masa jabatan berikutnya.</p>\r\n<p align=\"center\"><strong>Pasal 16</strong></p>\r\n<p align=\"center\"><strong>Pelantikan Dewan Pengurus dan Majelis Pertimbangan</strong></p>\r\n<p>1.    Pelantikan Dewan Pengurus dan Majelis Pertimbangan dilakukan oleh Pengurus setingkat jenjang di atasnya.</p>\r\n<p>2.    Majelis Pertimbangan Pusat dan Dewan Pengurus Pusat pada saat pelantikan diwajibkan mengucapkan ikrar pengurus bersama-sama, dipimpin oleh Presidium MUNAS.</p>\r\n<p align=\"center\"><strong>Pasal 17</strong></p>\r\n<p align=\"center\"><strong>Ikrar Pengurus</strong></p>\r\n<p>Pernyataan Ikrar Pengurus Pusat Badan Komunikasi Pemuda Remaja Masjid Indonesia:</p>\r\n<p>“Dengan nama Allah Yang Maha Pengasih dan Penyayang, kami bersaksi bahwa sesungguhnya tiada Tuhan selain Allah dan Nabi Muhammad SAW adalah utusan Allah. Kami ridho Allah sebagai Tuhan kami, dan Islam sebagai agama kami serta Nabi Muhammad SAW sebagai rasul kami, kami berikrar:</p>\r\n<p>1.    Akan memenuhi kewajiban Pengurus Badan Komunikasi Pemuda Remaja Masjid Indonesia dengan sebaik-baiknya.</p>\r\n<p>2.    Memegang teguh Anggaran Dasar/Anggaran Rumah Tangga dan peraturan lainnya dengan konsisten.</p>\r\n<p>3.    Mengutamakan prinsip-prinsip aqidah, akhlakul karimah dan ukhuwah Islamiyah, kesatuan dan persatuan, sebangsa, setanah air sesama manusia, dan kemanusiaan.</p>\r\n<p>4.    Mengembangkan prinsip-prinsip dakwah untuk mendapatkan keselarasan dan keseimbangan hidup.</p>\r\n<p>Semoga Allah mencurahkan rahmat, hidayah dan taufiknya”.</p>\r\n<p align=\"center\"><strong>Pasal 18</strong></p>\r\n<p align=\"center\"><strong>Masa Bakti Kepengurusan</strong></p>\r\n<p>1.    Dewan Pengurus Pusat dipilih untuk masa bakti 4 (empat) tahun dan Ketua Umum dapat dipilih kembali hanya untuk satu kali masa bakti berikutnya,</p>\r\n<p>2.    Dewan Pengurus Wilayah dipilih untuk masa bakti 4 (empat) tahun dan ketua umum dapat dipilih kembali hanya untuk satu kali masa bakti berikutnya</p>\r\n<p>3.    Dewan Pengurus Daerah dipilih untuk masa bakti 4 (empat) tahun dan ketua umum dapat dipilih kembali hanya untuk satu masa bakti berikutnya.</p>\r\n<p>4.    Dewan Pengurus Kecamatan dipilih untuk masa bakti 2 (dua) tahun dan hanya dapat menjadi Dewan Pengurus untuk dua kali masa bakti. Jabatan Ketua Umum hanya dapat dipilih dua kali masa bakti.</p>\r\n<p>5.    Dewan Pengurus Kelurahan/Desa dipilih untuk masa bakti 1 (satu) tahun dan hanya dapat menjadi Dewan Pengurus untuk dua kali masa bakti. Jabatan Ketua Umum hanya dapat dipilih dua kali masa bakti.</p>\r\n<p align=\"center\"><strong>Pasal 19</strong></p>\r\n<p align=\"center\"><strong>Pembinaan Kepengurusan</strong></p>\r\n<p>1.    Keberadaan dan kesinambungan kepengurusan BKPRMI merupakan tugas dan tanggungjawab semua pengurus secara berjenjang sehingga upaya pembinaan anggota pemuda remaja masjid, umat dan bangsa.</p>\r\n<p>2.    Pada setiap penyelenggaraan permusyawaratan suatu jenjang kepengurusan harus dihadiri oleh pengurus di atasnya di dalam wilayahnya.</p>\r\n<p>3.    Pada saat akan berakhirnya masa bakti kepengurusan, paling lambat 1 (satu) bulan sebelumnya pengurus yang berada 1 (satu) tingkat di atasnya.</p>\r\n<p>4.    Bersamaan dengan berakhirnya masa kepengurusan sebuah tingkat kepengurus BKPRMI dan belum melaksanakan permusyawaratan untuk itu, maka pengurus yang berada 1 (satu) tingkat di atasnya melaksanakan musyawarah untuk melaksanakan evaluasi dan pergantian kepengurusan dalam waktu paling lama 3 (tiga) bulan.</p>\r\n<p>5.    Setelah mendapat Surat Peringatan 2 (dua) kali dan pengurus yang bersangkutan masih belum melaksanakan musyawarah, dalam rentang waktu 4 (empat) bulan, pengurus yang berada 1 (satu) tingkat di atasnya wajib melakukan suatu tindakan pembinaan berupa perpanjangan sementara, atau pembekuan pengurus dengan membentuk karateker kepengurusan dalam rangka melaksanakan musyawarah untuk membentuk pengurus baru periode berikutnya.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>BAB IV</strong></p>\r\n<p align=\"center\"><strong>PEMBINA DAN PENASEHAT</strong></p>\r\n<p align=\"center\"><strong>Pasal 20</strong></p>\r\n<p align=\"center\"><strong>Pembina</strong></p>\r\n<p>1.    Pembina BKPRMI terdiri dari:</p>\r\n<p>a.     Pemerintah</p>\r\n<p>b.    Majelis Ulama Indonesia</p>\r\n<p>c.     Dewan Masjid Indonesia</p>\r\n<p>d.    Tokoh Masyarakat</p>\r\n<p>2.    Jumlah dan susunan Pembina ditetapkan oleh Dewan Pengurus.</p>\r\n<p>3.    Pembina memberikan pembinaan untuk pengembangan organisasi dan program.</p>\r\n<p align=\"center\"><strong>Pasal 21</strong></p>\r\n<p align=\"center\"><strong>Penasehat</strong></p>\r\n<p>1.    Penasehat BKPRMI terdiri dari Para Pakar, Figur Ulama dan Tokoh Masyarakat.</p>\r\n<p>2.    Jumlah dan susunan Penasehat ditetapkan oleh Dewan Pengurus.</p>\r\n<p>3.    Penasehat memberikan nasehat kearifan bagi kepentingan pengembangan organisasi.</p>\r\n\r\n<p align=\"center\"><strong>BAB V</strong></p>\r\n<p align=\"center\"><strong>LEMBAGA-LEMBAGA BKPRMI</strong></p>\r\n<p align=\"center\"><strong>Pasal 22</strong></p>\r\n<p align=\"center\"><strong>Nama-nama Lembaga</strong></p>\r\n<p>Agar program kerja yang khusus dapat dikerjakan secara lebih sistematis, berkesinambungan dan profesional, maka BKPRMI membentuk lembaga-lembaga, yaitu:</p>\r\n<p>1.    Lembaga Pembinaan dan Pengembangan Da’wah dan Sumber Daya Manusia (LPPDSDM), yang memberikan perhatian kepada program pembinaan kader yang berkesinambungan untuk tercapainya kualitas pemuda remaja masjid dan masyarakat yang beriman dan bertaqwa, berakhlak mulia, tangguh, cerdas, kreatif, berbudaya, produktif, mandiri, dan profesional.</p>\r\n<p>2.    Lembaga Pembinaan dan Pengembangan Taman Kanak-Kanak Al-Qur’an (LPPTKA), yang memberi perhatian kepada program dan gerakan membaca, menulis dan memahami Al-Qur’an bagi anak-anak di masjid dalam arti luas.</p>\r\n<p>3.    Lembaga Pembinaan dan Pengembangan Ekonomi dan Koperasi (LPPEKOP), yang memberi perhatian kepada program pengembangan potensi ekonomi untuk meningkatkan partisipasi pemuda remaja masjid dalam pengembangan dan pembinaan ekonomi umat yang berjiwa ke-Islaman, kerakyatan, kemandirian, kewairausahaan dan keadilan.</p>\r\n<p>4.    Lembaga Pembinaan Pengembangan Keluarga Sakinah BKPRMI (LPPK Sakinah BKPRMI), yang memberi perhatian kepada program pembina kesejahteraan keluarga muslim, khususnya keluarga besar BKPRMI dan peningkatan potensi keluarga muslim khususnya perempuan dalam arti luas.</p>\r\n<p>5.    Lembaga Pemberdayaan dan Penguatan Kesehatan Masyarakat (LPPKM), yang memberikan perhatian kepada program pembinaan, dan kesadaran masyarakat dalam mewujudkan kehidupan yang sehat jasmani dan rohani dengan berbasis masjid.</p>\r\n<p>6.    Lembaga Bantuan Hukum dan Advokasi (LBHA), yang memberikan perhatian dalam mewujudkan tertib organisasi dan meletakkan dasar serta arah perjuangan lembaga, membangun, membina dan meningkatkan kualitas keilmuan khususnya di bidang hukum terhadap anggota dan pengurus sebagai upaya dalam mencermati dinamika hukum, menjalin kerjasama terhadap instansi, LBH dan lembaga terkait lainnya dan memberikan konsultasi hukum dan atau bantuan hukum terhadap masyarakat.</p>\r\n<p>7.    Brigade Masjid yang memberikan perhatian kepada program cinta tanah air, bela negara dan bela masyarakat, termasuk kegiatan SAR, dalam arti luas bagi Pemuda Remaja Masjid Indonesia.</p>\r\n\r\n<p align=\"center\"><strong>BAB VI</strong></p>\r\n<p align=\"center\"><strong>M</strong><strong>AKSUD, FUNGSI, DAN SIFAT LEMBAGA</strong></p>\r\n<p align=\"center\"><strong>Pasal 23</strong></p>\r\n<p align=\"center\"><strong>Maksud</strong></p>\r\n<p>Peran Lembaga-lembaga BKPRMI dalam melakukan setiap aktivitas program, bersifat mandiri dengan pendekatan fungsional.</p>\r\n<p align=\"center\"><strong>Pasal 24</strong></p>\r\n<p>Keanggotaan lembaga-lembaga BKPRMI adalah perorangan dan unit-unit organisasi yang secara otomatis menjadi Anggota BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 25</strong></p>\r\n<p>Yang dimaksud sebagai penyelenggara program organisasi yaitu dalam melakukan kaderisasi professional di bidangnya, dalam rangka pengembangan potensi yang memiliki komitmen pada eksistensi dan citra BKPRMI.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>Pasal 26</strong></p>\r\n<p>1.    Lembaga-lembaga BKPRMI adalah organ integral dan merupakan bagian yang tak terpisahkan secara struktural dalam tubuh kepengurusan BKPRMI dan disahkan oleh Dewan Pengurus.</p>\r\n<p>2.    Bahwa kedudukan lembaga-lembaga BKPRMI Tingkat Nasional/Pusat harus berada di Ibukota Negara bersama dengan Struktur Kepengurusan DPP BKPRMI, sedangkan untuk kegiatan (pelaksana teknis) boleh dilaksanakan di Wilayah atau di Daerah.</p>\r\n<p>3.    Lembaga-lembaga BKPRMI adalah bersifat mandiri dalam menata program kerjanya, yang didasarkan pada AD dan ART serta Ketetapan MUNAS mengenai pokok-pokok Program Nasional.</p>\r\n<p>4.    Lembaga-lembaga BKPRMI selain yang telah ditetapkan dalam ART BKPRMI, hanya dapat dibentuk oleh DPP BKPRMI yang didasarkan pada kebutuhan dan kehendak hasil ketetapan MUNAS.</p>\r\n<p>5.    Struktur organisasi dan kepengurusan lembaga-lembaga BKPRMI diatur dengan Ketetapan/Keputusan DPP BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal 27</strong></p>\r\n<p>Kedudukan lembaga-lembaga BKPRMI dan pengaturan asset kelembagaan BKPRMI diatur tersendiri dalam Peraturan Organisasi yang didasarkan AD dan ART hasil MUNAS sebagai Forum Permusyawaratan Tertinggi.</p>\r\n<p align=\"center\"><strong>Pasal 28</strong></p>\r\n<p>Lembaga-lembaga BKPRMI bertugas membantu DPP BKPRMI menjalankan program spesialisasi bidang atau program unggulan serta melakukan kemitraan dalam kerangka pengembangan potensi kader dan pencapaian maksud tujuan lembaga.</p>\r\n<p align=\"center\"><strong>Pasal 29</strong></p>\r\n<p>Lembaga-lembaga BKPRMI melakukan apresiasi program sesuai skill dan spesifikasi bidang secara professional sebagai wadah pengembangan kader dengan kiprah program kemitraan.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>BAB VI</strong></p>\r\n<p align=\"center\"><strong>P</strong><strong>ERWAKILAN BKPRMI LUAR NEGERI</strong></p>\r\n<p align=\"center\"><strong>Pasal 30</strong></p>\r\n<p align=\"center\"><strong>Tujuan dan Maksud</strong></p>\r\n<p>1.    Pedoman Perwakilan BKPRMI Luar Negeri ini sebagai perangkat organisasi, dalam membuka kerjasama hubungan luar negeri dalam mengaktualkan dan mengimplementasikan program kerja BKPRMI, terkait dengan peningkatan skill, pengetahuan (pendidikan) dan syiar Islam (dakwah) sesuai dengan program DPP BKPRMI secara keseluruhan.</p>\r\n<p>2.    Yang dimaksud sebagai perangkat organisasi yaitu dalam melakukan misi kemitraan professional dalam rangka pengembangan potensi yang memiliki komitmen pada eksistensi dan citra BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>31</strong></p>\r\n<p align=\"center\"><strong>Kedudukan dan status</strong></p>\r\n<p>1.    Perwakilan BKPRMI Luar Negeri adalah organ integral dan merupakan bagian yang tak terpisahkan secara struktural dalam tubuh kepengurusan DPP BKPRMI.</p>\r\n<p>2.    Bahwa kedudukan Perwakilan BKPRMI Luar Negeri berada di ibu kota negara sahabat Republik Indonesia dengan struktur perwakilan DPP BKPRMI.</p>\r\n<p>3.    Perwakilan BKPRMI Luar Negeri adalah bersifat semi otonomi dalam menata program kerjanya, yang didasarkan pada AD dan ART serta Ketetapan MUNAS BKPRMI mengenai Pokok-Pokok Program Nasional.</p>\r\n<p>4.    Perwakilan BKPRMI Luar Negeri selain yang telah ditetapkan dalam ART BKPRMI, hanya dapat dibentuk oleh DPP BKPRMI yang didasarkan pada kebutuhan dan kehendak hasil ketetapan BKPRMI.</p>\r\n<p>5.    Struktur Organisasi dan Kepengurusan Perwakilan BKPRMI Luar Negeri diatur dengan Ketetapan/Keputusan DPP BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>32</strong></p>\r\n<p>Kedudukan Perwakilan BKPRMI Luar Negeri dan pengaturan asset perwakilan BKPRMI akan diatur tersendiri dalam peraturan organisasi yang didasarkan AD dan ART hasil MUNAS XII BKPRMI sebagai Forum Permusyawaratan Tertinggi.</p>\r\n<p align=\"center\"><strong>Pasal 33</strong></p>\r\n<p align=\"center\"><strong>Tugas dan wewenang</strong></p>\r\n<p>Perwakilan BKPRMI Luar Negeri bertugas membantu DPP BKPRMI menjalankan program kemitraan luar negeri dalam spesifikasi bidang peningkatan skill, pengetahuan atau program unggulan serta melakukan kemitraan dalam kerangka pengembangan potensi kader dan pencapaian maksud dan tujuan BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>3</strong><strong>4</strong></p>\r\n<p>Kewenangan Perwakilan BKPRMI Luar Negeri adalah melakukan apresiasi program kemitraan dan hubungan luar negeri sesuai skill dan spesifikasi bidang secara professional sebagai wadah pengembangan kader dengan kiprah program kemitraan dan program kemandirian yang tidak bertentangan dengan AD dan ART BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>3</strong><strong>5</strong></p>\r\n<p align=\"center\"><strong>Kedaulatan</strong></p>\r\n<p>Kedaulatan BKPRMI berada di tangan anggota dan dilaksanakan sepenuhnya oleh Musyawarah Nasional.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>BAB VI</strong></p>\r\n<p align=\"center\"><strong>DISIPLIN DAN SANKSI</strong></p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>3</strong><strong>6</strong></p>\r\n<p align=\"center\"><strong>Disiplin</strong></p>\r\n<p>Setiap anggota yang melanggar ketentuan organisasi dikenakan penerapan disiplin/sanksi.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>3</strong><strong>7</strong></p>\r\n<p align=\"center\"><strong>Tata Cara Penerapan Sanksi</strong></p>\r\n<p>Tata cara penerapan disiplin/sanksi dilakukan dengan berpegang teguh pada kaidah: Terbukti, Bijaksana, Adil dan Tegas.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>3</strong><strong>8</strong></p>\r\n<p align=\"center\"><strong>Jenis Disiplin</strong></p>\r\n<p>1.    Klarifikasi penerapan disiplin/sanksi terdiri dari: teguran lisan, teguran tertulis, skorsing, diminta untuk mengundurkan diri dan diberhentikan.</p>\r\n<p>2.    Pedoman disiplin/sanksi dan disiplin keanggotaan diatur dengan Keputusan DPP BKPRMI</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>BAB VII</strong></p>\r\n<p align=\"center\"><strong>PERMUSYAWARATAN</strong></p>\r\n<p align=\"center\"><strong>Pasal 39</strong></p>\r\n<p align=\"center\"><strong>Musyawarah Nasional</strong></p>\r\n<p>Musyawarah Nasional adalah pemegang kekuasaan tertinggi dalam organisasi diselenggarakan sekali dalam 4 (empat) tahun, diadakan oleh DPP BKPRMI.</p>\r\n<p>1.    Musyawarah Nasional dihadiri oleh Majelis Pertimbangan dan Dewan Pengurus Tingkat Pusat, Wilayah, Daerah serta Peninjau dan Undangan.</p>\r\n<p>2.    Segala ketetapan Musyawarah Nasional ditetapkan dengan semangat musyawarah untuk mufakat.</p>\r\n<p>3.    Musyawarah Nasional diselenggarakan untuk:</p>\r\n<p>a.     Menetapkan tata tertib musyawarah</p>\r\n<p>b.    Mendengar dan mengesahkan laporan pertanggungjawaban Dewan Pengurus Pusat.</p>\r\n<p>c.     Menetapkan Khittah, Anggaran Dasar dan Anggaran Rumah Tangga.</p>\r\n<p>d.    Menetapkan Program Nasional.</p>\r\n<p>e.     Menetapkan kebijaksanaan umum organisasi yang berkaitan dengan kehidupan keagamaan, kebangsaan dan kemasyarakatan.</p>\r\n<p>f.     Memilih dan menetapkan Ketua Umum Dewan Pengurus Pusat.</p>\r\n<p>g.    Memilih dan menetapkan anggota Formatur Dewan Pengurus Pusat.</p>\r\n<p>h.    Memilih dan menetapkan keputusan-keputusan lainnya.</p>\r\n<p>4.    Peserta Musyawarah Nasional terdiri dari MPP, DPP, MPW, DPW, MPD, dan DPD BKPRMI.</p>\r\n<p>5.    Jumlah peserta ditetapkan oleh DPP BKPRMI.</p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>4</strong><strong>0</strong></p>\r\n<p align=\"center\"><strong>Musyawarah Nasional Istimewa</strong></p>\r\n<p>1.    Dalam keadaan istimewa dapat diadakan Musyawarah Nasional Istimewa dan mempunyai kewenangan yang sama dengan Musyawarah Nasional.</p>\r\n<p>2.    Musyawarah Nasional Istimewa dapat diadakan sewaktu-waktu atas prakarsa Dewan Pengurus Pusat atau atas permintaan paling sedikit 2/3 (dua per tiga) dari jumlah Dewan Pengurus Wilayah setelah mendengar pendapat Majelis Pertimbangan.</p>\r\n<p align=\"center\"><strong>Pasal 41:</strong></p>\r\n<p align=\"center\"><strong>Musyawarah Wilayah</strong></p>\r\n<p>1.    Musyawarah Wilayah diselenggarakan 4 (empat) tahun sekali oleh Dewan Pengurus Wilayah, atau dalam keadaan istimewa dapat diadakan sewaktu-waktu atas Penetapan Dewan Pengurus Wilayah atas permintaan 2/3 (dua per tiga) dari jumlah Dewan Pengurus Daerah yang sah.</p>\r\n<p>2.    Musyawarah Wilayah dihadiri oleh:</p>\r\n<p>a.     DPW dan MPW BKPRMI.</p>\r\n<p>b.    DPD, MPD, DPK dan MPK BKPRMI.</p>\r\n<p>c.     Undangan yang ditetapkan oleh DPW BKPRMI.</p>\r\n<p>3.    Musyawarah Wilayah diselenggarakan untuk:</p>\r\n<p>a. Menetapkan tata tertib musyawarah.</p>\r\n<p>b. Mendengar dan mengesahkan pertanggungjawaban Dewan Pengurus Wilayah.</p>\r\n<p>c. Menetapkan Program Kerja Wilayah.</p>\r\n<p>d. Memilih dan menetapkan Ketua Umum Dewan Pengurus.</p>\r\n<p>e. Memilih dan menetapkan anggota Formatur Dewan Pengurus Wilayah.</p>\r\n<p>f. Menetapkan Kebijakan Strategis Organisasi di Tingkat Wilayah Provinsi.</p>\r\n<p align=\"center\"> </p>\r\n<p align=\"center\"><strong>Pasal 42</strong></p>\r\n<p align=\"center\"><strong>Musyawarah Daerah</strong></p>\r\n<p>1.    Musyawarah Daerah diselenggarakan 4 (empat) tahun sekali oleh Dewan Pengurus Daerah, atau dalam keadaan istimewa dapat diadakan sewaktu-waktu atas penetapan Dewan Pengurus Wilayah atas permintaan 2/3 dari jumlah Dewan Pengurus Kecamatan yang sah di daerah tersebut.</p>\r\n<p>2.    Musyawarah daerah dihadiri oleh:</p>\r\n<p>a.     DPD dan MPD BKPRMI.</p>\r\n<p>b.    DPK, MPK, DP Kel/Des, MP Kel/Des dan Organisasi Pemuda dan Remaja Masjid.</p>\r\n<p>c.     Undangan lain yang ditetapkan DPD BKPRMI.</p>\r\n<p>3.    Musyawarah Daerah diselenggarakan untuk:</p>\r\n<p>a. Menetapkan tata tertib musyawarah.</p>\r\n<p>b. Mendengar dan mengesahkan pertanggungjawaban Dewan Pengurus Daerah.</p>\r\n<p>c. Menetapkan Program Kerja Daerah.</p>\r\n<p>d. Memilih dan menetapkan Ketua Umum Dewan Pengurus Daerah.</p>\r\n<p>e. Memilih dan menetapkan anggota Formatur Dewan Pengurus Daerah.</p>\r\n<p>f.Menetapkan Kebijakan Strategis Organisasi di tingkat Daerah Kabupaten/Kota.</p>\r\n<p align=\"center\"><strong>Pasal 43</strong></p>\r\n<p align=\"center\"><strong>Musyawarah Kecamatan</strong></p>\r\n<p>1.    Musyawarah Kecamatan diselenggarakan 2 (dua) tahun sekali oleh Dewan Pengurus Kecamatan, atau dalam keadaan istimewa dapat diadakan sewaktu-waktu atas penetapan Dewan Pengurus Daerah atas permintaan 2/3 dari jumlah Anggota Fungsional atau 10 (sepuluh) Anggota Biasa di Kecamatan tersebut.</p>\r\n<p>2.    Musyawarah Kecamatan dihadiri oleh:</p>\r\n<p>a.     DPK dan MPK BKPRMI.</p>\r\n<p>b.    Dewan Pengurus Kelurahan/Desa.</p>\r\n<p>c.     Organisasi Pemuda dan Remaja Masjid di Wilayah Kecamatan.</p>\r\n<p>d.    Undangan lain yang ditetapkan oleh DPK BKPRMI.</p>\r\n<p>3.    Musyawarah Kecamatan diselenggarakan untuk:</p>\r\n<p>a. Menetapkan tata tertib musyawarah</p>\r\n<p>b. Mendengar dan mengesahkan pertanggungjawaban Dewan Pengurus Kecamatan.</p>\r\n<p>c. Menetapkan Program Kerja Kecamatan.</p>\r\n<p>d. Memilih Ketua Umum DPK dan MPK BKPRMI.</p>\r\n<p>e. Memilih dan menetapkan anggota Formatur Dewan Pengurus Kecamatan.</p>\r\n<p>f. Menetapkan Kebijakan Strategis Organisasi di Tingkat Kecamatan.</p>\r\n<p align=\"center\"><strong>Pasal 44</strong></p>\r\n<p align=\"center\"><strong>Musyawarah Kelurahan/Desa</strong></p>\r\n<p>1.    Musyawarah Kelurahan/Desa diselenggarakan 1 (satu) tahun sekali oleh Dewan Pengurus Kecamatan, atau dalam keadaan istimewa dapat diadakan sewaktu-waktu atas penetapan Dewan Pengurus Kelurahan/Desa atas permintaan 2/3 dari jumlah anggota perorangan atau 10 anggota kelembagaan di Kelurahan/Desa tersebut.</p>\r\n<p>2.    Musyawarah Kelurahan/Desa dihadiri oleh:</p>\r\n<p>a.     Dewan Pengurus Kelurahan/Desa dan Mejelis Pertimbangan Kelurahan/Desa.</p>\r\n<p>b.    Anggota Biasa, organisasi Pemuda Remaja Masjid di Wilayah Kelurahan/Desa.</p>\r\n<p>c.     Undangan lain yang ditetapkan oleh DP Kel/Des.</p>\r\n<p>3.    Musyawarah Kelurahan/Desa diselenggarakan untuk:</p>\r\n<p>a.    Menetapkan tata tertib musyawarah</p>\r\n<p>b. Mendengar dan mengesahkan pertanggungjawaban Dewan Pengurus   Kelurahan/Desa dan Majelis Pertimbangan Kelurahan/Desa.</p>\r\n<p>c.    Menetapkan Program Kerja Kelurahan/Desa.</p>\r\n<p>d.    Memilih Ketua Umum DP Kel/Des dan MP Kel/Des BKPRMI.</p>\r\n<p>e. Memilih dan menetapkan anggota Formatur Dewan Pengurus Kelurahan/Desa.</p>\r\n<p>f.    Menetapkan Kebijakan Strategis Organisasi di Tingkat Kelurahan/Desa.</p>\r\n\r\n<p align=\"center\"><strong>BAB VIII</strong></p>\r\n<p align=\"center\"><strong>HAK SUARA</strong></p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>4</strong><strong>5</strong></p>\r\n<p align=\"center\"><strong>Hak Suara</strong></p>\r\n<p>1.    Dalam Musyawarah Nasional, DPW BKPRMI dan DPD BKPRMI memiliki masing-masing 1 (satu) hak suara.</p>\r\n<p>2.    Dalam Musyawarah Wilayah, DPD BKPRMI dan DPK BKPRMI memiliki masing-masing 1 (satu) hak suara.</p>\r\n<p>3.    Dalam Musyawarah Daerah, DPK BKPRMI memiliki masing-masing 1 (satu) suara</p>\r\n\r\n<p align=\"center\"><strong>BAB IX</strong></p>\r\n<p align=\"center\"><strong>RAPAT-RAPAT DAN SILATURAHMI</strong></p>\r\n<p align=\"center\"><strong>Pasal </strong><strong>4</strong><strong>6</strong></p>\r\n<p align=\"center\"><strong>Rapat-rapat</strong></p>\r\n<p>Rapat-rapat BKPRMI terdiri dari:</p>\r\n<p>1.    Rapat Pimpinan Nasional (RAPIMNAS) yaitu rapat yang diselenggarakan oleh DPP BKPRMI, dihadiri oleh para Pengurus Harian dan Majelis Pertimbangan Pusat. Ketua Umum dan Ketua Majelis Pertimbangan Wilayah, berwenang memutuskan ketentuan organisasi yang bersifat strategis di tingkat nasional dan mempunyai kekuatan hukum setingkat di bawah Musyawarah Nasional atau Musyawarah Istimewa.</p>\r\n<p>2.    Rapat Kerja Nasional (RAKERNAS), yaitu rapat yang diselenggarakan oleh DPP BKPRMI, dihadiri oleh para Pengurus DPP BKPRMI ditambah utusan DPW BKPRMI, untuk memutuskan rincian dan agenda program kerja nasional.</p>\r\n<p>3.    Rapat Pimpinan Wilayah (RAPIMWIL), yaitu rapat yang diselenggarakan oleh DPW BKPRMI, dihadiri oleh Pengurus Harian dan Majelis Pertimbangan Wilayah, Ketua Umum dan Ketua Majelis Pertimbangan Daerah, berwenang memutuskan ketentuan organisasi yang bersifat strategis di tingkat Wilayah.</p>\r\n<p>4.    Rapat Kerja Wilayah (RAKERWIL), yaitu rapat yang diselenggarakan oleh DPW BKPRMI, dihadiri oleh para Pengurus DPW BKPRMI ditambah utusan DPD BKPRMI, untuk memutuskan rincian dan agenda program kerja Wilayah.</p>\r\n<p>5.    Rapat Pimpinan Daerah (RAPIMDA), yaitu rapat yang diselenggarakan oleh DPD BKPRMI, dihadiri oleh Pengurus Harian dan Majelis Pertimbangan Daerah, Ketua Umum dan Ketua Majelis Pertimbangan Kecamatan, Ketua Umum tingkat Kelurahan/Desa berwenang memutuskan ketentuan organisasi yang bersifat strategis Daerah.</p>\r\n<p>6.    Rapat Kerja Daerah (RAKERDA), yaitu rapat yang diselenggarakan oleh DPD BKPRMI, dihadiri oleh para pengurus DPD BKPRMI ditambah utusan DPK BKPRMI, untuk memutuskan rincian agenda program kerja Daerah.</p>\r\n<p>7.    Rapat Pimpinan Kecamatan (RAPIMCAM), yaitu rapat yang diselenggarakan oleh DPK BKPRMI, dihadiri oleh para Pengurus Harian dan Majelis Pertimbangan Kecamatan Ketua Umum, Ketua Umum dan Majelis Pertimbangan Kelurahan/Desa, berwenang memutuskan ketentuan organisasi yang bersifat strategis ditingkat Kelurahan/Desa.</p>\r\n<p>8.    Rapat Kerja Kecamatan (RAKERCAM), yaitu rapat yang diselenggarakan oleh DPK BKPRMI, dihadiri oleh para Pengurus DPK BKPRMI, ditambah utusan DP Kel/Des BKPRMI, untuk memutuskan rincian dan agenda program kerja Kelurahan/ Desa.</p>\r\n<p>9.    Rapat Pimpinan Kelurahan/Desa (RAPIMKEL/DESA), yaitu rapat yang diselenggarakan oleh DP Kel/Des BKPRMI, yang dihadiri oleh para Pengurus DP Kel/Des BKPRMI ditambah Ketua Umum Pemuda Remaja Masjid, untuk memutuskan rincian dan agenda program kerja Kelurahan/Desa.</p>\r\n<p>10.  Rapat Pengurus harian ialah rapat yang dihadiri oleh para Pengurus Harian sesuai jenjang organisasi.</p>\r\n<p>11.  Rapat Pleno ialah rapat yang dihadiri oleh Dewan Pengurus dan Majelis Pertimbangan, sesuai jenjang organisasi.</p>\r\n<p>12.  Rapat Pleno sekurang-kurangnya diselenggarakan 6 (enam) bulan sekali.</p>\r\n<p>13.  Rapat Kerja sekurang-kurangnya diselenggarakan 1 (satu) kali dalam satu periode.</p>\r\n<p>14.  Rapat Pimpinan sekurang-kurangnya diselenggarakan 2 (dua) kali dalam satu periode.</p>\r\n<p align=\"center\"><strong>Pasal 47</strong></p>\r\n<p align=\"center\"><strong>Silaturahmi Kerja</strong></p>\r\n<p>1.    Silaturahmi Kerja diselenggarakan oleh lembaga BKPRMI, sekurang-kurangnya 1 (satu) kali dalam satu periode, berwenang merumuskan dan menetapkan rincian agenda program kerja lembaga, sesuai jenjang struktur organisasi.</p>\r\n<p>2.    Merumuskan kebijakan lembaga sesuai tugas dan fungsi untuk ditetapkan lebih lanjut dengan keputusan Dewan Pengurus.</p>\r\n<p>3.    Menetapkan rincian agenda program kerja lembaga sesuai jenjang struktur organis</p>', NULL, NULL, NULL, 0, 1);
INSERT INTO `t_konten` (`id`, `user_id`, `menu_id`, `sub_menu1_id`, `sub_menu2_id`, `judul`, `slug`, `tanggal`, `deskripsi`, `foto`, `video`, `keterangan_foto`, `dibaca`, `publish`) VALUES
(3, 1, 8, NULL, NULL, 'MPM PP Muhammadiyah Soroti Pesoalan Buruh', 'mpm-pp-muhammadiyah-soroti-pesoalan-buruh', '2019-05-10 14:39:18', '<p>Sejumlah persoalan buruh yang hari ini belum tuntas menjadi catatan khusus Majelis Pemberdayaan Masyarakat (MPM) Pimpinan Pusat Muhammadiyah, melalui Diskusi Publik yang membedah tema “Keadilan Buruh Ditengah Euforia Politik antara Utopia dan Realita”</p>\r\n<p>M. Nurul Yamin, Ketua MPM PP Muhammadiyah menyampaikan, isu buruh menjadi isu politik karena memiliki sejumlah persoalan.</p>\r\n<p>“Terdapat permasalahan yang subtansial terkait buruh di Indonesia, bukan hanya persoalan menyangkut upah, tetapi relasi kuasa antara majikan dengan buruh dan relasi sosial buruh dengan keluarga di kampung halaman,” ungkap Yamin dalam pengantar Diskusi di Gedung Muhammadiyah, Yogyakarta pada Jum\'at (3/5).</p>\r\n<p>M. Nurul Yamin mengatakan bahwa MPM PP Muhammadiyah punya tanggung jawab besar mengenai buruh, apalagi sejak awal dibentuknya MPM merupakan transformasi lembaga buruh, tani dan nelayan dituntut untuk peduli mereka yang termarjinalkan, termasuk buruh.</p>\r\n<p>“Komitmen MPM terhadap pemberdayaan buruh memang sudah menjadi tugas dakwah agar keadilan benar-benar dapat dirasakan bagi mereka baik yang di dalam negeri maupun luar negeri,” katanya.</p>\r\n<p>Menghadirkan pembicara diskusi diantaranya Kirnadi Wakil Ketua DPD Konferensi Serikat Pekerja Seluruh Indonesia (KSPSI) DIY, Siti Badriyah Aktivis Migran Care dan Mantan Pekerja Migran Indonesia dan Kasubdit Perlindungan Tenaga Kerja Indonesia Kementrian Ketenagakerjaan. MPM berharap diskusi publik ini memberi persepektif bagi anak muda dalam melihat fakta sosial yang terjadi di zaman dan lingkungan mereka.</p>\r\n<p>Dikesempatan yang sama salah satu pemateri Kirnadi mengatakan, buruh harus tahu dan mengerti pesoalan politik. Karena melihat realitas saat ini bahwa semua hal tentang buruh di atur dalam UU bahkan Peraturan Pemerintahnya.</p>\r\n<p>“Mengenai soal buruh pemerintah surplus peraturan tetapi minim impelmentasi, atas dasar inilah kami dari elemen buruh berhara Muhamamdiyah bisa menghadirkan pemberdayaan impelemtatif kepada buruh agar mengerti persoalan politik,” katanya.</p>\r\n<p>Menyikapi soal Peraturan Pemerintah nomor 78 tahun 2015 tentang pengupahan, Kirnadi berpendapat harus dicabut,  karena upah yang ada semakin rendah dan yang tinggi semakin tinggi.</p>\r\n<p>\"Sehingga kami, terus mengupayakan agar ketidakadilan ini segera berakhir dan dikembalikan ke peraturan awal,\" pungkasnya. (<strong>indra</strong>)</p>', 'mpm-pp-muhammadiyah-soroti-pesoalan-buruh.jpeg', '', 'dsikusi rutin pemberdayaan masyarakat', 33, 1),
(4, 1, 8, 9, NULL, 'Partai Politik Diminta Kedepankan Kaderisasi Kaum Perempuan', 'partai-politik-diminta-kedepankan-kaderisasi-kaum-perempuan', '2019-05-10 14:38:20', '<p>Politikus Partai Gerindra Novita Dewi mengatakan partai politik harus mengedepankan kaderisasi terhadap kaum perempuan. Sebab, banyak calon anggota legislatif keterwakilan perempuan yang tidak terpilih pada pemilu 2019</p>\r\n<p>\"Penting sekali kaderisasi perempuan yang dilakukan oleh partai politik, sehingga perempuan dapat mengejar ketertinggalannya dengan laki-laki,\" kata Novita di<strong> Jakarta</strong> Rabu (8/5).</p>\r\n<p>Dia melihat banyak caleg perempuan tidak terpilih baik di DPR maupun DPR karena beberapa faktor. Hal yang paling mendasar adalah sistem sosial yang masih bias soal gender.</p>\r\n<p>\"Misalnya saja di sebuah keluarga, perempuan dianggap yang paling bertanggungjawab mengurus rumah tangga. Jika ia ingin berkarya di domain publik, maka pekerjaan yang dilakukan menjadi ganda yakni mengurus rumah tangga dan berkarya di luar rumah,\" ujarnya.</p>\r\n<p>Atas dasar itu, menurutnya, perempuan dituntut kerja ekstra jika ingin terjun ke dunia politik. \"Di sisi lain, bantuan partai kepada kalangan perempuan juga tidak banyak. Bidang perempuan dalam struktur kepengurusan partai tidak banyak memberi peningkatan kapasitas pendidikan dan lainnya,\" jelas calon anggota legislatif DPRD Jakarta ini.</p>\r\n<p>Sementara itu, Peneliti Formappi, Lucius Karus mengatakan partai politik harus mencarikan rumusan agar kaum perempuan memiliki perwakilan di parlemen. Sehingga, tidak hanya sebagai formalitas pemenuhan kuota 30 persen.</p>\r\n<p>\"Jadi, jawaban untuk perjuangan panjang terkait keterwakilan mereka sebatas formalitas saja, dijawab mekanisme pencalonan dengan mewajibkan partai politik minimal 30 persen pencalonan perempuan,\" kata Lucius.</p>\r\n<p>Dia menilai yang menjadi persoalan kuota 30 persen itu hanya diatur sampai batas pencalonan saja. Tetapi, setelah itu tidak ada jaminan perempuan masih punya wakil yang cukup di parlemen baik DPR maupun DPRD.</p>\r\n<p>\"Padahal yang kita butuhkan itu kan perempuan hadir di lembaga parlemen minimal 30 persen, sehingga ada kekuatan untuk memperjuangkan kepentingan perempuan,\" ujarnya.</p>\r\n<p>Dia mengatakan kaderisasi partai politik masih cenderung didominasi oleh politisi laki-laki, sistem oligarki partai politik itu memelihara dengan sangat baik dominasi politisi laki-laki atas perempuan. Sehingga, peluang politisi perempuan untuk diusung jadi calon legislatif terpilih sangat kecil.</p>\r\n<p>\"Kesadaran ini masih dominan, tidak hanya dunia politik tapi juga masyarakat umumnya dominasi patriarki itu masih kental,\" tandasnya. <strong>[indra]</strong></p>', 'partai-politik-diminta-kedepankan-kaderisasi-kaum-perempuan.jpg', '', 'Pengundian nomor urut Parpol peserta Pemilu 2019. ©2018', 53, 1),
(5, 1, 10, NULL, NULL, 'Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', 'perspektif-5-orientasi-pembelajaran-adalah-mengenal-allah', '2019-05-09 13:55:30', '<p>Pembelajaran memiliki orientasi yang jelas dalam membentuk suatu peradaban bangsa. Orientasi yang jelas itu tentunya merupakan hasil perenungan dan pengkajian mendalam tentang tujuang hakiki dari pelaksanaan pendidikan itu sendiri. Sehingga setiap aktivitas pembelajaran yang dilaksanakan dapat berjalan sesuai pedoman yang tersusun dengan jelas dan sistematis. Oleh karena itu, bagi setiap pelaksana atau penyelenggara pendidikan sangat penting menyadari setiap ketetapan dan kebijakan arah pembelajaran yang disusun. Sebab, jika keliru dalam menyusun orientasinya maka akan terjadi kekacauan dalam sistem dan struktur aktivitas yang dikelola.</p>\r\n<p>Orientasi tujuan pendidikan yang hakiki adalah mengenal Allah, mengenal Tuhan yang menciptakan seluruh makhluk di muka bumi. Dia maha pengasih dan maha penyayang. Pendidikan yang dilaksanakan sudah sepatutnya mengarahkan setiap aktivitas pembelajaran untuk mengenal pencipta alam semesta. Seperti, mempelajari dengan serius asal usul ilmu pengetahuan darimana ia datang, ia berproses, dan ia bermanfaat bagi manusia. Semua pelajaran dapat diorientasikan ke arah yang sama, yakni mengenal Allah. Manusia yang mengenal Tuhan maka ia akan selalu berusaha dengan maksimal melaksanakan aktivitas karena Tuhannya.</p>\r\n<p>Mengenalkan Allah melalui pendidikan sudah seharusnya dilakoni penyelenggara pendidikan dengan sekuat tenaga. Peserta didik yang di keluarganya telah dibekali pemahaman agama atau pengenalan Allah, lalu diperkuat oleh pihak instansi pendidikan di tiap sekolah. Dengan begitu, peserta didik akan memiliki pengetahuan yang bagus tentang Tuhannya. Setelah itu, mereka akan menyadari bahwa setiap kegiatan yang ia kerjakan selalu diketahui oleh Allah. Mereka akan semakin rajin dan taat dalam menjalankan perintah agama. Namun, perlu juga diperhatikan dengan proses pembimbingan keberlanjutan. Artinya, harus ada pemantauan intensif dan bimbingan yang maksimal dalam membiasakan peserta didik dekat dengan Allah.</p>\r\n<p>Selanjutnya, fasilitas pendidikan juga patut disediakan dengan layak. Buku bacaan yang dengan mudah diakses bagi peserta didik, sekolah memiliki ruang diskusi dan eksplorasi pengetahuan, dan memiliki kelengkapan sarana ibadah, dan bentuk lainnya. Fasilitas ini menjadi penting karena ia akan menjadi mediator atau alat mengoptimalkan keberhasilan pembelajaran sesuai arah dan tujuan yang ditetapkan. Tanpa ada didukung dengan fasilitas yang memadai akan muncul kendala seperti lambatnya pemahaman peserta didik dan korelasi materi pelajaran dengan implementasi yang kurang optimal. Dengan demikian, beberapa catatan di atas akan diharapkan mampu membentuk pesera didik sesuai perintah Allah.</p>', NULL, NULL, NULL, 9, 1),
(6, 1, 2, 5, NULL, 'Pengurus', 'pengurus', '2019-05-09 14:20:46', '<p>\r\n<strong>Pembina</strong><strong> :</strong>\r\n<ol>\r\n<li>Camat Ngaglik</li>\r\n<li>Kapolsek. Ngaglik</li>\r\n<li>Kepala KUA Ngaglik</li>\r\n</ol>\r\n</p>\r\n\r\n<p>\r\n<strong>Penasehat</strong><strong> :</strong>\r\n<ol>\r\n<li>Ketua MUI Ngaglik</li>\r\n<li>Ketua IKADI Ngaglik</li>\r\n<li>Ketua DDI Ngaglik</li>\r\n</ol>\r\n</p>\r\n\r\n<p><strong>Ketua</strong><strong> :</strong>\r\n<ol>\r\n<li>Hariyatmoko</li>\r\n<li>Dian Hudawan, S.Si., M.Sc.</li>\r\n</ol>\r\n</p>\r\n\r\n<p><strong>Sekretaris</strong> <strong>:</strong>\r\n<ol>\r\n<li>Muhammad Zainal HA</li>\r\n<li>Andi Ahmad Akbar Z.</li>\r\n</ol>\r\n</p>\r\n\r\n<p><strong>Bendahara</strong><strong> :</strong>\r\n<ol>\r\n<li>Davit Endra T.</li>\r\n<li>Siti Nurfitarini</li>\r\n</ol>\r\n</p>\r\n\r\n<p><strong>Bidang Syiar</strong><strong> :</strong>\r\n<ul>\r\n<li>Muslikh Bahaddur, S.Pd. (Kepala)</li>\r\n<li>Ibnu (Wakil Kepala)</li>\r\n<li>Yusuf Indra (Sekretaris)</li>\r\n<li>Abdillah Abdi</li>\r\n<li>Wahyu Adi Saputro</li>\r\n<li>Arif Rakhmat Nasrullah</li>\r\n<li>Hilmy Alghifari</li>\r\n</ul>\r\n</p>\r\n\r\n<p><strong>Bidang Sumber Daya Manusia :</strong>\r\n<ul>\r\n<li>Fajar Untoro (Kepala)</li>\r\n<li>Bayu Alfian Argi (Wakil Kepala)</li>\r\n<li>Aviara Sumarsono (Sekretaris)</li>\r\n<li>M. Ridwan</li>\r\n<li>Diyah Novi S.</li>\r\n<li>Lindra M.</li>\r\n<li>Yuli Indarti</li>\r\n<li>Dwi Retno</li>\r\n<li>Nur Rizki F.</li>\r\n</ul>\r\n</p>\r\n\r\n<p><strong>Bidang Humas dan Media :</strong>\r\n<ul>\r\n<li>Dido Setyo Wibowo (Kepala)</li>\r\n<li>Nur Hamid Sutanto, S.Kom. (Wakil Kepala)</li>\r\n<li>Robby Yudistira (Sekretaris)</li>\r\n<li>Cesar Apri Widayanti</li>\r\n<li>Rima Cholilah</li>\r\n<li>Anis Nureni</li>\r\n<li>Noviani Dewi Kirana</li>\r\n<li>Aliefsyiah Kinta Seroja</li>\r\n<li>Nala Sekarini</li>\r\n<li>Fajar Eka Saputra</li>\r\n<li>Cahyo Ardhy Pratomo</li>\r\n</ul>\r\n</p>\r\n\r\n<p><strong>Bidang Olahraga :</strong>\r\n<ul>\r\n<li>Tego Raharjo (Kepala)</li>\r\n<li>Fandri Lis Malindra</li>\r\n<li>Tedy Prasetya N.</li>\r\n<li>Surya Yusuf Pratama</li>\r\n</ul>\r\n</p>', NULL, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_log`
--

CREATE TABLE `t_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `konten_id` int(11) DEFAULT NULL,
  `ip_address` varchar(16) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_log`
--

INSERT INTO `t_log` (`id`, `user_id`, `konten_id`, `ip_address`, `keterangan`, `tanggal`) VALUES
(1, NULL, 3, '::1', 'Melihat Konten Berita dengan judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-09 07:56:44'),
(2, NULL, NULL, '::1', 'Melihat Konten  ->  Dengan Judul ', '2019-05-09 08:00:57'),
(3, NULL, NULL, '::1', 'Melihat Konten  ->  Dengan Judul ', '2019-05-09 08:01:11'),
(4, NULL, NULL, '::1', 'Melihat Konten  -> Sejarah Dengan Judul ', '2019-05-09 08:01:27'),
(5, NULL, NULL, '::1', 'Melihat Konten  -> Sejarah Dengan Judul ', '2019-05-09 08:01:35'),
(6, NULL, NULL, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul ', '2019-05-09 08:01:53'),
(7, NULL, NULL, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 08:02:45'),
(8, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 08:04:24'),
(9, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 08:05:10'),
(10, 1, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 08:06:35'),
(11, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 08:06:49'),
(12, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-09 08:08:28'),
(13, NULL, NULL, '::1', 'List Konten Berita', '2019-05-09 08:08:46'),
(14, NULL, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-09 08:09:15'),
(15, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 08:13:16'),
(16, NULL, NULL, '::1', 'List Konten Berita', '2019-05-09 08:13:23'),
(17, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-09 09:16:08'),
(18, NULL, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-09 09:16:16'),
(19, NULL, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-09 09:16:55'),
(20, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-09 09:20:36'),
(21, NULL, NULL, '::1', 'List Konten Berita', '2019-05-09 09:20:55'),
(22, NULL, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-09 09:21:00'),
(23, NULL, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-09 09:42:47'),
(24, NULL, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-09 09:43:26'),
(25, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 09:59:58'),
(26, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-09 10:02:30'),
(27, NULL, NULL, '::1', 'List Konten Berita', '2019-05-09 10:02:44'),
(28, NULL, NULL, '::1', 'List Konten Berita', '2019-05-09 10:03:04'),
(29, NULL, NULL, '::1', 'List Konten Berita', '2019-05-09 10:04:09'),
(30, NULL, NULL, '::1', 'List Konten Berita', '2019-05-09 10:06:37'),
(31, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-10 02:01:24'),
(32, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-10 02:01:25'),
(33, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-10 02:02:32'),
(34, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:02:44'),
(35, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:03:04'),
(36, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:12:07'),
(37, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:20:02'),
(38, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:20:59'),
(39, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:22:42'),
(40, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:29:40'),
(41, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:30:00'),
(42, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:32:58'),
(43, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:37:03'),
(44, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:37:16'),
(45, 1, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-10 02:38:44'),
(46, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:39:35'),
(47, 1, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-10 02:39:40'),
(48, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:40:00'),
(49, 1, NULL, '::1', 'List Konten Berita', '2019-05-10 02:40:10'),
(50, 1, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-10 03:25:48'),
(51, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-10 03:40:18'),
(52, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-10 03:42:27'),
(53, NULL, NULL, '::1', 'List Konten Berita', '2019-05-10 03:42:33'),
(54, NULL, NULL, '::1', 'List Konten Berita', '2019-05-10 03:42:51'),
(55, NULL, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-10 03:42:54'),
(56, NULL, NULL, '::1', 'List Konten Berita', '2019-05-10 03:42:57'),
(57, NULL, NULL, '::1', 'List Konten Berita', '2019-05-10 03:43:04'),
(58, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-10 03:43:10'),
(59, 1, 6, '::1', 'Melihat Konten Organisasi -> Pengurus Dengan Judul Pengurus', '2019-05-10 03:45:50'),
(60, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-10 04:03:32'),
(61, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 04:03:43'),
(62, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 06:03:52'),
(63, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-10 06:24:16'),
(64, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-10 06:24:16'),
(65, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-10 06:25:18'),
(66, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-10 06:25:19'),
(67, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-10 06:25:22'),
(68, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 07:23:42'),
(69, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 07:36:37'),
(70, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 07:38:24'),
(71, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 07:39:37'),
(72, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 07:42:55'),
(73, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-10 07:43:02'),
(74, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-10 07:47:14'),
(75, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-10 07:59:19'),
(76, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 07:59:27'),
(77, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:00:05'),
(78, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:00:58'),
(79, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:03:36'),
(80, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:37:21'),
(81, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:37:46'),
(82, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:38:18'),
(83, 1, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-10 08:38:22'),
(84, 1, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-10 08:39:24'),
(85, 1, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-10 08:39:37'),
(86, 1, NULL, '::1', 'Melihat Konten  Dengan Judul ', '2019-05-10 08:40:01'),
(87, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:40:28'),
(88, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:40:54'),
(89, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:41:20'),
(90, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:41:27'),
(91, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:41:55'),
(92, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:41:57'),
(93, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:42:07'),
(94, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:42:16'),
(95, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:42:41'),
(96, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:42:44'),
(97, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:42:54'),
(98, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:42:59'),
(99, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:43:01'),
(100, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:43:55'),
(101, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:43:59'),
(102, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:51:37'),
(103, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:53:21'),
(104, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:53:46'),
(105, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:55:12'),
(106, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:56:36'),
(107, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:59:06'),
(108, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:59:40'),
(109, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 08:59:48'),
(110, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-10 09:00:53'),
(111, 1, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-10 09:01:07'),
(112, 1, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-10 09:01:32'),
(113, 1, NULL, '::1', 'Melihat List Konten dengan Tags politik', '2019-05-10 09:04:55'),
(114, 1, NULL, '::1', 'Melihat List Konten dengan Tags islam', '2019-05-10 09:05:01'),
(115, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-10 09:05:17'),
(116, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 01:36:51'),
(117, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 01:38:01'),
(118, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 01:38:12'),
(119, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 01:39:38'),
(120, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 01:48:16'),
(121, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-13 01:48:25'),
(122, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-13 01:48:49'),
(123, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-13 01:48:55'),
(124, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-13 01:48:59'),
(125, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 01:53:58'),
(126, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 01:53:59'),
(127, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-13 01:54:03'),
(128, 1, 5, '::1', 'Melihat Konten Artikel Dengan Judul Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', '2019-05-13 01:55:05'),
(129, 1, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-13 01:58:35'),
(130, 1, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-13 02:14:36'),
(131, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-13 02:14:49'),
(132, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-13 02:15:38'),
(133, 1, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-13 02:15:50'),
(134, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 02:15:58'),
(135, 1, 5, '::1', 'Melihat Konten Artikel Dengan Judul Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', '2019-05-13 02:16:06'),
(136, 1, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-13 02:16:40'),
(137, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 02:28:49'),
(138, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 02:48:32'),
(139, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 02:48:33'),
(140, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 02:48:36'),
(141, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 02:53:32'),
(142, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 03:21:31'),
(143, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 03:55:34'),
(144, 1, NULL, '::1', 'List Konten Berita', '2019-05-13 03:56:00'),
(145, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-13 03:56:09'),
(146, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-13 03:56:14'),
(147, 1, NULL, '::1', 'Melihat List Konten dengan Tags search.html', '2019-05-13 04:43:49'),
(148, 1, NULL, '::1', 'Melihat List Konten dengan Tags indonesia', '2019-05-13 04:43:56'),
(149, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-13 08:38:38'),
(150, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-13 08:39:51'),
(151, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 08:39:54'),
(152, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-13 08:41:17'),
(153, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 08:41:53'),
(154, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-13 08:42:46'),
(155, 1, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-13 08:51:28'),
(156, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-13 08:53:29'),
(157, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 02:28:59'),
(158, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 02:29:06'),
(159, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 02:29:08'),
(160, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:18:46'),
(161, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:20:14'),
(162, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:20:24'),
(163, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:20:56'),
(164, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:21:14'),
(165, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:22:07'),
(166, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:46:12'),
(167, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:46:36'),
(168, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:46:37'),
(169, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:48:23'),
(170, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:48:58'),
(171, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:50:51'),
(172, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:51:58'),
(173, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:53:59'),
(174, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:54:21'),
(175, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 03:59:35'),
(176, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:00:20'),
(177, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 04:00:23'),
(178, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:00:37'),
(179, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:02:28'),
(180, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:03:28'),
(181, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:03:35'),
(182, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:05:00'),
(183, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:05:59'),
(184, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:06:05'),
(185, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:06:46'),
(186, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:06:57'),
(187, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:07:55'),
(188, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:08:04'),
(189, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:08:10'),
(190, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:09:26'),
(191, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:09:43'),
(192, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:10:58'),
(193, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:10:58'),
(194, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:31:46'),
(195, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:37:40'),
(196, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:38:16'),
(197, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 04:39:02'),
(198, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 07:06:04'),
(199, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 07:07:03'),
(200, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 07:07:30'),
(201, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 07:09:19'),
(202, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-15 07:10:05'),
(203, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:11:11'),
(204, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:47:09'),
(205, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:47:22'),
(206, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-15 07:47:25'),
(207, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:47:28'),
(208, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:48:47'),
(209, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:49:21'),
(210, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:49:40'),
(211, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:58:26'),
(212, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:59:09'),
(213, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 07:59:28'),
(214, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:02:17'),
(215, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:06:56'),
(216, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:13:56'),
(217, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:41:07'),
(218, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:41:41'),
(219, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:42:46'),
(220, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:45:44'),
(221, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:46:15'),
(222, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 08:48:41'),
(223, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:03:43'),
(224, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:05:11'),
(225, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:05:30'),
(226, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:05:37'),
(227, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:10:07'),
(228, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:10:29'),
(229, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:12:42'),
(230, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:13:11'),
(231, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:14:05'),
(232, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:14:22'),
(233, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:15:16'),
(234, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:15:28'),
(235, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:15:36'),
(236, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-15 09:16:27'),
(237, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 02:05:36'),
(238, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-16 02:36:55'),
(239, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-16 02:37:00'),
(240, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-16 02:37:05'),
(241, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-16 02:40:03'),
(242, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 04:16:55'),
(243, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 04:48:28'),
(244, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 04:48:47'),
(245, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 04:49:09'),
(246, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 04:49:50'),
(247, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 04:50:21'),
(248, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:35:13'),
(249, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:36:42'),
(250, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:40:13'),
(251, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:40:21'),
(252, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:46:13'),
(253, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:46:27'),
(254, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:46:39'),
(255, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:48:00'),
(256, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:48:04'),
(257, 1, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-16 06:51:34'),
(258, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-16 06:52:00'),
(259, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-16 06:52:15'),
(260, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-16 06:52:52'),
(261, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-16 07:03:15'),
(262, 1, NULL, '::1', 'Melihat List Konten dengan Tags muhammadiayah', '2019-05-16 07:03:56'),
(263, 1, 3, '::1', 'Melihat Konten Berita Dengan Judul MPM PP Muhammadiyah Soroti Pesoalan Buruh', '2019-05-16 07:04:02'),
(264, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-16 07:48:44'),
(265, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-16 07:50:40'),
(266, NULL, 6, '::1', 'Melihat Konten Organisasi -> Pengurus Dengan Judul Pengurus', '2019-05-16 07:52:05'),
(267, NULL, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-16 07:55:40'),
(268, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-16 07:55:51'),
(269, NULL, NULL, '::1', 'Melihat List Konten dengan Tags pemilu2019', '2019-05-16 07:56:01'),
(270, NULL, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-16 07:57:40'),
(271, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 07:57:46'),
(272, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:11:39'),
(273, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:34:40'),
(274, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:38:43'),
(275, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:39:14'),
(276, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:39:24'),
(277, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:39:40'),
(278, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:40:00'),
(279, NULL, NULL, '::1', 'List Konten Berita', '2019-05-16 08:44:55'),
(280, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-21 03:13:42'),
(281, NULL, NULL, '::1', 'Masuk / Melihat Halaman Awal BKPRMI', '2019-05-21 03:13:43'),
(282, NULL, NULL, '::1', 'Masuk Halaman Login / Resgitrasi', '2019-05-21 03:14:19'),
(283, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-21 04:00:23'),
(284, NULL, NULL, '::1', 'List Konten Berita', '2019-05-21 04:00:29'),
(285, NULL, NULL, '::1', 'List Konten Berita -> Politik', '2019-05-21 04:00:37'),
(286, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 04:01:01'),
(287, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 06:44:03'),
(288, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:12:08'),
(289, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:22:54'),
(290, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:23:39'),
(291, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:23:56'),
(292, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:24:22'),
(293, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:24:31'),
(294, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:24:55'),
(295, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:42:46'),
(296, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:43:56'),
(297, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:46:07'),
(298, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:47:09'),
(299, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:47:30'),
(300, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:47:45'),
(301, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:48:17'),
(302, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:48:38'),
(303, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:48:57'),
(304, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:49:37'),
(305, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 07:54:31'),
(306, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:00:37'),
(307, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:04:58'),
(308, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:05:10'),
(309, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:05:42'),
(310, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:06:03'),
(311, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:07:44'),
(312, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:08:10'),
(313, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:08:22'),
(314, NULL, 4, '::1', 'Melihat Konten Berita -> Politik Dengan Judul Partai Politik Diminta Kedepankan Kaderisasi Kaum Pere', '2019-05-21 08:11:23'),
(315, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-21 08:12:32'),
(316, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-21 08:13:15'),
(317, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-21 08:13:23'),
(318, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-21 08:14:02'),
(319, NULL, 1, '::1', 'Melihat Konten Organisasi -> Sejarah Dengan Judul Sejarah BKPRMI', '2019-05-21 08:14:12'),
(320, NULL, NULL, '::1', 'List Konten Artikel', '2019-05-21 08:14:28'),
(321, NULL, 5, '::1', 'Melihat Konten Artikel Dengan Judul Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', '2019-05-21 08:14:35'),
(322, NULL, 5, '::1', 'Melihat Konten Artikel Dengan Judul Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', '2019-05-21 08:45:45'),
(323, NULL, 5, '::1', 'Melihat Konten Artikel Dengan Judul Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', '2019-05-21 08:46:33'),
(324, NULL, 5, '::1', 'Melihat Konten Artikel Dengan Judul Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', '2019-05-21 08:46:46'),
(325, NULL, 5, '::1', 'Melihat Konten Artikel Dengan Judul Perspektif 5 | Orientasi Pembelajaran adalah Mengenal Allah', '2019-05-21 08:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `t_menu`
--

CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `urutan` int(11) NOT NULL,
  `nama_menu` varchar(50) NOT NULL,
  `slug_menu` varchar(100) NOT NULL,
  `page_type` int(1) NOT NULL DEFAULT '1' COMMENT '1=berita, 2=artikel, 3=html, 4=form, 5=list',
  `page_url` varchar(255) DEFAULT NULL COMMENT 'kalau ada http / https ngambil dari luar, kalau tidak ada nganbil dari nama controller nya',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0=tidak aktif, 1=aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_menu`
--

INSERT INTO `t_menu` (`id`, `parent_id`, `urutan`, `nama_menu`, `slug_menu`, `page_type`, `page_url`, `status`) VALUES
(1, 0, 1, 'Beranda', 'home', 5, 'home', 1),
(2, 0, 2, 'Organisasi', 'organisasi', 3, 'sejarah', 1),
(3, 2, 1, 'Sejarah', 'sejarah', 3, 'sejarah', 1),
(4, 2, 2, 'AD/ART BKPRMI', 'adart-bkprmi', 3, 'adart-bkprmi', 1),
(5, 2, 3, 'Pengurus', 'pengurus', 3, 'pengurus', 1),
(6, 0, 3, 'Kontak', 'kontak', 4, 'kontak', 1),
(7, 0, 6, 'Masuk / Daftar', 'masuk-daftar', 4, 'login', 1),
(8, 0, 4, 'Berita', 'berita', 1, 'berita', 1),
(9, 8, 1, 'Politik', 'politik', 1, 'politik', 1),
(10, 0, 5, 'Artikel', 'artikel', 2, 'artikel', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_setting`
--

CREATE TABLE `t_setting` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `logo` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `facebook` varchar(150) DEFAULT NULL,
  `twitter` varchar(150) DEFAULT NULL,
  `instagram` varchar(150) DEFAULT NULL,
  `google_plus` varchar(150) DEFAULT NULL,
  `youtube` varchar(150) DEFAULT NULL,
  `skype` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_setting`
--

INSERT INTO `t_setting` (`id`, `tanggal`, `logo`, `title`, `alamat`, `email`, `no_hp`, `facebook`, `twitter`, `instagram`, `google_plus`, `youtube`, `skype`) VALUES
(1, '2019-05-16 11:03:20', 'foto-20190516110320.png', 'Portal BKPRMI', 'JL. Garunggang Kulon, 192/65, Bandung, Sukabungah, Sukajadi, Bandung City, West Java 40162', 'info@bpkrmi.com', '(022) 2039080', 'https://www.facebook.com/bkprmikotabandung/', '', 'https://www.instagram.com/bkprmi_kotabandung/', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_slider`
--

CREATE TABLE `t_slider` (
  `id` int(11) NOT NULL,
  `judul` varchar(20) DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `foto` varchar(50) NOT NULL,
  `video` varchar(50) DEFAULT NULL,
  `tanggal` datetime NOT NULL,
  `publish` int(1) NOT NULL COMMENT '0=Tidak Aktif, 1=Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_slider`
--

INSERT INTO `t_slider` (`id`, `judul`, `keterangan`, `foto`, `video`, `tanggal`, `publish`) VALUES
(1, 'Beautifully Flexible', 'Looks beautiful & ultra-sharp on Retina Screen Displays. Powerful Layout with Responsive functionali', 'foto-20190515112013.jpg', 'video-20190515112013.mp4', '2019-05-15 11:20:13', 1),
(2, 'Coba Coba', 'adsad adas dasd asdas das dasd asd asdas dasd asd as dsad asas', 'foto-20190515112519.png', '', '2019-05-15 11:25:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_tag`
--

CREATE TABLE `t_tag` (
  `id` int(11) NOT NULL,
  `tag` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tag`
--

INSERT INTO `t_tag` (`id`, `tag`) VALUES
(1, 'politik'),
(2, 'pemilu2019'),
(3, 'partai'),
(4, 'parpol'),
(5, 'indonesia'),
(6, 'islam'),
(7, 'masyarakat'),
(8, 'muhammadiayah');

-- --------------------------------------------------------

--
-- Table structure for table `t_tag_relasi`
--

CREATE TABLE `t_tag_relasi` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `konten_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_tag_relasi`
--

INSERT INTO `t_tag_relasi` (`id`, `tag_id`, `konten_id`) VALUES
(19, 1, 4),
(20, 2, 4),
(21, 3, 4),
(22, 5, 4),
(23, 5, 3),
(24, 6, 3),
(25, 7, 3),
(26, 8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_users`
--

CREATE TABLE `t_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(1) NOT NULL COMMENT '1=Admin, 2=Peserta',
  `nama` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `reg_date` datetime NOT NULL,
  `act_date` datetime NOT NULL,
  `kode_aktivasi` varchar(10) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0=Tidak Aktif, 1=Aktif , 2=Block'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_users`
--

INSERT INTO `t_users` (`id`, `username`, `password`, `level`, `nama`, `email`, `no_hp`, `reg_date`, `act_date`, `kode_aktivasi`, `status`) VALUES
(1, 'admin', 'db96de53f52ba6a51c01341e6f2260a6', 1, 'indra permana', 'inda@gmail.com', '08743234123', '2019-05-02 07:26:09', '2019-05-02 07:30:22', 'bs2dpw321d', 1),
(2, 'rully', 'b29887e3798fa47bd7a3cca63cab3716', 2, 'Rully Subastian', 'subastianrully@gmail.com', '08765432134', '2019-05-03 17:23:54', '0000-00-00 00:00:00', '26e539c3d9', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_banner`
--
ALTER TABLE `t_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_komentar`
--
ALTER TABLE `t_komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kontak`
--
ALTER TABLE `t_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_konten`
--
ALTER TABLE `t_konten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_log`
--
ALTER TABLE `t_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_menu`
--
ALTER TABLE `t_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_setting`
--
ALTER TABLE `t_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_slider`
--
ALTER TABLE `t_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tag`
--
ALTER TABLE `t_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tag_relasi`
--
ALTER TABLE `t_tag_relasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_banner`
--
ALTER TABLE `t_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_komentar`
--
ALTER TABLE `t_komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_kontak`
--
ALTER TABLE `t_kontak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_konten`
--
ALTER TABLE `t_konten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_log`
--
ALTER TABLE `t_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;

--
-- AUTO_INCREMENT for table `t_menu`
--
ALTER TABLE `t_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `t_setting`
--
ALTER TABLE `t_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_slider`
--
ALTER TABLE `t_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_tag`
--
ALTER TABLE `t_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_tag_relasi`
--
ALTER TABLE `t_tag_relasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `t_users`
--
ALTER TABLE `t_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
