<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Setting</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <?php if($aksi!=""){ ?>
        <li>
          <a href="<?= base_url('setting'); ?>">Setting</a>
        </li>
        <li class="active">
          <?= $aksi." Setting"; ?>
        </li>
        <?php } else { ?>
        <li class="active">
          Setting
        </li>
        <?php } ?>
      </ol>
    </div>
  </div>
</div>

<?php if($aksi==""){ ?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Setting</h4>
      <div class="filtter-right">
        <a href="<?= base_url("setting/manage/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">Foto</th>
            <th class="text-center">Judul</th>
            <th class="text-center">Email</th>
            <th class="text-center">No Tlp</th>
            <th class="text-center">Alamat</th>
            <th class="text-center">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $no=1; 
            foreach($data_setting as $data) {
              $date = date_create($data->tanggal);
              $year = date_format($date,"Y");
              $img = "assets/upload/setting/foto/".$year."/".$data->logo;
          ?>
          <tr>
            <td style="text-align:center; vertical-align: middle;"><?= $no; ?></td>
            <td style="text-align:center;">
              <img src="<?= base_url($img); ?>" width="100" height="100"/>
            </td>
            <td style="vertical-align: middle;"><?= $data->title; ?></td>
            <td style="vertical-align: middle;"><?= $data->email ?></td>
            <td style="vertical-align: middle;"><?= $data->no_hp ?></td>
            <td style="vertical-align: middle;"><?= $data->alamat ?></td>
            <td style="vertical-align: middle;">
              <a href="<?= base_url("setting/manage/detail/".$data->id); ?>" class="btn btn-default" title="detail">
                <i class="fa fa-eye"></i>
              </a>
              <a href="<?= base_url("setting/manage/ubah/".$data->id); ?>" class="btn btn-warning">
                <i class="fa fa-pencil"></i>
              </a>
              <a href="<?= base_url("setting/manage/hapus/".$data->id); ?>" class="btn btn-danger">
                <i class="fa fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php 
} elseif($aksi=="tambah" || $aksi=="ubah" || $aksi=="hapus") {

  $judul = ""; $alamat = ""; $email = ""; $no_hp = ""; $facebook = "";
  $twitter = ""; $instagram = ""; $google_plus = ""; $youtube = "";
  $skype = ""; 
  $foto = "assets/backend/images/anonymous.png"; 
  if(count($data_setting) > 0){
    $judul = $data_setting->title;
    $alamat = $data_setting->alamat;
    $email = $data_setting->email;
    $no_hp = $data_setting->no_hp;
    $facebook = $data_setting->facebook;
    $twitter = $data_setting->twitter;
    $instagram = $data_setting->instagram;
    $google_plus = $data_setting->google_plus;
    $youtube = $data_setting->youtube;
    $skype = $data_setting->skype;

    $date = date_create($data_setting->tanggal);
    $year = date_format($date,"Y");
    $foto = "assets/upload/setting/foto/".$year."/".$data_setting->logo;
  }
  // echo "<pre>";print_r($sub_menu2_id);die;
  // die(($sub_menu2_id!="0" && $sub_menu2_id!="")? "form-sub_kategori2" : "");

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Setting"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Setting"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Setting"; }
?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title"><?= $xjudul; ?></h4>
      <hr>

      <div class="row">
        <div class="col-sm-12">

          <?php if($error!=""){ ?>
          <div class="alert alert-danger nobottommargin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon-remove-sign"></i> <?= $error; ?>
          </div>
          <?php } ?>

          <form class="form-horizontal" action="<?= base_url("setting/manage/".$aksi."/".$settingid); ?>" method="post" enctype="multipart/form-data">
          
            <div class="form-group" id="form-judul">
              <label for="judul" class="col-sm-2 control-label">Judul</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-bookmark"></i></span>
                  <input type="text" name="judul" class="form-control" placeholder="Judul" value="<?= $judul; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-email">
              <label for="judul" class="col-sm-2 control-label">Email</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                  <input type="email" name="email" class="form-control" placeholder="Email" value="<?= $email; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-no_hp">
              <label for="judul" class="col-sm-2 control-label">Nomor TLP</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-headphones"></i></span>
                  <input type="text" name="no_hp" class="form-control" placeholder="Nomot TLP" value="<?= $no_hp; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-url-alamat">
              <label for="judul" class="col-sm-2 control-label">Alamat</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-home"></i></span>
                  <input type="text" name="alamat" class="form-control" placeholder="" value="<?= $alamat; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-facebook">
              <label for="judul" class="col-sm-2 control-label">Facebook</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-facebook-square"></i></span>
                  <input type="text" name="facebook" class="form-control" placeholder="https://facebook.com/bkprmi" value="<?= $facebook; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-email">
              <label for="judul" class="col-sm-2 control-label">Twitter</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                  <input type="text" name="twitter" class="form-control" placeholder="https://twitter.com/bkprmi" value="<?= $twitter; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-email">
              <label for="judul" class="col-sm-2 control-label">Instagram</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                  <input type="text" name="instagram" class="form-control" placeholder="https://instagram.com/bkprmi" value="<?= $instagram; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-email">
              <label for="judul" class="col-sm-2 control-label">Google Plus</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                  <input type="text" name="google_plus" class="form-control" placeholder="" value="<?= $google_plus; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-email">
              <label for="judul" class="col-sm-2 control-label">YouTube</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-youtube-square"></i></span>
                  <input type="text" name="youtube" class="form-control" placeholder="https://youtube.com/channel/UCv-Mm28YD2aPFbmAhZoaZgw" value="<?= $youtube; ?>">
                </div>
              </div>
            </div>

            <div class="form-group" id="form-email">
              <label for="judul" class="col-sm-2 control-label">Skype</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-skype"></i></span>
                  <input type="text" name="skype" class="form-control" placeholder="" value="<?= $skype; ?>">
                </div>
              </div>
            </div>

            <!-- <div class="row" style="margin-bottom: 20px">
              <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2" id="form-photo">
                <img src="<?= base_url($foto); ?>" id="img-preview" class="img-thumbnail"><br>
                <div class="btn-box">	
                  <span class="btn btn-default btn-file btn-block" >
                    <i class="fa fa-pencil"></i> Upload Foto <input type="file" name="foto" id="upload-file-img">
                  </span>
                  <span>Catatan: Maximal file 2 MB</span>
                </div>
              </div>
            </div> -->

            <div class="form-group" id="form-foto">
              <label for="judul" class="col-sm-2 control-label">Gambar</label>
              <div class="col-sm-10">
                <input type="file" name="foto" class="filestyle" data-iconname="fa fa-cloud-upload">
                <span>Catatan: Maximal file 2 MB</span>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                <a href="<?= base_url("setting"); ?>" class="btn btn-default">
                  <i class="fa fa-reply"></i> Kembali
                </a> 
                <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
                  <i class="fa fa-save"></i> Submit
                </button> 
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php } else { 
  $judul = ""; $alamat = ""; $email = ""; $no_hp = ""; $facebook = "";
  $twitter = ""; $instagram = ""; $google_plus = ""; $youtube = "";
  $skype = ""; 
  $foto = "assets/backend/images/anonymous.png"; 
  if(count($data_setting) > 0){
    $judul = $data_setting->title;
    $alamat = $data_setting->alamat;
    $email = $data_setting->email;
    $no_hp = $data_setting->no_hp;
    $facebook = $data_setting->facebook;
    $twitter = $data_setting->twitter;
    $instagram = $data_setting->instagram;
    $google_plus = $data_setting->google_plus;
    $youtube = $data_setting->youtube;
    $skype = $data_setting->skype;

    $date = date_create($data_setting->tanggal);
    $year = date_format($date,"Y");
    $foto = "assets/upload/setting/foto/".$year."/".$data_setting->logo;
  }
?>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Detail Setting</h4>
      <hr>

      <div class="row">
        <div class="col-sm-3">
          <img src="<?= base_url($foto); ?>" title="<?= $judul; ?>" class="img-thumbnail">
        </div>
        <div class="col-sm-9">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon" style="
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-bookmark"></i></span>
              <input type="text" name="judul" class="form-control" placeholder="Judul" value="<?= $judul; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-envelope-o"></i></span>
              <input type="email" name="email" class="form-control" placeholder="Email" value="<?= $email; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-headphones"></i></span>
              <input type="text" name="no_hp" class="form-control" placeholder="Nomor TLP" value="<?= $no_hp; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-home"></i></span>
              <input type="text" name="alamat" class="form-control" placeholder="Alamat" value="<?= $alamat; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="background-color: #3B5998;
                color: #fff;
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-facebook"></i></span>
              <input type="text" name="facebook" class="form-control" placeholder="https://facebook.com/bkprmi" value="<?= $facebook; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="background-color: #00ACEE;
                color: #fff;
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-twitter"></i></span>
              <input type="text" name="twitter" class="form-control" placeholder="https://twitter.com/bkprmi" value="<?= $twitter; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="background-color: #3F729B;
                color: #fff;
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-instagram"></i></span>
              <input type="text" name="instagram" class="form-control" placeholder="https://instagram.com/bkprmi" value="<?= $instagram; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="background-color: #DD4B39;
                color: #fff;
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-google-plus"></i></span>
              <input type="text" name="google_plus" class="form-control" placeholder="" value="<?= $google_plus; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="background-color: #C4302B;
                color: #fff;
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-youtube"></i></span>
              <input type="text" name="youtube" class="form-control" placeholder="https://youtube.com/channel/UCv-Mm28YD2aPFbmAhZoaZgw" value="<?= $youtube; ?>" disabled>
            </div>

            <div class="input-group m-t-10">
              <span class="input-group-addon" style="background-color: #00AFF0;
                color: #fff;
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
                min-width: 38px;
              "><i class="fa fa-skype"></i></span>
              <input type="text" name="skype" class="form-control" placeholder="" value="<?= $skype; ?>" disabled>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php } ?>