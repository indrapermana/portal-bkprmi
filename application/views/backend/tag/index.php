<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Data Tag</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <li class="active">
        Data Tag
        </li>
      </ol>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Tag</h4>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Tag</th>
            <th>Judul Konten</th>
            <th>Menu</th>
            <th>Sub Menu 1</th>
            <th>Sub Menu 2</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no=1; 
          foreach($data_tag as $data) { 
          ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= $data->tag; ?></td>
            <td><?= $data->judul; ?></td>
            <td><?= $data->nama_menu; ?></td>
            <td><?= $data->sub_menu_1; ?></td>
            <td><?= $data->sub_menu_1; ?></td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>