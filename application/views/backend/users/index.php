<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Daftar Pengguna</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <li class="active">
          Daftar Pengguna
        </li>
      </ol>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Pengguna</h4>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Username</th>
            <th>Email</th>
            <th>No HP</th>
            <th>Tanggal Daftar</th>
            <th>Tanggal Aktif</th>
            <th>Kode Aktivasi</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($data_users as $user) { 
            if($user->status=="0"){
              $status = "Belum aktivasi";
            } elseif($user->status=="1") {
              $status = "Aktif";
            } else {
              $status = "Di Block";
            }
          ?>
          <tr>
            <td><?= $user->nama; ?></td>
            <td><?= $user->username; ?></td>
            <td><?= $user->email; ?></td>
            <td><?= $user->no_hp; ?></td>
            <td><?= $user->reg_date; ?></td>
            <td><?= $user->act_date; ?></td>
            <td><?= $user->kode_aktivasi; ?></td>
            <td><?= $status; ?></td>
            <td>
              <a href="#" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
              <a href="#" class="btn btn-danger"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>