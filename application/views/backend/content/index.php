<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Konten</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <?php if($aksi!=""){ ?>
        <li>
          <a href="<?= base_url('content'); ?>">Konten</a>
        </li>
        <li class="active">
          <?= $aksi." Konten"; ?>
        </li>
        <?php } else { ?>
        <li class="active">
          Konten
        </li>
        <?php } ?>
      </ol>
    </div>
  </div>
</div>

<?php if($aksi==""){ ?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Konten</h4>
      <div class="filtter-right">
        <a href="<?= base_url("content/manage/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Menu</th>
            <th>Sub Menu 1</th>
            <th>Sub Menu 2</th>
            <th>Nama Pembuat</th>
            <th>Tanggal</th>
            <th>Jumlah Baca</th>
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $no=1; 
            foreach($data_content as $data) { 
              $page_url = "";
              if($data->sub_menu_1!="" && $data->sub_menu_2=="") {
                $page_url = $data->page_url_sub_1;
              } elseif($data->sub_menu_1!="" && $data->sub_menu_2!="") {
                $page_url = $data->page_url_sub_2;
              } else {
                $page_url = $data->page_url;
              }

              $slug_menu = "";
              if($data->slug_sub_menu_1!="" && $data->slug_sub_menu_2==""){
                $slug_menu = $data->slug_sub_menu_1;
              } elseif($data->slug_sub_menu_1!="" && $data->slug_sub_menu_2!="") {
                $slug_menu = $data->slug_sub_menu_2;
              } else {
                $slug_menu = $data->slug_menu;
              }

              $url = "";
              if($data->page_type=="1" || $data->page_type=="2") {
                $date = date_create($data->tanggal);
                $year = date_format($date,"Y");
                $mount = date_format($date,"m");
                $day = date_format($date,"d");
                $url = "read/".$year."/".$mount."/".$day."/".$data->slug;
              } else {
                $url = (strstr($data->page_url, "http:") || strstr($data->page_url, "https:")) ? $data->page_url : base_url($data->page_url);
              }
          ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= $data->judul; ?></td>
            <td><?= $data->nama_menu; ?></td>
            <td><?= $data->sub_menu_1; ?></td>
            <td><?= $data->sub_menu_2; ?></td>
            <td><?= $data->nama; ?></td>
            <td><?= $data->tanggal; ?></td>
            <td><?= $data->dibaca; ?></td>
            <td><?= ($data->publish=="1")? "Publish" : "Non Publish"; ?></td>
            <td>
              <a href="<?= $url; ?>" class="btn btn-default" target="blank">
                <i class="fa fa-eye"></i>
              </a>
              <a href="<?= base_url("content/manage/ubah/".$data->id); ?>" class="btn btn-warning">
                <i class="fa fa-pencil"></i>
              </a>
              <a href="<?= base_url("content/manage/hapus/".$data->id); ?>" class="btn btn-danger">
                <i class="fa fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php 
}else{
  $menu_id = ""; $sub_menu1_id = ""; $sub_menu2_id = ""; $judul = ""; $tanggal = ""; 
  $deskripsi = ""; $video = ""; $keterangan_foto = "";  $page_type = ""; $tags = "";
  if(count($data_content) > 0){
    $menu_id = $data_content->menu_id;
    $sub_menu1_id = $data_content->sub_menu1_id;
    $sub_menu2_id = $data_content->sub_menu2_id;
    $judul = $data_content->judul;
    $tanggal = $data_content->tanggal;
    $deskripsi = $data_content->deskripsi;
    $video = $data_content->video;
    $keterangan_foto = $data_content->keterangan_foto;
    $page_type = $data_content->page_type;
  }

  if(count($data_tags) > 0){
    foreach($data_tags as $tag){
      $tags .= $tag->tag.",";
    }
  }
  // echo "<pre>";print_r($sub_menu2_id);die;
  // die(($sub_menu2_id!="0" && $sub_menu2_id!="")? "form-sub_kategori2" : "");

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Konten"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Konten"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Konten"; }
?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title"><?= $xjudul; ?></h4>
      <hr>

      <div class="row">
        <div class="col-sm-12">
          <form class="form-horizontal" action="<?= base_url("content/manage/".$aksi."/".$contentid); ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="page_type" id="page_type" value="<?= $page_type; ?>">
            <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">Menu</label>
              <div class="col-sm-10">
                <select name="menu" class="form-control" id="kategori">
                  <option value="">Pilih</option>
                  <?php foreach($data_menu as $menu){ $select = ($menu->id==$menu_id)? "selected" : ""; ?>
                  <option value="<?= $menu->id."|".$menu->page_type; ?>" <?= $select; ?>> <?= $menu->nama_menu; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="<?= (isset($data_sub_menu_1))? "" : "form-sub_kategori1"; ?>">
              <label for="judul" class="col-sm-2 control-label">Sub Kategori 1</label>
              <div class="col-sm-10">
                <select name="sub_menu1" class="form-control" id="sub_kategori1">
                  <?php
                  if(isset($data_sub_menu_1)) {
                    echo '<option value="">Pilih</option>';
                    foreach($data_sub_menu_1 as $sub_menu1) {
                      $select = ($sub_menu1->id==$sub_menu1_id)? "selected" : "";
                  ?>
                  <option value="<?= $sub_menu1->id ?>" <?= $select; ?>><?= $sub_menu1->nama_menu?></option>
                  <?php 
                    } 
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group" id="<?= (isset($data_sub_menu_2))? "" : "form-sub_kategori2"; ?>">
              <label for="judul" class="col-sm-2 control-label">Sub Kategori 2</label>
              <div class="col-sm-10">
                <select name="sub_menu2" class="form-control" id="sub_kategori2">
                  <?php
                  if(isset($data_sub_menu_2)) {
                    echo '<option value="">Pilih</option>';
                    foreach($data_sub_menu_2 as $sub_menu2) {
                      $select = ($sub_menu2->id==$sub_menu2_id)? "selected" : "";
                  ?>
                  <option value="<?= $sub_menu2->id ?>" <?= $select; ?>><?= $sub_menu2->nama_menu?></option>
                  <?php 
                    } 
                  }
                  ?>
                </select>
              </div>
            </div>
          
            <div class="form-group" id="form-judul">
              <label for="judul" class="col-sm-2 control-label">Judul</label>
              <div class="col-sm-10">
                <input type="text" name="judul" placeholder="Judul" class="form-control" value="<?= $judul; ?>">
              </div>
            </div>
            
            <div class="form-group" id="form-status">
              <label for="Status" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <select name="status" class="form-control">
                  <option value="1">Publish</option>
                  <option value="0">Non Publish</option>
                </select>
              </div>
            </div>
            <div class="form-group" id="form-tanggal">
              <label for="judul" class="col-sm-2 control-label">Tanggal</label>
              <div class="col-sm-10">
                <input type="text" name="Tanggal" id="datetime" placeholder="Tanggal" class="form-control" value="<?= $tanggal; ?>">
              </div>
            </div>
            <div class="form-group" id="form-video">
              <label for="judul" class="col-sm-2 control-label">Video</label>
              <div class="col-sm-10">
                <input type="text" name="video" class="form-control" placeholder="Url Video">
              </div>
            </div>
            <div class="form-group" id="form-foto">
              <label for="judul" class="col-sm-2 control-label">Gambar</label>
              <div class="col-sm-10">
                <input type="file" name="foto" class="filestyle" data-iconname="fa fa-cloud-upload">
              </div>
            </div>
            <div class="form-group" id="form-keterangan-foto">
              <label for="judul" class="col-sm-2 control-label">Keterangan Foto</label>
              <div class="col-sm-10">
                <input type="text" name="keterangan_foto" placeholder="Keterangan Foto" class="form-control" value="<?= $keterangan_foto; ?>">
              </div>
            </div>
            <div class="form-group" id="form-tag">
              <label for="tag" class="col-sm-2 control-label">Tag</label>
              <div class="col-sm-10 tags-default">
                <input type="text" name="tags" class="bootstrap-tagsinput" data-role="tagsinput" placeholder="add tags" value="<?= $tags; ?>"/>
              </div>
            </div>
            <div class="form-group" id="form-deskripsi">
              <label for="judul" class="col-sm-2">Deskripsi</label>
              <div class="col-sm-12">
                <textarea class="form-control" name="deskripsi" id="content"><?= $deskripsi; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <a href="<?= base_url("content"); ?>" class="btn btn-default">
                  <i class="fa fa-reply"></i> Kembali
                </a> 
                <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
                  <i class="fa fa-save"></i> Submit
                </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>