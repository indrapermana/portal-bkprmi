<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Banner</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <?php if($aksi!=""){ ?>
        <li>
          <a href="<?= base_url('banner'); ?>">Banner</a>
        </li>
        <li class="active">
          <?= $aksi." Banner"; ?>
        </li>
        <?php } else { ?>
        <li class="active">
          Banner
        </li>
        <?php } ?>
      </ol>
    </div>
  </div>
</div>

<?php if($aksi==""){ ?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Banner</h4>
      <div class="filtter-right">
        <a href="<?= base_url("banner/manage/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">Foto</th>
            <th class="text-center">Judul</th>
            <th class="text-center">Status</th>
            <th class="text-center">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $no=1; 
            foreach($data_banner as $data) {
              $date = date_create($data->tanggal);
              $year = date_format($date,"Y");
              $img = "assets/upload/banner/foto/".$year."/".$data->foto;
              $url = (strstr($data->url, "http:") || strstr($data->url, "https:")) ? $data->url : base_url($data->url);
          ?>
          <tr>
            <td style="text-align:center; vertical-align: middle;"><?= $no; ?></td>
            <td style="text-align:center;">
              <img src="<?= base_url($img); ?>" width="100" height="100"/>
            </td>
            <td style="vertical-align: middle;"><?= $data->judul; ?></td>
            <td style="text-align:center; vertical-align: middle;"><?= ($data->publish=="1")? "Publish" : "Non Publish"; ?></td>
            <td style="text-align:center; vertical-align: middle;">
              <a href="<?= $url; ?>" class="btn btn-default" target="blank">
                <i class="fa fa-eye"></i>
              </a>
              <a href="<?= base_url("banner/manage/ubah/".$data->id); ?>" class="btn btn-warning">
                <i class="fa fa-pencil"></i>
              </a>
              <a href="<?= base_url("banner/manage/hapus/".$data->id); ?>" class="btn btn-danger">
                <i class="fa fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php 
}else{
  $judul = ""; $url = ""; $status = ""; 
  $foto = "assets/backend/images/anonymous.png"; 
  if(count($data_banner) > 0){
    $judul = $data_banner->judul;
    $url = $data_banner->url;
    $status = $data_banner->publish;

    $date = date_create($data_banner->tanggal);
    $year = date_format($date,"Y");
    $foto = "assets/upload/banner/foto/".$year."/".$data_banner->foto;
  }
  // echo "<pre>";print_r($sub_menu2_id);die;
  // die(($sub_menu2_id!="0" && $sub_menu2_id!="")? "form-sub_kategori2" : "");

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data banner"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data banner"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data banner"; }
?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title"><?= $xjudul; ?></h4>
      <hr>

      <div class="row">
        <div class="col-sm-12">

          <?php if($error!=""){ ?>
          <div class="alert alert-danger nobottommargin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon-remove-sign"></i> <?= $error; ?>
          </div>
          <?php } ?>

          <form class="form-horizontal" action="<?= base_url("banner/manage/".$aksi."/".$bannerid); ?>" method="post" enctype="multipart/form-data">
          
            <div class="form-group" id="form-judul">
              <label for="judul" class="col-sm-2 control-label">Judul</label>
              <div class="col-sm-10">
                <input type="text" name="judul" placeholder="Judul" class="form-control" value="<?= $judul; ?>">
              </div>
            </div>

            <div class="form-group" id="form-url-foto">
              <label for="judul" class="col-sm-2 control-label">Url</label>
              <div class="col-sm-10">
                <input type="text" name="url" placeholder="Page URL / Controller" class="form-control" value="<?= $url; ?>">
              </div>
            </div>

            <div class="form-group" id="form-status">
              <label for="Status" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <select name="status" class="form-control">
                  <option value="1" <?php if($status=="1") echo "selected"; ?>>Publish</option>
                  <option value="0" <?php if($status=="0") echo "selected"; ?>>Non Publish</option>
                </select>
              </div>
            </div>

            <!-- <div class="row" style="margin-bottom: 20px">
              <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2" id="form-photo">
                <img src="<?= base_url($foto); ?>" id="img-preview" class="img-thumbnail"><br>
                <div class="btn-box">	
                  <span class="btn btn-default btn-file btn-block" >
                    <i class="fa fa-pencil"></i> Upload Foto <input type="file" name="foto" id="upload-file-img">
                  </span>
                  <span>Catatan: Maximal file 2 MB</span>
                </div>
              </div>
            </div> -->

            <div class="form-group" id="form-foto">
              <label for="judul" class="col-sm-2 control-label">Gambar</label>
              <div class="col-sm-10">
                <input type="file" name="foto" class="filestyle" data-iconname="fa fa-cloud-upload">
                <span>Catatan: Maximal file 2 MB</span>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                <a href="<?= base_url("banner"); ?>" class="btn btn-default">
                  <i class="fa fa-reply"></i> Kembali
                </a> 
                <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
                  <i class="fa fa-save"></i> Submit
                </button> 
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>