          <!--- Divider -->
          <div id="sidebar-menu">
            <ul>

              <li class="text-muted menu-title">Navigation</li>

              <li class="">
                <a href="<?= base_url("dashboard"); ?>" class="waves-effect"><i class="ti-bar-chart"></i> Dashboard</a>
              </li>
              
              <?php if($this->user_level == "1") { ?>
              <li class="">
                <a href="<?= base_url("users"); ?>" class="waves-effect"><i class="ti-user"></i> Daftar Pengguna</a>
              </li>
              <li class="">
                <a href="<?= base_url("menu"); ?>" class="waves-effect"><i class="ti-desktop"></i> Menu</a>
              </li>
              <?php } ?>
              
              <li class="">
                <a href="<?= base_url("content"); ?>" class="waves-effect"><i class="fa fa-file-text-o"></i> Konten</a>
              </li>

              <?php if($this->user_level == "1") { ?>
              <li class="">
                <a href="<?= base_url("setting"); ?>" class="waves-effect"><i class="fa fa-gears"></i> Setting</a>
              </li>
              <li class="">
                <a href="<?= base_url("slider"); ?>" class="waves-effect"><i class="fa fa-file-image-o"></i> Slider</a>
              </li>
              <li class="">
                <a href="<?= base_url("banner"); ?>" class="waves-effect"><i class="fa fa-file-image-o"></i> Banner</a>
              </li>
              <?php } ?>

              <li class="">
                <a href="<?= base_url("tag"); ?>" class="waves-effect"><i class="ti-bookmark"></i> Tag</a>
              </li>
              <li class="">
                <a href="<?= base_url("logs"); ?>" class="waves-effect"><i class="fa fa-file-text-o"></i> Log Pengunjung</a>
              </li>

              <li class="">
                <a href="<?= base_url("logout")?>" class="waves-effect"><i class="ti-power-off"></i> Logout</a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>