<!-- 
  * ----------------------------------------------------------------------------
  * 
  *                             Pesta Wirausaha
  *
  *                          PT. Mobi Media Mandiri
  *                      Create By Muhammad Indra Permana
  * 
  * ----------------------------------------------------------------------------
-->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="<?= base_url("assets/backend/images/logo.png"); ?>">

    <title>BKPRMI</title>

    <!-- Plugins Css-->
    <link href="<?= base_url("assets/backend/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css"); ?>" rel="stylesheet" />
    <link href="<?= base_url("assets/backend/plugins/switchery/css/switchery.min.css"); ?>" rel="stylesheet" />
    <link href="<?= base_url("assets/backend/plugins/select2/css/select2.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"); ?>" rel="stylesheet">
    
    <!-- DataTables -->
    <link href="<?= base_url("assets/backend/plugins/datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/buttons.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/fixedHeader.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/responsive.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/scroller.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/dataTables.colVis.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/dataTables.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/fixedColumns.dataTables.min.css"); ?>" rel="stylesheet" type="text/css"/>

    <!--Morris Chart CSS -->
    <link href="<?= base_url("assets/backend/plugins/morris/morris.css");?>" rel="stylesheet">

    <!-- Captcha -->
    <!-- <link type="text/css" rel="Stylesheet" href="<?php //echo CaptchaUrls::LayoutStylesheetUrl() ?>" /> -->

    <!-- Master Css -->
    <link href="<?= base_url("assets/backend/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/core.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/components.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/icons.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/pages.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/responsive.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/style.css"); ?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?= base_url("assets/backend/js/modernizr.min.js"); ?>"></script>
    <script>
    var base_url = "<?= base_url(); ?>";
    </script>

  </head>
  <body class="fixed-left">
    <div id="wrapper">
      <!-- Top Bar Start -->
      <div class="topbar">
        <?= $header; ?>
      </div>
      <!-- Top Bar End -->

      <!-- ========== Left Sidebar Start ========== -->
      <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
          <?= $sidebar; ?>
        </div>
      </div>
      <!-- Left Sidebar End -->

      <!-- ============================================================== -->
      <!-- Start right Content here -->
      <!-- ============================================================== -->                      
      <div class="content-page">
        <!-- Start content -->
        <div class="content">
          <div class="container">
            <?= $content; ?>
          </div> <!-- container -->
        </div> <!-- content -->

        <footer class="footer text-right">
          © BKPRMI 2019. All rights reserved.
        </footer>
      </div>

      
    </div>

    <script>
      var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?= base_url("assets/backend/js/jquery.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/bootstrap.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/detect.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/fastclick.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.slimscroll.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.blockUI.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/waves.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/wow.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.nicescroll.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.scrollTo.min.js");?>"></script>

    <script src="<?= base_url("assets/backend/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.bootstrap.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.buttons.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/buttons.bootstrap.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/jszip.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/pdfmake.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/vfs_fonts.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/buttons.html5.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/buttons.print.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.fixedHeader.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.keyTable.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.responsive.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/responsive.bootstrap.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.scroller.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.colVis.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.fixedColumns.min.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/pages/datatables.init.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/plugins/moment/moment.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/moment/moment-id.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/js/jquery.core.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.app.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/js/script.js"); ?>"></script>

    <!--form validation init-->
    <script src="<?= base_url("assets/backend/plugins/tinymce/tinymce.min.js"); ?>"></script>

    <script type="text/javascript">
      TableManageButtons.init();

      $(document).ready(function () {
        if($("#content").length > 0){
          tinymce.init({
            selector: "textarea#content",
            theme: "modern",
            height:300,
            plugins: [
              "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
              "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
              "save table contextmenu directionality emoticons template paste textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons", 
            style_formats: [
              {title: 'Bold text', inline: 'b'},
              {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
              {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
              {title: 'Example 1', inline: 'span', classes: 'example1'},
              {title: 'Example 2', inline: 'span', classes: 'example2'},
              {title: 'Table styles'},
              {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ]
          });
        }
			});
    </script>
  </body>
</html>
