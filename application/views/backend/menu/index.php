<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Menu</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <?php if($aksi!=""){ ?>
        <li>
          <a href="<?= base_url('menu'); ?>">Menu</a>
        </li>
        <li class="active">
          <?= $aksi." Menu"; ?>
        </li>
        <?php } else { ?>
        <li class="active">
          Menu
        </li>
        <?php } ?>
      </ol>
    </div>
  </div>
</div>

<?php if($aksi==""){ ?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Menu</h4>
      <div class="filtter-right">
        <a href="<?= base_url("menu/manage/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Parent</th>
            <th>Nama Menu</th>
            <th>Slug Menu</th>
            <th>Page Type</th>
            <th>Page URL</th>
            <!-- <th>Urutan</th> -->
            <th>Status</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no=1; 
          foreach($data_menu as $data) { 
            $page_type = "";
            if($data->page_type=="1"){
              $page_type = "Berita / NEWS";
            } elseif($data->page_type=="2") {
              $page_type = "Artikel";
            } elseif($data->page_type=="3") {
              $page_type = "HTML";
            } elseif($data->page_type=="4") {
              $page_type = "FORM";
            } elseif($data->page_type=="5"){
              $page_type = "LIST";
            }
          ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= $data->parent_id; ?></td>
            <td><?= $data->nama_menu; ?></td>
            <td><?= $data->slug_menu; ?></td>
            <td><?= $page_type; ?></td>
            <td><?= $data->page_url; ?></td>
            <!-- <td><?= $data->urutan; ?></td> -->
            <td><?= ($data->status=="1")? "Aktif" : "Tidak Aktif"; ?></td>
            <td>
              <a href="<?= base_url("menu/manage/ubah/".$data->id); ?>" class="btn btn-warning">
                <i class="fa fa-pencil"></i>
              </a>
              <a href="<?= base_url("menu/manage/hapus/".$data->id); ?>" class="btn btn-danger">
                <i class="fa fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php 
} else { 
  $parent = ""; $menu = ""; $page_type = ""; $page_url = ""; $status = "";
  if(count($data_menu)>0){
    $parent = $data_menu->parent_id;
    $menu = $data_menu->nama_menu;
    $page_type = $data_menu->page_type;
    $page_url = $data_menu->page_url;
    // $urutan = $data_menu->urutan;
    $status = $data_menu->status;
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Menu"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Menu"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Menu"; }
?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title"><?= $xjudul; ?></h4>
      <hr>

      <div class="row">
        <div class="col-sm-12">
          <form class="form-horizontal" action="<?= base_url("menu/manage/".$aksi."/".$menuid); ?>" method="post">
            <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">Menu</label>
              <div class="col-sm-10">
                <input type="text" name="menu" placeholder="Nama Menu" class="form-control" value="<?= $menu; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">Parent</label>
              <div class="col-sm-10">
                <select name="parent" class="form-control">
                  <option value="0">None</option>
                  <?php foreach($data_parent as $data){ $select = ($data->id==$parent)? "selected" : ""; ?>
                  <option value="<?= $data->id; ?>" <?= $select; ?>><?= $data->nama_menu; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">Page Type</label>
              <div class="col-sm-10">
                <select name="page_type" class="form-control">
                  <option value="1"> Berita / NEWS </option>
                  <option value="2"> Artikel </option>
                  <option value="3"> HTML </option>
                  <option value="4"> FORM </option>
                  <option value="4"> LIST </option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">URL </label>
              <div class="col-sm-10">
                <input type="text" name="page_url" placeholder="Page URL / Controller" class="form-control" value="<?= $page_url; ?>">
              </div>
            </div>
            <!-- <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">Urutan</label>
              <div class="col-sm-10">
                <input type="text" name="urutan" placeholder="0" class="form-control" value="<?= $urutan; ?>">
              </div>
            </div> -->
            <div class="form-group">
              <label for="Status" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <select name="status" class="form-control">
                  <option value="1">Aktif</option>
                  <option value="0">Tidak Aktif</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <a href="<?= base_url("menu"); ?>" class="btn btn-default">
                  <i class="fa fa-reply"></i> Kembali
                </a> 
                <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
                  <i class="fa fa-save"></i> Submit
                </button> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>