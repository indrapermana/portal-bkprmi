<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Slider</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <?php if($aksi!=""){ ?>
        <li>
          <a href="<?= base_url('slider'); ?>">Slider</a>
        </li>
        <li class="active">
          <?= $aksi." Slider"; ?>
        </li>
        <?php } else { ?>
        <li class="active">
          Slider
        </li>
        <?php } ?>
      </ol>
    </div>
  </div>
</div>

<?php if($aksi==""){ ?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Slider</h4>
      <div class="filtter-right">
        <a href="<?= base_url("slider/manage/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">Foto</th>
            <th class="text-center">Judul</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Status</th>
            <th class="text-center">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $no=1; 
            foreach($data_slider as $data) {
              $date = date_create($data->tanggal);
              $year = date_format($date,"Y");
              $img = "assets/upload/slider/foto/".$year."/".$data->foto;
              if($data->video!=""){
                $video = "assets/upload/slider/video/".$year."/".$data->video;
              }
          ?>
          <tr>
            <td style="text-align:center; vertical-align: middle;"><?= $no; ?></td>
            <td style="text-align:center; vertical-align: middle;">
              <img src="<?= base_url($img); ?>" width="100" height="50"/>
            </td>
            <td style="vertical-align: middle;"><?= $data->judul; ?></td>
            <td style="vertical-align: middle;"><?= $data->keterangan; ?></td>
            <td style="text-align:center; vertical-align: middle;"><?= ($data->publish=="1")? "Publish" : "Non Publish"; ?></td>
            <td style="text-align:center; vertical-align: middle;">
              <a href="<?= base_url("slider/manage/ubah/".$data->id); ?>" class="btn btn-warning">
                <i class="fa fa-pencil"></i>
              </a>
              <a href="<?= base_url("slider/manage/hapus/".$data->id); ?>" class="btn btn-danger">
                <i class="fa fa-trash"></i>
              </a>
            </td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php 
}else{
  $judul = ""; $keterangan = ""; $status = ""; 
  $foto = "assets/backend/images/anonymous.png"; 
  $video = "";
  if(count($data_slider) > 0){
    $judul = $data_slider->judul;
    $keterangan = $data_slider->keterangan;
    $status = $data_slider->publish;

    $date = date_create($data_slider->tanggal);
    $year = date_format($date,"Y");
    $foto = "assets/upload/slider/foto/".$year."/".$data_slider->foto;
    if($data_slider->video!=""){
      $video = "assets/upload/slider/video/".$year."/".$data_slider->video;
    }
  }
  // echo "<pre>";print_r($sub_menu2_id);die;
  // die(($sub_menu2_id!="0" && $sub_menu2_id!="")? "form-sub_kategori2" : "");

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Slider"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Slider"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Slider"; }
?>
<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title"><?= $xjudul; ?></h4>
      <hr>

      <div class="row">
        <div class="col-sm-12">

          <?php if($error!=""){ ?>
          <div class="alert alert-danger nobottommargin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon-remove-sign"></i> <?= $error; ?>
          </div>
          <?php } ?>

          <form class="form-horizontal" action="<?= base_url("slider/manage/".$aksi."/".$sliderid); ?>" method="post" enctype="multipart/form-data">
          
            <div class="form-group" id="form-judul">
              <label for="judul" class="col-sm-2 control-label">Judul</label>
              <div class="col-sm-10">
                <input type="text" name="judul" placeholder="Judul" class="form-control" value="<?= $judul; ?>">
              </div>
            </div>

            <div class="form-group" id="form-keterangan-foto">
              <label for="judul" class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-10">
                <input type="text" name="keterangan" placeholder="Keterangan" class="form-control" value="<?= $keterangan; ?>">
              </div>
            </div>

            <div class="form-group" id="form-status">
              <label for="Status" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <select name="status" class="form-control">
                  <option value="1" <?php if($status=="1") echo "selected"; ?>>Publish</option>
                  <option value="0" <?php if($status=="0") echo "selected"; ?>>Non Publish</option>
                </select>
              </div>
            </div>

            <!-- <div class="row" style="margin-bottom: 20px">
              <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2" id="form-photo">
                <img src="<?= base_url($foto); ?>" id="img-preview" class="img-thumbnail"><br>
                <div class="btn-box">	
                  <span class="btn btn-default btn-file btn-block" >
                    <i class="fa fa-pencil"></i> Upload Foto <input type="file" name="foto" id="upload-file-img">
                  </span>
                  <span>Catatan: Maximal file 2 MB</span>
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 " id="form-video">
                <video poster="<?= base_url($foto); ?>" id="video-preview-foto" muted style="width: 100%; border: 1px solid #ddd; border-radius: 5px; margin-bottom: -5px;">
                  <source src='<?= base_url($video); ?>' id="video-preview" type='video/mp4' />
                </video>

                <div class="btn-box">	
                  <span class="btn btn-default btn-file btn-block" >
                    <i class="fa fa-pencil"></i> Upload Video <input type="file" name="foto" id="upload-file-video">
                  </span>
                  <span>Catatan: Maximal file 10 MB</span>
                </div>
              </div>
            </div> -->

            <div class="form-group" id="form-foto">
              <label for="judul" class="col-sm-2 control-label">Gambar</label>
              <div class="col-sm-10">
                <input type="file" name="foto" class="filestyle" data-iconname="fa fa-cloud-upload">
                <span>Catatan: Maximal file 2 MB</span>
              </div>
            </div>

            <div class="form-group" id="form-video">
              <label for="judul" class="col-sm-2 control-label">Video</label>
              <div class="col-sm-10">
                <input type="file" name="video" class="filestyle" data-iconname="fa fa-cloud-upload">
                <span>Catatan: Maximal file 10 MB dan format video mp4</span>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-12">
                <a href="<?= base_url("slider"); ?>" class="btn btn-default">
                  <i class="fa fa-reply"></i> Kembali
                </a> 
                <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
                  <i class="fa fa-save"></i> Submit
                </button> 
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>