<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Logs Pengunjung</h4>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <li class="active">
          Logs Pengunjung
        </li>
      </ol>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card-box table-responsive">
      <h4 class="m-t-0 header-title">Daftar Log Pengunjung</h4>
      <hr>

      <table id="datatable-buttons" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama User</th>
            <th>Judul</th>
            <th>IP</th>
            <th>Keterangan</th>
            <th>Tanggal</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          $no=1; 
          foreach($data_log as $data) { 
          ?>
          <tr>
            <td><?= $no; ?></td>
            <td><?= $data->nama; ?></td>
            <td><?= $data->judul; ?></td>
            <td><?= $data->ip_address; ?></td>
            <td><?= $data->keterangan; ?></td>
            <td><?= $data->tanggal; ?></td>
          </tr>
          <?php $no++; } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>