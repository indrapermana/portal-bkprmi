<div class="col_full nobottommargin">
  <h3>Kirim Saran dan Kritik Anda atau Silahkan Menanyakan Seputar Kami</h3>

  <?php if($error!=""){ ?>
  <div class="alert alert-danger nobottommargin">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon-remove-sign"></i> <?= $error; ?>
  </div>
  <?php } ?>

  <?php if($success!=""){ ?>
  <div class="alert alert-danger nobottommargin">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon-remove-sign"></i> <?= $success; ?>
  </div>
  <?php } ?>

  <form id="register-form" name="register-form" class="nobottommargin" action="<?= site_url("kontak"); ?>" method="post">
    <input type="hidden" name="ip_address" value="<?= $ip_address; ?>">

    <div class="col_full">
      <label for="name">Nama(*):</label>
      <input type="text" id="name" name="nama" value="" placeholder="Nama (Wajib)" class="sm-form-control" tabindex="1"/>
    </div>

    <div class="clear"></div>

    <div class="col_full">
      <label for="email">Email(*):</label>
      <input type="text" id="email" name="email" value="" placeholder="Email (Wajib)" class="sm-form-control" tabindex="2"/>
    </div>

    <div class="clear"></div>

    <div class="col_full">
      <label for="website">Website:</label>
      <input type="text" id="website" name="website" value="" placeholder="Website" class="sm-form-control" tabindex="3"/>
    </div>

    <div class="clear"></div>

    <div class="col_full">
      <label for="pesan">Pesan(*):</label>
      <textarea name="pesan" cols="58" rows="7" placeholder="Pesan Anda (Wajib)" tabindex="4" class="sm-form-control"></textarea>
    </div>

    <div class="clear"></div>

    <div class="col_full nobottommargin">
      <button type="submit" class="button button-3d button-black nomargin" id="register-form-submit" name="submit" value="true">Kirim Pesan</button>
    </div>

  </form>
</div>