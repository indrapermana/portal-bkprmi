<!-- Posts
============================================= -->
<div id="posts" class="small-thumbs">

  <div class="">
    <div class="fancy-title title-border">
      <h4>Berita Terbaru</h4>
    </div>
    <?php 
      foreach($content_berita as $berita){
        $date = date_create($berita->tanggal);
        $tahun = date_format($date,"Y");
        $bulan = date_format($date,"m");
        $hari = date_format($date,"d");
        
        if($berita->foto==""){
          $img = base_url("assets/frontend/images/blog/full/1.jpg");
        }else{
          $img = base_url("assets/upload/content/".$tahun."/".$berita->foto);
        }
    ?>

    <div class="entry clearfix">
      <div class="entry-image">
        <a href="<?= $img; ?>" data-lightbox="image">
          <img class="image_fade" src="<?= $img; ?>" alt="Standard Post with Image">
        </a>
      </div>
      <div class="entry-c">
        <div class="entry-title">
          <h2><a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$berita->slug); ?>"><?= $berita->judul; ?></a></h2>
        </div>
        <ul class="entry-meta clearfix">
          <li><i class="icon-calendar3"></i> <?= set_datetime($berita->tanggal); ?></li>
          <li><a href="#"><i class="icon-user"></i> <?= $berita->nama; ?></a></li>
          <li>
            <i class="icon-folder-open"></i> 
            <a href="<?= base_url($berita->slug_menu);?>"><?= $berita->nama_menu; ?></a> 
            <?php if($berita->slug_sub_menu_1!="") { ?>
            , <a href="<?= base_url($berita->slug_sub_menu_1);?>"><?= $berita->sub_menu_1; ?></a>
            <?php } if($berita->slug_sub_menu_2!="") { ?>
            , <a href="<?= base_url($berita->slug_sub_menu_1);?>"><?= $berita->sub_menu_2; ?></a>
            <?php } ?>
          </li>
          <!-- <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
          <li><a href="#"><i class="icon-camera-retro"></i></a></li> -->
        </ul>
        <div class="entry-content">
          <p><?= substr($berita->deskripsi, 0, 250)."..."; ?></p>
          <a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$berita->slug); ?>"class="more-link">Selengkapnya</a>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  

  <div class="">
    <div class="fancy-title title-border">
      <h4>Artikel Terbaru</h4>
    </div>
    <?php 
    foreach($content_artikel as $artikel){ 
      $date = date_create($artikel->tanggal);
      $tahun = date_format($date,"Y");
      $bulan = date_format($date,"m");
      $hari = date_format($date,"d");
      
    ?>
    <div class="entry clearfix">
      <div class="entry-c">
        <div class="entry-title">
          <h2><a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$artikel->slug); ?>"><?= $artikel->judul; ?></a></h2>
        </div>
        <ul class="entry-meta clearfix">
          <li><i class="icon-calendar3"></i> <?= set_datetime($artikel->tanggal); ?></li>
          <li><a href="#"><i class="icon-user"></i> <?= $artikel->nama; ?></a></li>
          <li>
            <i class="icon-folder-open"></i> 
            <a href="<?= base_url($artikel->slug_menu);?>"><?= $artikel->nama_menu; ?></a> 
            <?php if($artikel->slug_sub_menu_1!="") { ?>
            , <a href="<?= base_url($artikel->slug_sub_menu_1);?>"><?= $artikel->sub_menu_1; ?></a>
            <?php } if($artikel->slug_sub_menu_2!="") { ?>
            , <a href="<?= base_url($artikel->slug_sub_menu_1);?>"><?= $artikel->sub_menu_2; ?></a>
            <?php } ?>
          </li>
          <!-- <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
          <li><a href="#"><i class="icon-camera-retro"></i></a></li> -->
        </ul>
        <div class="entry-content">
          <p><?= substr($artikel->deskripsi, 0, 250)."..."; ?></p>
          <a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$artikel->slug); ?>"class="more-link">Selengkapnya</a>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>

</div><!-- #posts end -->