<div class="single-post nobottommargin">
  <!-- Single Post
  ============================================= -->
  <div class="entry clearfix">
    <!-- Entry Title
    ============================================= -->
    <div class="entry-title">
      <h2><?= $konten->judul; ?></h2>
    </div><!-- .entry-title end -->

    <!-- Entry Meta
    ============================================= -->
    <ul class="entry-meta clearfix">
      <li><i class="icon-calendar3"></i> <?= set_date($konten->tanggal); ?></li>
      <li><a href="#"><i class="icon-user"></i> <?= $konten->nama; ?></a></li>
      <li>
        <i class="icon-folder-open"></i> 
        <a href="<?= base_url($konten->slug_menu);?>"><?= $konten->nama_menu; ?></a>
        <?php if($konten->slug_sub_menu_1!="") { ?>
        , <a href="<?= base_url($konten->slug_sub_menu_1);?>"><?= $konten->sub_menu_1; ?></a>
        <?php } if($konten->slug_sub_menu_2!="") { ?>
        , <a href="<?= base_url($konten->slug_sub_menu_1);?>"><?= $konten->sub_menu_2; ?></a>
        <?php } ?>
      </li>
    </ul><!-- .entry-meta end -->

    <?php if($konten->page_type=="1") { ?>
    <!-- Entry Image
    ============================================= -->
    <div class="entry-image">
      <a href="<?= $img; ?>"><img src="<?= $img; ?>" alt="Blog Single"></a>
      <div style="margin: 0 auto;
    padding: 15px 0 15px;
    font-size: 12px;
    color: #666;
    text-align: center;background-color: #f2f2f2;
    border-bottom-left-radius: 3px;border-bottom-right-radius: 3px;"><?= $konten->keterangan_foto; ?></div>
    </div><!-- .entry-image end -->
    <?php } ?>

    <!-- Entry Content
    ============================================= -->
    <div class="entry-content notopmargin">
      <?= $konten->deskripsi; ?>
      <!-- Post Single - Content End -->

      <!-- Tag Cloud
      ============================================= -->
      <div class="tagcloud clearfix bottommargin">
        <?php
        if(count($data_tags) > 0){
          foreach($data_tags as $tags){
        ?>
        <a href="<?= base_url("tags/".$tags->tag); ?>"><?= $tags->tag; ?></a>
        <?php
          }
        }else{
        ?>
        <a href="#">general</a>
        <a href="#">information</a>
        <a href="#">media</a>
        <a href="#">press</a>
        <a href="#">gallery</a>
        <a href="#">illustration</a>
        <?php } ?>
      </div><!-- .tagcloud end -->

      <div class="clear"></div>

      <!-- Post Single - Share
      ============================================= -->
      <div class="si-share noborder clearfix">
        <span>Share this Post:</span>
        <div>
          <a href="#" class="social-icon si-borderless si-facebook">
            <i class="icon-facebook"></i>
            <i class="icon-facebook"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-twitter">
            <i class="icon-twitter"></i>
            <i class="icon-twitter"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-pinterest">
            <i class="icon-pinterest"></i>
            <i class="icon-pinterest"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-gplus">
            <i class="icon-gplus"></i>
            <i class="icon-gplus"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-rss">
            <i class="icon-rss"></i>
            <i class="icon-rss"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-email3">
            <i class="icon-email3"></i>
            <i class="icon-email3"></i>
          </a>
        </div>
      </div><!-- Post Single - Share End -->
    </div>
  </div> <!-- .entry end -->

  <h4>Related Posts:</h4>

  <div class="related-posts clearfix">

    <div class="col_half nobottommargin">

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/10.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is an Image Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 10th July 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 12</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/20.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is a Video Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 24th July 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 16</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>
    
    </div>

    <div class="col_half nobottommargin col_last">

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/21.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is a Gallery Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 8th Aug 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 8</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/22.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is an Audio Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 22nd Aug 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 21</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>

    </div>
  
  </div>

  <!-- <div class="line"></div> -->

  <!-- Comments
  ============================================= -->
  <div id="comments" class="clearfix">

    <!-- Comment Form
    ============================================= -->
    <div id="respond" class="clearfix">

      <h3>Leave a <span>Comment</span></h3>

      <?php if($pesan!=""){ ?>
      <div class="alert alert-danger nobottommargin">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon-remove-sign"></i> <?= $pesan; ?>
      </div>
      <?php } ?>

      <form class="clearfix" action="" method="post" id="commentform">
        <input type="hidden" name="parent" value="0">

        <div class="col_one_third">
          <label for="author">Name</label>
          <input type="text" name="nama" id="author" value="" size="22" tabindex="1" class="sm-form-control" required/>
        </div>

        <div class="col_one_third">
          <label for="email">Email</label>
          <input type="email" name="email" id="email" value="" size="22" tabindex="2" class="sm-form-control" required/>
        </div>

        <div class="col_one_third col_last">
          <label for="url">Website</label>
          <input type="text" name="website" id="url" value="" size="22" tabindex="3" class="sm-form-control" />
        </div>

        <div class="clear"></div>

        <div class="col_full">
          <label for="comment">Comment</label>
          <textarea name="comment" cols="58" rows="7" tabindex="4" class="sm-form-control" required></textarea>
        </div>

        <div class="col_full nobottommargin">
          <button name="submit" type="submit" id="submit-button" tabindex="5" value="true" class="button button-3d nomargin">Submit Comment</button>
        </div>

      </form>

    </div><!-- #respond end -->

    <div class="line"></div>

    <h3 id="comments-title"><span><?= count($comment); ?></span> Comments</h3>
    
    <!-- Comments List
    ============================================= -->
    <?php if(count($comment)>0) { ?>

    <ol class="commentlist clearfix">
      <?php foreach($comment as $data) { ?>
      <li class="comment byuser comment-author-_smcl_admin even thread-odd thread-alt depth-1" id="li-comment-2">
        <div id="comment-2" class="comment-wrap clearfix">
          <div class="comment-meta">
            <div class="comment-author vcard">
              <span class="comment-avatar clearfix">
              <img alt='' src='<?= base_url("assets/backend/images/users.png"); ?>' class='avatar avatar-60 photo' height='60' width='60' /></span>
            </div>
          </div>

          <div class="comment-content clearfix">
            <div class="comment-author">
              <a href='#' rel='external nofollow' class='url'>
                <?= ucwords($data->nama); ?>
              </a>
              <span>
                <a href="#" title="Permalink to this comment">
                  <?= set_datetime($konten->tanggal); ?>
                </a>
              </span>
            </div>
            <p><?= $data->komentar; ?></p>
            <!-- <a class='comment-reply-link' href='#'><i class="icon-reply"></i></a> -->
          </div>

          <div class="clear"></div>
        </div>
      </li>
      <?php } ?>
    </ol>

    <?php } ?>
  </div><!-- #comments end -->

</div>