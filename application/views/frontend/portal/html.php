<?php if($konten[0]->page_type=="3") { ?>
<div class="single-post nobottommargin">
  <!-- Single Post
  ============================================= -->
  <div class="entry clearfix">
    <!-- Entry Title
    ============================================= -->
    <div class="entry-title">
      <h2><?= $konten[0]->judul; ?></h2>
    </div><!-- .entry-title end -->

    <!-- Entry Meta
    ============================================= -->
    <ul class="entry-meta clearfix">
      <li><i class="icon-calendar3"></i> <?= set_date($konten[0]->tanggal); ?></li>
      <li><a href="#"><i class="icon-user"></i> <?= $konten[0]->nama; ?></a></li>
      <li>
        <i class="icon-folder-open"></i> 
        <a href="<?= base_url($konten[0]->slug_menu);?>"><?= $konten[0]->nama_menu; ?></a>
        <?php if($konten[0]->slug_sub_menu_1!="") { ?>
        , <a href="<?= base_url($konten[0]->slug_sub_menu_1);?>"><?= $konten[0]->sub_menu_1; ?></a>
        <?php } if($konten[0]->slug_sub_menu_2!="") { ?>
        , <a href="<?= base_url($konten[0]->slug_sub_menu_1);?>"><?= $konten[0]->sub_menu_2; ?></a>
        <?php } ?>
      </li>
    </ul><!-- .entry-meta end -->

    <!-- Entry Content
    ============================================= -->
    <div class="entry-content notopmargin">
      <?= $konten[0]->deskripsi; ?>
      <!-- Post Single - Content End -->

      <!-- Tag Cloud
      ============================================= -->
      <div class="tagcloud clearfix bottommargin">
        <a href="#">general</a>
        <a href="#">information</a>
        <a href="#">media</a>
        <a href="#">press</a>
        <a href="#">gallery</a>
        <a href="#">illustration</a>
      </div><!-- .tagcloud end -->

      <div class="clear"></div>

      <!-- Post Single - Share
      ============================================= -->
      <div class="si-share noborder clearfix">
        <span>Share this Post:</span>
        <div>
          <a href="#" class="social-icon si-borderless si-facebook">
            <i class="icon-facebook"></i>
            <i class="icon-facebook"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-twitter">
            <i class="icon-twitter"></i>
            <i class="icon-twitter"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-pinterest">
            <i class="icon-pinterest"></i>
            <i class="icon-pinterest"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-gplus">
            <i class="icon-gplus"></i>
            <i class="icon-gplus"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-rss">
            <i class="icon-rss"></i>
            <i class="icon-rss"></i>
          </a>
          <a href="#" class="social-icon si-borderless si-email3">
            <i class="icon-email3"></i>
            <i class="icon-email3"></i>
          </a>
        </div>
      </div><!-- Post Single - Share End -->
    </div>
  </div> <!-- .entry end -->

  <h4>Related Posts:</h4>

  <div class="related-posts clearfix">

    <div class="col_half nobottommargin">

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/10.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is an Image Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 10th July 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 12</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/20.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is a Video Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 24th July 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 16</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>
    
    </div>

    <div class="col_half nobottommargin col_last">

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/21.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is a Gallery Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 8th Aug 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 8</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>

      <div class="mpost clearfix">
        <div class="entry-image">
          <a href="#"><img src="<?= base_url("assets/frontend/images/blog/small/22.jpg"); ?>" alt="Blog Single"></a>
        </div>
        <div class="entry-c">
          <div class="entry-title">
            <h4><a href="#">This is an Audio Post</a></h4>
          </div>
          <ul class="entry-meta clearfix">
            <li><i class="icon-calendar3"></i> 22nd Aug 2014</li>
            <li><a href="#"><i class="icon-comments"></i> 21</a></li>
          </ul>
          <div class="entry-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi perferendis.</div>
        </div>
      </div>

    </div>
  
  </div>

</div>

<?php } else if($konten[0]->page_type=="1" || $konten[0]->page_type=="2") { ?>
<!-- Posts
============================================= -->
<div id="posts" class="small-thumbs">
  <div class="">
    <div class="fancy-title title-border">
      <h4><?= ($konten[0]->page_type=="1")? "Berita" : "Artikel";?></h4>
    </div>
    <?php 
      foreach($konten as $berita){
        $date = date_create($berita->tanggal);
        $tahun = date_format($date,"Y");
        $bulan = date_format($date,"m");
        $hari = date_format($date,"d");

        if($berita->page_type=="1"){
          if($berita->foto==""){
            $img = base_url("assets/frontend/images/blog/full/1.jpg");
          }else{
            $img = base_url("assets/upload/content/".$tahun."/".$berita->foto);
          }
        }
    ?>

    <div class="entry clearfix">
      <?php if($berita->page_type=="1"){ ?>
      <div class="entry-image">
        <a href="<?= $img; ?>" data-lightbox="image">
          <img class="image_fade" src="<?= $img; ?>" alt="Standard Post with Image">
        </a>
      </div>
      <?php } ?>
      <div class="entry-c">
        <div class="entry-title">
          <h2><a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$berita->slug); ?>"><?= $berita->judul; ?></a></h2>
        </div>
        <ul class="entry-meta clearfix">
          <li><i class="icon-calendar3"></i> <?= set_date($berita->tanggal); ?></li>
          <li><a href="#"><i class="icon-user"></i> <?= $berita->nama; ?></a></li>
          <li>
            <i class="icon-folder-open"></i> 
            <a href="<?= base_url($berita->slug_menu);?>"><?= $berita->nama_menu; ?></a>
            <?php if($berita->slug_sub_menu_1!="") { ?>
            , <a href="<?= base_url($berita->slug_sub_menu_1);?>"><?= $berita->sub_menu_1; ?></a>
            <?php } if($berita->slug_sub_menu_2!="") { ?>
            , <a href="<?= base_url($berita->slug_sub_menu_1);?>"><?= $berita->sub_menu_2; ?></a>
            <?php } ?>
          </li>
          <!-- <li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
          <li><a href="#"><i class="icon-camera-retro"></i></a></li> -->
        </ul>
        <div class="entry-content">
          <p><?= substr($berita->deskripsi, 0, 200)."..."; ?></p>
          <a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$berita->slug); ?>"class="more-link">Selengkapnya</a>
        </div>
      </div>
    </div>

    <?= $paging; ?>
    <?php } ?>
  </div>
</div>
<?php }?>