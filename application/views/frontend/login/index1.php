<div class="tabs divcenter nobottommargin clearfix" id="tab-login-register">

  <ul class="tab-nav tab-nav2 center clearfix">
    <li class="inline-block"><a href="#tab-login">Login</a></li>
    <li class="inline-block"><a href="#tab-register">Register</a></li>
  </ul>

  <div class="tab-container">

    <div class="tab-content clearfix" id="tab-login" style="max-width: 500px; margin-left: auto; margin-right: auto;">
      <div class="card nobottommargin">
        <div class="card-body" style="padding: 40px;">
          <form id="login-form" name="login-form" class="nobottommargin" action="<?= site_url("login"); ?>" method="post">

            <h3>Login to your Account</h3>
            
            <?php if($pesan1!=""){ ?>
            <div class="alert alert-danger nobottommargin">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="icon-remove-sign"></i> <?= $pesan1; ?>
            </div>
            <?php } ?>

            <div class="col_full">
              <label for="login-form-username">Username:</label>
              <input type="text" id="login-form-username" name="username" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="login-form-password">Password:</label>
              <input type="password" id="login-form-password" name="password" value="" class="form-control" />
            </div>

            <div class="col_full nobottommargin">
              <button class="button button-3d button-black nomargin" id="login-form-submit" name="submit" value="true">Login</button>
              <a href="#" class="fright">Forgot Password?</a>
            </div>

          </form>
        </div>
      </div>
    </div>

    <div class="tab-content clearfix" id="tab-register">
      <div class="card nobottommargin">
        <div class="card-body" style="padding: 40px;">
          <h3>Membership Application Form</h3>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th colspan="4" style="font-size:20px; text-align:center;">Membership Benefits</th>
              </tr>
              <tr>
                <th class="text-center">Individual Membership</th>
                <th class="text-center">Bronze Package</th>
                <th class="text-center">Silver Package</th>
                <th class="text-center">Gold Package</th>
              </tr>
              <tr>
                <th class="text-center">IDR 1.000.000</th>
                <th class="text-center">IDR 10.000.000</th>
                <th class="text-center">IDR 25.000.000</th>
                <th class="text-center">IDR 50.000.000</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Invitations to normal events</td>
                <td>All in Individual Membership, PLUS</td>
                <td>All in Bronze Package, PLUS</td>
                <td>All in Silver Packege, PLUS</td>
              </tr>
              <tr>
                <td>Invitations to special member only events</td>
                <td>Logo on Website</td>
                <td>More prominent & larger logo on Website</td>
                <td>Complimentary booth space on Special Events upon request</td>
              </tr>
              <tr>
                <td>Membership discounts</td>
                <td>Members directory</td>
                <td>May distribute promotional material on Special Events upon request</td>
                <td>Complimentary host of one business or networking event per year</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td>Complimentary logo on ITS Indonesia Annual Events</td>
                <td>Main Speaker on ITS Indonesia Annual Events</td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Advertorial on Website and social media channels</td>
              </tr>
            </tbody>
          </table>

          <form id="register-form" name="register-form" class="nobottommargin" action="<?= site_url("register"); ?>" method="post">

            <div class="fancy-title title-border">
              <h4>PLEASE CHOOSE PREFERRED MEMBERSHIP TYPE</h4>
            </div>

            <div class="col_full">
              <label for="register-form-name"><strong>Corporate Membership</strong> :</label>
              <div style="padding-left: 10px;">
                <div class="radio">
                  <label>
                    <input type="radio" name="corporate" value="cm-1">
                    Bronze Package -> IDR 10.00.000
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="corporate" value="cm-2">
                    Silver Package -> IDR 25.000.000
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="corporate" value="cm-3">
                    Gold Package -> IDR 50.000.000
                  </label>
                </div>
              </div>
            </div>

            <div class="col_full">
              <label for="register-form-name"><strong>Individual Membership</strong> :</label>
              <div style="padding-left: 10px;">
                <div class="radio">
                  <label>
                    <input type="radio" name="individual" value="im-1">
                    Student -> IDR 600.000
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="individual" value="im-2">
                    Professional -> IDR 1.200.000
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="individual" value="im-3">
                    Government -> IDR 1.200.000
                  </label>
                </div>
              </div>
            </div>

            <div class="fancy-title title-border">
              <h4>PLEASE FILL IN YOUR DETAILS</h4>
            </div>

            <div class="col_full">
              <label for="register-form-name">Full Name:</label>
              <input type="text" id="register-form-name" name="register-form-name" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-name">Company:</label>
              <input type="text" id="register-form-company" name="register-form-company" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-name">Industry/Business:</label>
              <input type="text" id="register-form-industry" name="register-form-industry" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-name">Position:</label>
              <input type="text" id="register-form-position" name="register-form-position" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-name">Business Address:</label>
              <input type="text" id="register-form-address" name="register-form-address" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-name">Telp. No:</label>
              <input type="text" id="register-form-tlp" name="register-form-tlp" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-email">Email Address:</label>
              <input type="text" id="register-form-email" name="register-form-email" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-username">Choose a Username:</label>
              <input type="text" id="register-form-username" name="register-form-username" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-phone">Phone:</label>
              <input type="text" id="register-form-phone" name="register-form-phone" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-password">Choose Password:</label>
              <input type="password" id="register-form-password" name="register-form-password" value="" class="form-control" />
            </div>

            <div class="col_full">
              <label for="register-form-repassword">Re-enter Password:</label>
              <input type="password" id="register-form-repassword" name="register-form-repassword" value="" class="form-control" />
            </div>

            <div class="col_full">
            I hereby certify that the information above is true and accurate.
            I understand that the membership will automatically renew and dues become payable xx yyyy zzzz, unless a written notice withdraw has been received by xx yyyy zzzz.
            </div>

            <div class="col_full nobottommargin">
              <button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
            </div>

          </form>
        </div>
      </div>
    </div>

  </div>

</div>