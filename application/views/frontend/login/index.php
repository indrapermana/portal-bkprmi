<div class="col_one_third nobottommargin">
  <div class="well well-lg nobottommargin">
    <form id="login-form" name="login-form" class="nobottommargin" action="<?= site_url("login"); ?>" method="post">

      <h3>Masuk Ke Akun Anda</h3>
      <?php if($pesan1!=""){ ?>
      <div class="alert alert-danger nobottommargin">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon-remove-sign"></i> <?= $pesan1; ?>
      </div>
      <?php } ?>

      <div class="col_full">
        <label for="login-form-username">Username:</label>
        <input type="text" id="login-form-username" name="username" value="" class="form-control" />
      </div>

      <div class="col_full">
        <label for="login-form-password">Password:</label>
        <input type="password" id="login-form-password" name="password" value="" class="form-control" />
      </div>

      <div class="col_full nobottommargin">
        <button type="submit" class="button button-3d nomargin" id="login-form-submit" name="submit" value="true">Masuk</button>
        <a href="#" class="fright">Lupa Password?</a>
      </div>

    </form>
  </div>
</div>

<div class="col_two_third col_last nobottommargin">
  <h3>Belum punya akun? Daftar sekarang.</h3>

  <?php if($pesan2!=""){ ?>
  <div class="alert alert-danger nobottommargin">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <i class="icon-remove-sign"></i> <?= $pesan2; ?>
  </div>
  <?php } ?>

  <form id="register-form" name="register-form" class="nobottommargin" action="<?= site_url("register"); ?>" method="post">

    <div class="col_half">
      <label for="register-form-name">Nama:</label>
      <input type="text" id="register-form-name" name="nama" value="" class="form-control" />
    </div>

    <div class="col_half col_last">
      <label for="register-form-email">Email:</label>
      <input type="text" id="register-form-email" name="email" value="" class="form-control" />
    </div>

    <div class="clear"></div>

    <div class="col_half">
      <label for="register-form-username">Username:</label>
      <input type="text" id="register-form-username" name="username" value="" class="form-control" />
    </div>

    <div class="col_half col_last">
      <label for="register-form-phone">Nomor HP:</label>
      <input type="text" id="register-form-phone" name="no_hp" value="" class="form-control" />
    </div>

    <div class="clear"></div>

    <div class="col_half">
      <label for="register-form-password">Password:</label>
      <input type="password" id="register-form-password" name="password" value="" class="form-control" />
    </div>

    <div class="col_half col_last">
      <label for="register-form-repassword">Password Ulang:</label>
      <input type="password" id="register-form-repassword" name="repassword" value="" class="form-control" />
    </div>

    <div class="clear"></div>

    <div class="col_full nobottommargin">
      <button type="submit" class="button button-3d button-black nomargin" id="register-form-submit" name="submit" value="true">Daftar Sekarang</button>
    </div>

  </form>
</div>