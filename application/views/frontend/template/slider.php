      <section id="slider" class="slider-element slider-parallax swiper_wrapper clearfix">

        <div class="swiper-container swiper-parent">
          <div class="swiper-wrapper">

            <?php
              if(count($data_slider) > 0) {
                foreach($data_slider as $data){
                  $date = date_create($data->tanggal);
                  $year = date_format($date,"Y");
                  $img = "assets/upload/slider/foto/".$year."/".$data->foto;
                  
                  if($data->video!=""){
                    $video = "assets/upload/slider/video/".$year."/".$data->video;
            ?>
            
            <div class="swiper-slide dark">
              <div class="container clearfix">
                <div class="slider-caption slider-caption-center">
                  <h2 data-animate="fadeInUp"><?= $data->judul; ?></h2>
                  <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200"><?= $data->keterangan; ?></p>
                </div>
              </div>
              <div class="video-wrap">
                <video poster="<?= base_url($img); ?>" preload="auto" loop autoplay muted>
                  <source src='<?= base_url($video); ?>' type='video/mp4' />
                  <!-- <source src='<?= base_url("assets/frontend/images/videos/explore.webm"); ?>' type='video/webm' /> -->
                </video>
                <div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
              </div>
            </div>
            
            <?php
                  } else {
            ?>
            
            <div class="swiper-slide dark" style="background-image: url('<?= base_url($img); ?>');">
              <div class="container clearfix">
                <div class="slider-caption slider-caption-center">
                  <h2 data-animate="fadeInUp"><?= $data->judul; ?></h2>
                  <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200"><?= $data->keterangan; ?></p>
                </div>
              </div>
            </div>
            
            <?php
                  }
                }
              } else {
            ?>
            
            <div class="swiper-slide dark" style="background-image: url('assets/frontend/images/slider/swiper/1.jpg');">
              <div class="container clearfix">
                <div class="slider-caption slider-caption-center">
                  <h2 data-animate="fadeInUp">Welcome to Canvas</h2>
                  <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Create just what you need for your Perfect Website. Choose from a wide range of Elements &amp; simply put them on our Canvas.</p>
                </div>
              </div>
            </div>
            <div class="swiper-slide dark">
              <div class="container clearfix">
                <div class="slider-caption slider-caption-center">
                  <h2 data-animate="fadeInUp">Beautifully Flexible</h2>
                  <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Powerful Layout with Responsive functionality that can be adapted to any screen size.</p>
                </div>
              </div>
              <div class="video-wrap">
                <video poster="<?= base_url("assets/frontend/images/videos/explore.jpg"); ?>" preload="auto" loop autoplay muted>
                  <source src='<?= base_url("assets/frontend/images/videos/explore.mp4"); ?>' type='video/mp4' />
                  <!-- <source src='<?= base_url("assets/frontend/images/videos/explore.webm"); ?>' type='video/webm' /> -->
                </video>
                <div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
              </div>
            </div>
            <div class="swiper-slide" style="background-image: url('assets/frontend/images/slider/swiper/3.jpg'); background-position: center top;">
              <div class="container clearfix">
                <div class="slider-caption">
                  <h2 data-animate="fadeInUp">Great Performance</h2>
                  <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">You'll be surprised to see the Final Results of your Creation &amp; would crave for more.</p>
                </div>
              </div>
            </div>

            <?php } ?>

          </div>
          <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
          <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
          <div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
        </div>

      </section>