<!-- <pre>
  <?php //echo "<pre>";print_r(get_menu(0));die; ?>
</pre> -->
<ul>
  <?php
    $data_menu = get_menu(0);
    $html = "";
    $no = 1;
    foreach($data_menu as $menu){
      $uri = $this->uri->segment(1);
      
      if($no==1){
        $active = ($uri==$menu->slug_menu || $uri=="")? "current" : "";
        $html .= '<li class="'.$active.'"><a href="'.base_url($menu->slug_menu).'"><div>'.$menu->nama_menu.'</div></a>';  
      }else{
        if($menu->page_type=="3" && $menu->page_url==""){
          $active = ($uri==$menu->slug_menu)? "current" : "";
          $html .= '<li class="'.$active.'"><a href="'.base_url($menu->slug_menu).'"><div>'.$menu->nama_menu.'</div></a>';
        
        } elseif($menu->page_type=="3" && $menu->page_url!="") {
          $active = ($uri==$menu->page_url)? "current" : "";
          $url = (strstr($menu->page_url, "http:") || strstr($menu->page_url, "https:")) ? $menu->page_url : base_url($menu->page_url);
          $html .= '<li class="'.$active.'"><a href="'.$url.'"><div>'.$menu->nama_menu.'</div></a>';
        
        } elseif($menu->page_type=="4") {
          $active = ($uri==$menu->page_url)? "current" : "";
          $html .= '<li class="'.$active.'"><a href="'.base_url($menu->page_url).'"><div>'.$menu->nama_menu.'</div></a>';
        
        } else {
          
          $active = "";
          if($menu->page_url!=""){
            $url = $menu->page_url;
            $active = ($uri==$menu->page_url)? "current" : "";
          }else{
            $url = $menu->slug_menu;
            $active = ($uri==$menu->slug_menu)? "current" : "";
          }
          $html .= '<li class="'.$active.'"><a href="'.base_url($url).'"><div>'.$menu->nama_menu.'</div></a>';
        }
      }

      if(count($menu->sub) > 0){
        $html .= '<ul>';
        foreach($menu->sub as $sub_menu){
          if($sub_menu->page_type=="3" && $sub_menu->page_url==""){
            $html .= '<li><a href="'.base_url($sub_menu->slug_menu).'"><div>'.$sub_menu->nama_menu.'</div></a>';
          
          } elseif($sub_menu->page_type=="3" && $sub_menu->page_url!="") {
            $url_sub_1 = (strstr($sub_menu->page_url, "http:") || strstr($sub_menu->page_url, "https:"))? $sub_menu->page_url : base_url($sub_menu->page_url);
            $html .= '<li><a href="'.$url_sub_1.'"><div>'.$sub_menu->nama_menu.'</div></a>';
          
          } elseif($sub_menu->page_type=="4") {
            $html .= '<li><a href="'.base_url($sub_menu->page_url).'"><div>'.$sub_menu->nama_menu.'</div></a>';
          
          } else {
            $url = ($sub_menu->page_url!="")? $sub_menu->page_url : $sub_menu->slug_menu;
            $html .= '<li><a href="'.base_url($url).'"><div>'.$sub_menu->nama_menu.'</div></a>';
          }

          if(count($sub_menu->sub) > 0){
            $html .= '<ul>';
            foreach($sub_menu->sub as $sub_menu_2){
              if($sub_menu_2->page_type=="3" && $sub_menu_2->page_url==""){
                $html .= '<li><a href="'.base_url($sub_menu_2->slug_menu).'"><div>'.$sub_menu_2->nama_menu.'</div></a>';
              
              } elseif($sub_menu_2->page_type=="3" && $sub_menu_2->page_url!="") {
                $url_sub_2 = (strstr($sub_menu_2->page_url, "http:") || strstr($sub_menu_2->page_url, "https:"))? $sub_menu_2->page_url : base_url($sub_menu_2->page_url);
                $html .= '<li><a href="'.$url_sub_2.'"><div>'.$sub_menu_2->nama_menu.'</div></a>';
              
              } elseif($sub_menu_2->page_type=="4") {
                $html .= '<li><a href="'.base_url($sub_menu_2->page_url).'"><div>'.$sub_menu_2->nama_menu.'</div></a>';
              
              } else {
                $url = ($sub_menu_2->page_url!="")? $sub_menu_2->page_url : $sub_menu_2->slug_menu;
                $html .= '<li><a href="'.base_url($url).'"><div>'.$sub_menu_2->nama_menu.'</div></a>';
              }
    
              $html .= '</li>';
            }
            $html .= '</ul>';
          }

          $html .= '</li>';
        }
        $html .= '</ul>';
      }

      $html .= '</li>';
      $no++;
    }

    echo $html;
  ?>
</ul>