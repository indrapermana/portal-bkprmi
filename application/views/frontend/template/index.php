<!-- 
  * ----------------------------------------------------------------------------
  * 
  *                             Pesta Wirausaha
  *
  *                          PT. Mobi Media Mandiri
  *                      Create By Muhammad Indra Permana
  * 
  * ----------------------------------------------------------------------------
-->

<?php
$dates = date_create($settings->tanggal);
$year = date_format($dates,"Y");
if($settings->logo!=""){
  $img = "assets/upload/setting/foto/".$year."/".$settings->logo;
}else{
  $img = "assets/frontend/images/logo.png";
}
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
  <head>
  
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Icon -->
    <link rel="shortcut icon" href="<?= base_url($img); ?>">
    
    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/bootstrap.css"); ?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/style.css"); ?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/swiper.css"); ?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/dark.css"); ?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/font-icons.css"); ?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/animate.css"); ?>" type="text/css" />
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/magnific-popup.css"); ?>" type="text/css" />
    
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/responsive.css"); ?>" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    
    <!-- Document Title
    ============================================= -->
    <title><?= ($settings->title!="")? $settings->title : "Portal BKPRMI"; ?></title>
  
  </head>

  <body class="stretched">
    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

      <!-- Top Bar
      ============================================= -->
      <div id="top-bar" class="hidden-xs">

        <div class="container clearfix">

          <div class="col_half nobottommargin">

            <!-- Top Links
            ============================================= -->
            <div class="top-links">
              <ul>
                <li><a href="<?= base_url(); ?>">Beranda</a></li>
                <li><a href="#">FAQs</a></li>
                <li><a href="<?= base_url("kontak")?>">Kontak</a></li>
              </ul>
            </div><!-- .top-links end -->

          </div>

          <div class="col_half fright col_last nobottommargin">

            <!-- Top Social
            ============================================= -->
            <div id="top-social">
              <ul>
                <?php if($settings->facebook!="") { ?>
                <li><a href="<?= $settings->facebook; ?>" class="si-facebook" target="blank"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
                <?php } ?>

                <?php if($settings->twitter!="") { ?>
                <li><a href="<?= $settings->twitter; ?>" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
                <?php } ?>

                <?php if($settings->instagram!="") { ?>
                <li><a href="<?= $settings->instagram; ?>" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
                <?php } ?>

                <?php if($settings->google_plus!="") { ?>
                <li><a href="<?= $settings->google_plus; ?>" class="si-gplus"><span class="ts-icon"><i class="icon-gplus"></i></span><span class="ts-text">Google Plus</span></a></li>
                <?php } ?>

                <?php if($settings->youtube!="") { ?>
                <li><a href="<?= $settings->youtube; ?>" class="si-youtube"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">YouTube</span></a></li>
                <?php } ?>
                
                <?php if($settings->skype!="") { ?>
                <li><a href="<?= $settings->skype; ?>" class="si-skype"><span class="ts-icon"><i class="icon-skype"></i></span><span class="ts-text">Skype</span></a></li>
                <?php } ?>

                <?php if($settings->no_hp!="") { ?>
                <li><a href="tel:<?= $settings->no_hp; ?>" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text"><?= $settings->no_hp; ?></span></a></li>
                <?php } ?>

                <?php if($settings->email!="") { ?>
                <li><a href="mailto:<?= $settings->email?>" class="si-email3"><span class="ts-icon"><i class="icon-email3"></i></span><span class="ts-text"><?= $settings->email; ?></span></a></li>
                <?php } ?>
              </ul>
            </div><!-- #top-social end -->

          </div>

        </div>

      </div><!-- #top-bar end -->

      <!-- Header
      ============================================= -->
      <header id="header" class="sticky-style-3">

        <div class="container clearfix">

          <!-- Logo
          ============================================= -->
          <div id="logo" class="divcenter">
            <a href="<?= base_url(); ?>" class="standard-logo" data-dark-logo="<?= base_url($img); ?>"><img class="divcenter" src="<?= base_url($img); ?>" alt="BKPRMI"></a>
            <a href="<?= base_url(); ?>" class="retina-logo" data-dark-logo="<?= base_url($img); ?>"><img class="divcenter" src="<?= base_url($img); ?>" alt="BKPRMI"></a>
          </div><!-- #logo end -->

        </div>

        <div id="header-wrap">

          <!-- Primary Navigation
          ============================================= -->
          <nav id="primary-menu" class="style-2 center">

            <div class="container clearfix">

              <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

              <?= $header; ?>

              <!-- Top Search
              ============================================= -->
              <div id="top-search">
                <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                <form action="search.html" method="get">
                  <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                </form>
              </div><!-- #top-search end -->

            </div>

          </nav><!-- #primary-menu end -->

        </div>

      </header> <!-- Header End -->

      <?= (isset($home_active))? $slider : ""; ?>


      <?php if(isset($breadcrumb)) { ?>
      <!-- Page Title
      ============================================= -->
      <section id="page-title">

        <div class="container clearfix">
          <h1><?= $title; ?></h1>
          <?= $breadcrumb; ?>
        </div>

      </section><!-- #page-title end -->
      <?php } ?>

      <!-- Content
      ============================================= -->
      <section id="content">

        <div class="content-wrap">

          <div class="container clearfix">

            <?php if(isset($login_active)) { ?>

            <?= $content; ?>

            <?php } else { ?>
            
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin clearfix">
              <?= $content; ?>
            </div><!-- .postcontent end -->

            <!-- Sidebar
            ============================================= -->
            <div class="sidebar nobottommargin col_last clearfix">
              <div class="sidebar-widgets-wrap">

                <div class="widget clearfix">

                  <div class="tabs nobottommargin clearfix" id="sidebar-tabs">

                    <ul class="tab-nav clearfix">
                      <li><a href="#tabs-1">Popular</a></li>
                      <li><a href="#tabs-2">Recent</a></li>
                      <li><a href="#tabs-3">Artikel</a></li>
                    </ul>

                    <div class="tab-container">

                      <div class="tab-content clearfix" id="tabs-1">
                        <div id="popular-post-list-sidebar">
                          <?php foreach($sidebar_popoler as $populer) {
                            $date = date_create($populer->tanggal);
                            $tahun = date_format($date,"Y");
                            $bulan = date_format($date,"m");
                            $hari = date_format($date,"d");

                            if($populer->foto==""){
                              $img = base_url("assets/backend/images/anonymous.png");
                            }else{
                              $img = base_url("assets/upload/content/".$tahun."/".$populer->foto);
                            }
                          ?>
                          <div class="spost clearfix">
                            <div class="entry-image">
                              <a href="#" class="nobg">
                                <img class="rounded-circle" src="<?= $img; ?>" alt="">
                              </a>
                            </div>
                            <div class="entry-c">
                              <div class="entry-title">
                                <h4>
                                  <a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$populer->slug); ?>">
                                    <?= $populer->judul; ?>
                                  </a>
                                </h4>
                              </div>
                              <ul class="entry-meta">
                                <li><i class="icon-eye"></i> <?= $populer->dibaca; ?> lihat</li>
                              </ul>
                            </div>
                          </div>
                          <?php } ?>

                        </div>
                      </div>
                      <div class="tab-content clearfix" id="tabs-2">
                        <div id="recent-post-list-sidebar">
                          
                          <?php foreach($sidebar_terbaru as $terbaru) {
                            $date = date_create($terbaru->tanggal);
                            $tahun = date_format($date,"Y");
                            $bulan = date_format($date,"m");
                            $hari = date_format($date,"d");

                            if($terbaru->foto==""){
                              $img = base_url("assets/backend/images/anonymous.png");
                            }else{
                              $img = base_url("assets/upload/content/".$tahun."/".$terbaru->foto);
                            }
                          ?>
                          <div class="spost clearfix">
                            <div class="entry-image">
                              <a href="#" class="nobg">
                                <img class="rounded-circle" src="<?= $img; ?>" alt="">
                              </a>
                            </div>
                            <div class="entry-c">
                              <div class="entry-title">
                                <h4>
                                  <a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$terbaru->slug); ?>">
                                    <?= $terbaru->judul; ?>
                                  </a>
                                </h4>
                              </div>
                              <ul class="entry-meta">
                                <li><?= set_datetime($terbaru->tanggal); ?></li>
                              </ul>
                            </div>
                          </div>
                          <?php } ?>

                        </div>
                      </div>
                      <div class="tab-content clearfix" id="tabs-3">
                        <div id="recent-post-list-sidebar">

                          <?php foreach($sidebar_artikel_terbaru as $artikel) {
                            $date = date_create($artikel->tanggal);
                            $tahun = date_format($date,"Y");
                            $bulan = date_format($date,"m");
                            $hari = date_format($date,"d");
                          ?>
                          <div class="spost clearfix">
                            <div class="entry-c">
                              <div class="entry-title">
                                <h4>
                                  <a href="<?= base_url("read/".$tahun."/".$bulan."/".$hari."/".$artikel->slug); ?>">
                                    <?= $artikel->judul; ?>
                                  </a>
                                </h4>
                              </div>
                              <ul class="entry-meta">
                                <li><?= set_datetime($artikel->tanggal); ?></li>
                              </ul>
                            </div>
                          </div>
                          <?php } ?>

                        </div>
                      </div>

                    </div>

                  </div>

                </div>

                <div class="widget clearfix">

                  <h4>Tag Cloud</h4>
                  <div class="tagcloud">
                    <?php
                    if(count($sidebar_tag_populer) > 0){
                      foreach($sidebar_tag_populer as $tags){
                    ?>
                    <a href="<?= base_url("tags/".$tags->tag); ?>"><?= $tags->tag; ?></a>
                    <?php
                      }
                    }else{
                    ?>
                    <a href="#">general</a>
                    <a href="#">videos</a>
                    <a href="#">music</a>
                    <a href="#">media</a>
                    <a href="#">photography</a>
                    <a href="#">parallax</a>
                    <a href="#">ecommerce</a>
                    <a href="#">terms</a>
                    <a href="#">coupons</a>
                    <a href="#">modern</a>
                    <?php } ?>
                  </div>

                </div>

                <div class="widget clearfix">

                  <h4>Banner</h4>
                  <div class="text-center">
                    <?php
                      if(count($sidebar_banner) > 0){
                        foreach($sidebar_banner as $banner){
                          $date = date_create($banner->tanggal);
                          $year = date_format($date,"Y");
                          $img = base_url("assets/upload/banner/foto/".$year."/".$banner->foto);
                          $url = (strstr($banner->url, "http:") || strstr($banner->url, "https:")) ? $banner->url : base_url($banner->url);
                    ?>
                    <a href = "<?= $url?>">
                      <img src = "<?= $img; ?>" width = "250" height="250" title="<?= $banner->judul; ?>">
                    </a><br>
                    <?php
                        }
                      }
                    ?>
                  </div>

                </div>

              </div>

					  </div><!-- .sidebar end -->

          <?php } ?>

          </div>

        </div>

      </section><!-- #content end -->

      <!-- Footer
      ============================================= -->
      <footer id="footer" class="dark">

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

          <div class="container clearfix">

            <div class="col_half">
              Copyrights &copy; 2019 All Rights Reserved by Mobi.<br>
              <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
            </div>

            <div class="col_half col_last tright">
              <div class="fright clearfix">
                
                <?php if($settings->facebook!="") { ?>
                <a href="<?= $settings->facebook; ?>" class="social-icon si-small si-borderless si-facebook">
                  <i class="icon-facebook"></i>
                  <i class="icon-facebook"></i>
                </a>
                <?php } ?>

                <?php if($settings->twitter!="") { ?>
                <a href="<?= $settings->twitter; ?>" class="social-icon si-small si-borderless si-twitter">
                  <i class="icon-twitter"></i>
                  <i class="icon-twitter"></i>
                </a>
                <?php } ?>

                <?php if($settings->instagram!="") { ?>
                <a href="<?= $settings->instagram; ?>" class="social-icon si-small si-borderless si-instagram">
                  <i class="icon-instagram2"></i>
                  <i class="icon-instagram2"></i>
                </a>
                <?php } ?>

                <?php if($settings->google_plus!="") { ?>
                <a href="<?= $settings->google_plus; ?>" class="social-icon si-small si-borderless si-gplus">
                  <i class="icon-gplus"></i>
                  <i class="icon-gplus"></i>
                </a>
                <?php } ?>

                <?php if($settings->youtube!="") { ?>
                <a href="<?= $settings->youtube; ?>" class="social-icon si-small si-borderless si-youtube">
                  <i class="icon-youtube"></i>
                  <i class="icon-youtube"></i>
                </a>
                <?php } ?>

                <?php if($settings->skype!="") { ?>
                <a href="<?= $settings->skype; ?>" class="social-icon si-small si-borderless si-skype">
                  <i class="icon-skype"></i>
                  <i class="icon-skype"></i>
                </a>
                <?php } ?>
              </div>

              <div class="clear"></div>

              <i class="icon-envelope2"></i> <?= $settings->email; ?> 
              
              <span class="middot">&middot;</span> 
              <i class="icon-headphones"></i> <?= $settings->no_hp; ?>

              <?php if($settings->skype!="") { ?>
              <span class="middot">&middot;</span>
              <i class="icon-skype2"></i> Mobi Indonesia
              <?php } ?>
            </div>

          </div>

        </div><!-- #copyrights end -->

      </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script src="<?= base_url("assets/frontend/js/jquery.js"); ?>"></script>
  <script src="<?= base_url("assets/frontend/js/plugins.js"); ?>"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script src="<?= base_url("assets/frontend/js/functions.js"); ?>"></script>

  </body>
</html>