<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }

  public function index(){
    if( ! $this->session->userdata('login')){
      $pesan1 = "";
      if($this->input->post("submit")){
        $ket = "Melakukan Login";

        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|strip_tags|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|strip_tags|required');

        if($this->form_validation->run()){
          $username = $this->security->sanitize_filename($this->input->post('username'));
          $password = $this->security->sanitize_filename($this->input->post('password'));
          $check = $this->BKPRMI_Model->login($username, $password);
          $get = $check->row();
          $submit = ($this->session->userdata('submitlog'))? $this->session->userdata('submitlog') : 0;
          // echo "<pre>";print_r($get);die;
          if($check->num_rows() > 0){
            $sessdata = array(
              'id' => $get->id,
              'nama' => $get->nama,
              'level' => $get->level,
              'email' => $get->email,
              'no_hp' => $get->no_hp,
              'login' => TRUE,
            );
            //echo "<pre>";print_r($sess);die;
            $this->session->set_userdata($sessdata);
            redirect("dashboard");
          }else{
            $pesan1 = "Username atau Password Anda Salah";
            $this->session->sess_destroy();
          }
        }else{
          $pesan1 = "Mohon isi semua dengan benar";
          $this->session->sess_destroy();
        }
      }

      // Insert Log
      $ket = "Masuk Halaman Login / Resgitrasi";
      $this->BKPRMI_Model->log_insert($this->user_id, null, $this->ip_address, $ket);

      $this->mybreadcrumb->add("Home", base_url());
      $this->mybreadcrumb->add("Masuk / Daftar");
      $data['breadcrumb'] = $this->mybreadcrumb->render();
      $data['title'] = "Masuk / Daftar";
      $data['login_active'] = true;
      
      $data['pesan2'] = "";
      $data['pesan1'] = $pesan1;

      $this->template->frontend('login/index1', $data);
    }else{
      redirect("dashboard");
    }
  }

  public function register(){
    $pesan2 = "";

    // Insert Log
    $ket = "Melakukan Resgitrasi Akun Baru";
    $this->BKPRMI_Model->log_insert($this->user_id, null, $this->ip_address, $ket);

    if($this->input->post("submit")){
      $this->load->library('form_validation');
      $this->form_validation->set_rules('nama', 'Nama', 'trim|xss_clean|strip_tags|required');
      $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|strip_tags|valid_email|required');
      $this->form_validation->set_rules('no_hp', 'No HP', 'trim|xss_clean|strip_tags|required');
      $this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|strip_tags|required');
      $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|strip_tags|required');
      $this->form_validation->set_rules('repassword', 'Password', 'trim|xss_clean|strip_tags|required');

      if($this->form_validation->run()){
        $nama = $this->input->post('nama', true);
        $email = $this->input->post('email', true);
        $telepon = $this->input->post('no_hp', true);
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $repassword = $this->input->post('repassword', true);

        if($password != $repassword){
          $pesan2 = "Password anda tidak sesuai";
          $this->session->sess_destroy();
        } else {
          $user_id = $this->BKPRMI_Model->user_insert($username, $password, $nama, $email, $telepon);
          $sessdata = array(
            'id' => $user_id,
            'nama' => $nama,
            'level' => 2,
            'email' => $email,
            'no_hp' => $telepon,
            'login' => TRUE,
          );
          //echo "<pre>";print_r($sess);die;
          $this->session->set_userdata($sessdata);
          redirect("dashboard");
        }
      } else {
        $err = explode("<|>", str_replace("<p>", "", validation_errors('', '<|>')));
        $this->session->sess_destroy();
        $pesan2 = $err[0];
      }
    }

    $this->mybreadcrumb->add("Home", base_url());
    $this->mybreadcrumb->add("Login");
    $data['breadcrumb'] = $this->mybreadcrumb->render();
    $data['title'] = "Masuk";
    $data['login_active'] = true;

    $data['pesan1'] = "";
    $data['pesan2'] = $pesan2;

    $this->template->frontend('login/index1', $data);
  }

  public function logout(){
    $this->session->sess_destroy();
    redirect("login");
  }

}
