<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }

  public function index(){
    $data['data_tag'] = $this->BKPRMI_Model->get_tag_relasi();
    $this->template->backend("tag/index", $data);
  }
}