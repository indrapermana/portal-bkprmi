<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }
  
  public function read($tahun, $bulan, $tanggal, $slug){
    $data['konten'] = $this->BKPRMI_Model->get_content_by_slug($slug);
    $data['data_tags'] = $this->BKPRMI_Model->get_tag_relasi_by_konten_id($data['konten']->id);
    $pesan = "";
    // echo "<pre>";print_r($data['konten']);die;

    if($data['konten']->foto==""){
        $img = base_url("assets/frontend/images/blog/full/1.jpg");
    }else{
        $img = base_url("assets/upload/content/".$tahun."/".$data['konten']->foto);
    }
    $data['img'] = $img;

    $sub1 = "";
    if($data['konten']->slug_sub_menu_1!=""){
      $sub1 = " -> ".$data['konten']->sub_menu_1;
    }

    $sub2 = "";
    if($data['konten']->slug_sub_menu_2!=""){
      $sub1 = " -> ".$data['konten']->sub_menu_2;
    }

    $tombol = $this->input->post("submit",true);
    if($tombol!="") {
      $this->load->library('form_validation');
      $this->form_validation->set_rules('nama', 'Nama', 'trim|xss_clean|strip_tags|required');
      $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|strip_tags|valid_email|required');
      $this->form_validation->set_rules('comment', 'Comment', 'strip_tags|required');

      if($this->form_validation->run()) {
        $nama = $this->input->post("nama", true);
        $email = $this->input->post("email", true);
        $website = $this->input->post("website", true);
        $comment = $this->input->post("comment", true);
        $parent = $this->input->post("parent", true);
        
        $this->BKPRMI_Model->comment_insert($data['konten']->id, $parent, $nama, $email, $website, $comment);
      } else {
        $err = explode("<|>", str_replace("<p>", "", validation_errors('', '<|>')));
        $this->session->sess_destroy();
        $pesan = $err[0];
      }
    }

    $data['pesan'] = $pesan;
    $data['comment'] = $this->BKPRMI_Model->get_comment_by_content($data['konten']->id);

    // Insert Log
    $ket = "Melihat Konten ".$data['konten']->nama_menu.$sub1.$sub2." Dengan Judul ".$data['konten']->judul;
    $this->BKPRMI_Model->log_insert($this->user_id, $data['konten']->id, $this->ip_address, $ket);

    // Insert dibaca
    $this->BKPRMI_Model->content_dibaca($data['konten']->id);

    $this->template->frontend("portal/read", $data);
  }

  public function html($slug){
    $limit_per_page = 10;
    $page = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
    $total_records = $this->BKPRMI_Model->get_content_count_by_page_url($slug);
    $url = base_url($slug);
    $uri_segment = 2;
    
    $konten = $this->BKPRMI_Model->get_content_by_page_url($slug, $page*$limit_per_page, $limit_per_page);
    // if(count($page_url) > 0){
    $data['konten'] = $konten;
    $data['paging'] = show_paging($url, $limit_per_page, $total_records, $uri_segment);
    // die($data['paging']);
    // }else{
    //   $data['konten'] = $this->BKPRMI_Model->get_content_by_slug_menu($slug);
    // }

    // echo "<pre>";print_r($data['konten']);die;

    $this->mybreadcrumb->add("Home", base_url());
    $this->mybreadcrumb->add($data['konten'][0]->nama_menu);
    $menus = $data['konten'][0]->nama_menu;

    $sub1 = "";
    if($data['konten'][0]->slug_sub_menu_1!=""){
      $this->mybreadcrumb->add($data['konten'][0]->sub_menu_1);
      $sub1 = " -> ".$data['konten'][0]->sub_menu_1;
    }

    $sub2 = "";
    if($data['konten'][0]->slug_sub_menu_2!=""){
      $this->mybreadcrumb->add($data['konten'][0]->sub_menu_2);
      $sub2 = " -> ".$data['konten'][0]->sub_menu_2;
    }
    
    $data['breadcrumb'] = $this->mybreadcrumb->render();

    if($data['konten'][0]->page_type=="1"){
      $data['title'] = "Berita";
      $ket = "List Konten ".$menus.$sub1.$sub2;
      $content_id = null;

    } elseif($data['konten'][0]->page_type=="2") {
      $data['title'] = "Artikel";
      $ket = "List Konten ".$menus.$sub1.$sub2;
      $content_id = null;

    } else {
      if($data['konten'][0]->slug_sub_menu_1!=""){
        $data['title'] = $data['konten'][0]->sub_menu_1;
      }

      if($data['konten'][0]->slug_sub_menu_2!=""){
        $data['title'] = $data['konten'][0]->sub_menu_2;
      }

      $ket = "Melihat Konten ".$menus.$sub1.$sub2." Dengan Judul ".$data['konten'][0]->judul;
      $content_id = $data['konten'][0]->id;
    }

    // Insert Log
    $this->BKPRMI_Model->log_insert($this->user_id, $content_id, $this->ip_address, $ket);

    $this->template->frontend("portal/html", $data);
  }

  public function tags($tags){
    $limit_per_page = 10;
    $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
    $total_records = $this->BKPRMI_Model->get_tag_relasi_count_by_tag($tags);
    $url = base_url("tags/".$tags);
    $uri_segment = 3;

    $data['konten'] = $this->BKPRMI_Model->get_tag_relasi_by_tag($tags, $page*$limit_per_page, $limit_per_page);
    $data['tags'] = $tags;

    $data['paging'] = show_paging($url, $limit_per_page, $total_records, $uri_segment);

    // Insert Log
    $ket = "Melihat List Konten dengan Tags ".$tags;
    $this->BKPRMI_Model->log_insert($this->user_id, null, $this->ip_address, $ket);

    $this->template->frontend("portal/tags", $data);
  }

}