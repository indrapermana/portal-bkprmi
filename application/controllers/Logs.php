<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }
  
  public function index(){
    if($this->user_id=="1"){
      $data['data_log'] = $this->BKPRMI_Model->get_log();
    } else {
      $data['data_log'] = $this->BKPRMI_Model->get_log_by_user($this->user_id);
    }

    $this->template->backend("logs/index", $data);
  }
}