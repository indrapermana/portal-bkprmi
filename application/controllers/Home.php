<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }

  public function index(){
    
    $data['content_berita'] = $this->BKPRMI_Model->get_content_by_page_type(1);
    $data['content_artikel'] = $this->BKPRMI_Model->get_content_by_page_type(2);
    $data['data_slider'] = $this->BKPRMI_Model->get_slider_by_status(1);
    $data['home_active'] = true;

    // Insert Log
    $ket = "Masuk / Melihat Halaman Awal BKPRMI";
    $this->BKPRMI_Model->log_insert($this->user_id, null, $this->ip_address, $ket);

    $this->template->frontend("home/index", $data);
  }
}