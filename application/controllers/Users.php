<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }

  function index(){
    $data['data_users'] = $this->BKPRMI_Model->get_user_by_level(2);
    $this->template->backend("users/index", $data);
  }
}