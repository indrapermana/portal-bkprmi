<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
  private $error = "";
  private $foto = "";
  private $video = "";

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }

  private function valid_upload($fn, $path, $type){
    // Upload File
    if($fn!=""){
      // die($fn);
      if(!is_dir($path)){
        if (!mkdir($path, 0777, true)) {
          // die('Failed to create folders...');
          // $this->input->set_cookie('msg_error', "Failed to create folders...", time()+3600);
          $this->error = $type." : Failed to create folders...";
          return false;
        }else{
          if(file_exists($path)){
            chmod($path, 0777);
            if( ! $this->uploads($fn, $path, $type)){
              $err = explode("<|>", str_replace("<p>", "", $this->error));
              $error = $err[0];
              // $this->input->set_cookie('msg_error', $error, time()+3600);
              $this->error = $type." : ".$error;
              return false;
            }
            return true;
          }
        }
      }else{
        if(file_exists($path)){
          if( ! $this->uploads($fn, $path, $type)){
            $err = explode("<|>", str_replace("<p>", "", $this->error));
            $error = $err[0];
            // $this->input->set_cookie('msg_error', $error, time()+3600);
            $this->error = $type." : ".$error;
            // die($error);
            return false;
          }
          return true;
        }
      }
    }
  }

  private function uploads($fn, $path, $type){
    //$config['upload_path'] = './assets/images/slider/';
    $config['upload_path'] = $path;
    if($type=="foto"){ // foto
      $config['allowed_types'] = 'jpg|png|jpeg';
      $config['max_size']	= '2048';
    }else{ // video
      $config['allowed_types'] = 'mp4|webm|mkv';
      $config['max_size']	= '10240';
    }
    $config['remove_space'] = TRUE;
    $config['file_name'] = $fn;
    //echo "<pre>";print_r($config);die;

    $this->load->library('upload');
    $this->upload->initialize($config, FALSE);
    if($this->upload->do_upload($type)){ // name input type file
      $file = $this->upload->data();
      
      if($type=="foto"){
        $this->foto = $file['file_name'];
      }else{
        $this->video = $file['file_name'];
      }
      
      return TRUE;
    }else{
      $this->error = $type." : ".$this->upload->display_errors('', '<|>');
      return FALSE;
    }
  }
  
  public function index(){
    $this->manage();
  }

  function manage($aksi="", $sliderid=0){
    $tombol = $this->input->post("tombol",true);
    $error = "";
		if ($tombol!="") {
      $judul = $this->input->post('judul', true);
      $keterangan = $this->input->post('keterangan', true);
      $status = $this->input->post('status', true);
      $tanggal = date("Y-m-d H:i:s");
      $date = date_create($tanggal);
      $tanggals = date_format($date,"YmdHis");

      $ext_foto = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
      $fn_foto = "foto-".$tanggals.".".$ext_foto;
      $path_img = "./assets/upload/slider/foto/".date("Y")."/";

      $ext_video = strtolower(pathinfo($_FILES['video']['name'], PATHINFO_EXTENSION));
      $fn_video = "video-".$tanggals.".".$ext_video;
      $path_video = "./assets/upload/slider/video/".date("Y")."/";

      if ($aksi=="tambah") {
        if(!$this->valid_upload($fn_foto, $path_img, "foto")){
          $error = $this->error;          
        }

        $video = "";
        if($ext_video!=""){
          if(!$this->valid_upload($fn_video, $path_video, "video")){
            $error = $this->error;
          }

          $video = $fn_video;
        }

        if($error==""){
          $this->BKPRMI_Model->slider_insert($judul, $keterangan, $fn_foto, $video, $tanggal, $status);
          redirect("slider");
        }
      }

      if ($aksi=="ubah") {
        // die($ext_foto);
        if($ext_foto!="" && $ext_video!=""){
          if(!$this->valid_upload($fn_foto, $path_img, "foto")){
            $error = $this->error;          
          }

          if(!$this->valid_upload($fn_video, $path_video, "video")){
            $error = $this->error;          
          }

          if($error==""){
            $this->BKPRMI_Model->slider_update_gambar($sliderid, $fn_foto);
            $this->BKPRMI_Model->slider_update_video($sliderid, $fn_video);
            $this->BKPRMI_Model->slider_update($sliderid, $judul, $keterangan, $status);
            redirect("slider");
          }
        } elseif($ext_foto!="" && $ext_video=="") {
          if(!$this->valid_upload($fn_foto, $path_img, "foto")){
            $error = $this->error;          
          }

          if($error==""){
            $this->BKPRMI_Model->slider_update_gambar($sliderid, $fn_foto);
            $this->BKPRMI_Model->slider_update($sliderid, $judul, $keterangan, $status);
            redirect("slider");
          }
        } elseif($ext_foto=="" && $ext_video!="") {

          if(!$this->valid_upload($fn_video, $path_video, "video")){
            $error = $this->error;          
          }

          if($error==""){
            $this->BKPRMI_Model->slider_update_video($sliderid, $fn_video);
            $this->BKPRMI_Model->slider_update($sliderid, $judul, $keterangan, $status);
            redirect("slider");
          }
        } else {
          $this->BKPRMI_Model->slider_update($sliderid, $judul, $keterangan, $status);
          redirect("slider");
        }
      }
      

      if ($aksi=="hapus") { 
        $this->BKPRMI_Model->slider_delete($sliderid);
        // $this->input->set_cookie('msg_success', "Data kegiatan berhasil dihapus", time()+3600);
        redirect("slider");
      }
    }

    if($aksi==""){ 
      $data['site_form'] = "list";
      $data['data_slider'] = $this->BKPRMI_Model->get_slider();

    }else{
      $data['site_form'] = "form";
      $data['data_slider'] = $this->BKPRMI_Model->get_slider_by_id($sliderid);
    }

    $data['aksi'] = $aksi;
    $data['sliderid'] = $sliderid;
    $data['error'] = $error;
    $this->template->backend("slider/index", $data);
  }
}