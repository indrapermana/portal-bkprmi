<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }

  public function index(){
    $error = "";
    $success = "";
    $tombol = $this->input->post("submit",true);
    if ($tombol!="") {
      $this->load->library('form_validation');
      $this->form_validation->set_rules('nama', 'Nama', 'trim|xss_clean|strip_tags|required');
      $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|strip_tags|valid_email|required');
      $this->form_validation->set_rules('pesan', 'Pesan', 'trim|xss_clean|strip_tags|required');

      if($this->form_validation->run()){
        $nama = $this->input->post('nama', true);
        $email = $this->input->post('email', true);
        $website = $this->input->post('website', true);
        $pesan = $this->input->post('pesan', true);
        $ip = $this->input->post('ip_address', true);

        $this->BKPRMI_Model->kontak_insert($ip, $nama, $email, $website, $pesan);
        $success = "Pesan anda telah terkirim";
      }else{
        $err = explode("<|>", str_replace("<p>", "", validation_errors('', '<|>')));
        $this->session->sess_destroy();
        $error = $err[0];
      }
      
    }

    $data['ip_address'] = $this->ip_address;
    $data['error'] = $error;
    $data['success'] = $success;

    $this->mybreadcrumb->add("Home", base_url());
    $this->mybreadcrumb->add("Kontak");
    $data['breadcrumb'] = $this->mybreadcrumb->render();

    $data['title'] = "Kontak";

    $this->template->frontend("kontak/index", $data);
  }

  public function manage($aksi="", $kontakid=""){

  }
}