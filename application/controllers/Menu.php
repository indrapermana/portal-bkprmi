<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }
  
  public function index(){
    $this->manage();
  }

  function manage($aksi="", $menuid=0){
    $tombol = $this->input->post("tombol",true);
    if ($tombol!="") {
      $parent = $this->input->post('parent', true);
      // $urutan = $this->input->post('urutan', true);
      $menu = $this->input->post('menu', true);
      $page_type = $this->input->post('page_type', true);
      $page_url = $this->input->post('page_url', true);
      $status = $this->input->post('status', true);

      $set_title = strtolower($menu);
      $slug = url_title($set_title);

      if($page_type==""){
        $page_type = $slug;
      }

      // if($urutan==""){
        $urutan = count($this->BKPRMI_Model->get_menu_by_parent($parent))+1;
      // }

      if ($aksi=="tambah") {
        $this->BKPRMI_Model->menu_insert($parent, $urutan, $menu, $slug, $page_type, $page_url, $status);
        redirect("menu");
        // $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
      }

      if ($aksi=="ubah") {
        $this->BKPRMI_Model->menu_update($menuid, $parent, $urutan, $menu, $slug, $page_type, $page_url, $status);
        // $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
        redirect("menu");
      }

      if ($aksi=="hapus") { 
        $this->BKPRMI_Model->menu_delete($menuid); 
        // $this->input->set_cookie('msg_success', "Data kegiatan berhasil dihapus", time()+3600);
        redirect("menu");
      }
    }

    if($aksi==""){ 
      $data['site_form'] = "list";
      $data['data_menu'] = $this->BKPRMI_Model->get_menu();

    }else{
      $data['site_form'] = "form";
      $data['data_menu'] = $this->BKPRMI_Model->get_menu_by_id($menuid);
      $data['data_parent'] = $this->BKPRMI_Model->get_menu();
      // echo "<pre>"; print_r($data['data_parent']);die();
    }

    $data['aksi'] = $aksi;
    $data['menuid'] = $menuid;
    $this->template->backend("menu/index", $data);
  }

  public function getmenu($id){
    echo json_encode($this->BKPRMI_Model->get_menu_by_id($id));
  }

  public function getsubmenu($id){
    echo json_encode($this->BKPRMI_Model->get_menu_by_parent($id, false));
  }
}