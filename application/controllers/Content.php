<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }
  
  public function index(){
    $this->manage();
  }

  function manage($aksi="", $contentid=0){
    $tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
      $user_id = $this->user_id;
      $menu = $this->input->post('menu', true);
      $page_type = $this->input->post('page_type', true);
      $sub_menu1 = $this->input->post('sub_menu1', true);
      $sub_menu2 = $this->input->post('sub_menu2', true);
      $judul = $this->input->post('judul', true);
      $tanggal = $this->input->post('tanggal', true);
      $deskripsi = $this->input->post('deskripsi', true);
      $status = $this->input->post('status', true);
      $tags = explode(",", $this->input->post('tags', true));
      // echo "<pre>";print_r($tags);die;

      $video = null;
      $keterangan_foto = null;
      $fn_foto = null;

      $set_title = strtolower($judul);
      $slug = url_title($set_title);
      // die($page_type);
      
      if($page_type=="2" || $page_type=="3" || $page_type=="4"){
        if ($aksi=="tambah") {
          $content_id = $this->BKPRMI_Model->content_insert($user_id, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $fn_foto, $video, $keterangan_foto, $status);
          $this->insert_tags($tags, $content_id);

          redirect("content");
        }

        if ($aksi=="ubah") {
          $this->BKPRMI_Model->content_update($contentid, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $video, $keterangan_foto, $status);
          $this->update_tags($tags, $contentid);

          redirect("content");
        }


      }else{
        $video = $this->input->post('video', true);
        $keterangan_foto = $this->input->post('keterangan_foto', true);

        $ext_foto = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
        $fn_foto = $slug.".".$ext_foto;
        $path_img = "./assets/upload/content/".date("Y")."/";

        if ($aksi=="tambah") {
          if($this->valid_upload($fn_foto, $path_img)){
            $content_id = $this->BKPRMI_Model->content_insert($user_id, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $fn_foto, $video, $keterangan_foto, $status);
            $this->insert_tags($tags, $content_id);
            
            redirect("content");
            // $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
          }else{
            die("Upload gagal");
          }
        }

        if ($aksi=="ubah") {
          // die($ext_foto);
          if($ext_foto!=""){
            if($this->valid_upload($fn_foto, $path_img)){
              $this->BKPRMI_Model->content_update_gambar($contentid, $fn_foto);
              $this->BKPRMI_Model->content_update($contentid, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $video, $keterangan_foto, $status);

              $this->update_tags($tags, $contentid);

              // $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
              redirect("content");
            } //else{
              // $this->input->set_cookie('msg_error', "Data kegiatan gagal disimpan", time()+3600);
              // die("Upload Gagal");
            // }
          }else{
            // die($materi_event);
            $this->BKPRMI_Model->content_update($contentid, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $video, $keterangan_foto, $status);
            $this->update_tags($tags, $contentid);
            // $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
            redirect("content");
          }
        }
      }

      if ($aksi=="hapus") { 
        $this->BKPRMI_Model->content_delete($contentid);
        $this->BKPRMI_Model->tag_relasi_delete_by_konten_id($contentid);
        // $this->input->set_cookie('msg_success', "Data kegiatan berhasil dihapus", time()+3600);
        redirect("content");
      }
    }

    if($aksi==""){ 
      $data['site_form'] = "list";
      if($this->user_level=="1"){
        $data['data_content'] = $this->BKPRMI_Model->get_content();
      }else{
        $data['data_content'] = $this->BKPRMI_Model->get_content_by_user($this->user_id);
      }

    }else{
      $data['site_form'] = "form";
      $data['data_content'] = $this->BKPRMI_Model->get_content_by_id($contentid);
      $data['data_menu'] = $this->BKPRMI_Model->get_menu_content(1);
      $data['data_tags'] = $this->BKPRMI_Model->get_tag_relasi_by_konten_id($contentid);

      if(count($data['data_content']) > 0){
        if($data['data_content']->sub_menu1_id!="0" && $data['data_content']->sub_menu1_id!=""){
          $data['data_sub_menu_1'] = $this->BKPRMI_Model->get_menu_by_parent($data['data_content']->menu_id, false);
        }

        if($data['data_content']->sub_menu2_id!="0" && $data['data_content']->sub_menu2_id!=""){
          $data['data_sub_menu_2'] = $this->BKPRMI_Model->get_menu_by_parent($data['data_content']->sub_menu1_id, false);
        }
      }
    }

    $data['aksi'] = $aksi;
    $data['contentid'] = $contentid;
    $this->template->backend("content/index", $data);
  }

  private function valid_upload($fn_foto, $path_img){
    // Upload Images
    if( ! empty($_FILES['foto']['name'])){
      if(!is_dir($path_img)){
        if (!mkdir($path_img, 0777, true)) {
          // die('Failed to create folders...');
          $this->input->set_cookie('msg_error', "Failed to create folders...", time()+3600);
        }else{
          if(file_exists($path_img)){
            chmod($path_img, 0777);
            if( ! $this->uploads($fn_foto, $path_img)){
              $err = explode("<|>", str_replace("<p>", "", $this->error));
              $error = $err[0];
              $this->input->set_cookie('msg_error', $error, time()+3600);
              // die($error);
              // return [];
            }
            return true;
          }
        }
      }else{
        if(file_exists($path_img)){
          if( ! $this->uploads($fn_foto, $path_img)){
            $err = explode("<|>", str_replace("<p>", "", $this->error));
            $error = $err[0];
            $this->input->set_cookie('msg_error', $error, time()+3600);
            // die($error);
          }
          return true;
        }
      }
    }
  }

  private function uploads($fn, $path){
    //$config['upload_path'] = './assets/images/content/';
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['max_size']	= '1000';
    $config['remove_space'] = TRUE;
    $config['file_name'] = $fn;
    //echo "<pre>";print_r($config);die;

    $this->load->library('upload');
    $this->upload->initialize($config, FALSE);
    if($this->upload->do_upload('foto')){
      $img = $this->upload->data();
      $this->img = $img['file_name'];
      return TRUE;
    }else{
      $this->error = $this->upload->display_errors('', '<|>');
      return FALSE;
    }
  }

  private function insert_tags($tags, $content_id){
    for($i=0; $i<count($tags); $i++){
      $data_tag = $this->BKPRMI_Model->get_tag_by_tag($tags[$i]);
      if(count($data_tag) == 0){
        $tag_id = $this->BKPRMI_Model->tag_insert($tags[$i]);
        $this->BKPRMI_Model->tag_relasi_insert($tag_id, $content_id);
      }else{
        $this->BKPRMI_Model->tag_relasi_insert($data_tag->id, $content_id);
      }
    }
  }

  private function update_tags($tags, $contentid){
    $this->BKPRMI_Model->tag_relasi_delete_by_konten_id($contentid);
    for($i=0; $i<count($tags); $i++){
      $data_tag = $this->BKPRMI_Model->get_tag_by_tag($tags[$i]);
      if(count($data_tag) == 0){
        $tag_id = $this->BKPRMI_Model->tag_insert($tags[$i]);
        $this->BKPRMI_Model->tag_relasi_insert($tag_id, $contentid);
      }else{
        // $content_tag = $this->BKPRMI_Model->get_tag_relasi_by_konten_id($contentid);
        // if(count($content_tag) > 0){
        //   $this->BKPRMI_Model->tag_relasi_insert($data_tag->id, $contentid);
        // }else {
        $this->BKPRMI_Model->tag_relasi_insert($data_tag->id, $contentid);
        // }
      }
    }
  }

}