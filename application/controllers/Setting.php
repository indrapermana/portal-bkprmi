<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
  private $error = "";
  private $foto = "";

  public function __construct(){
    parent::__construct();
    
    // $this->load->model('BKPRMI_Model');
    $this->general->session_check();
  }

  private function valid_upload($fn, $path, $type){
    // Upload File
    if($fn!=""){
      // die($fn);
      if(!is_dir($path)){
        if (!mkdir($path, 0777, true)) {
          // die('Failed to create folders...');
          // $this->input->set_cookie('msg_error', "Failed to create folders...", time()+3600);
          $this->error = $type." : Failed to create folders...";
          return false;
        }else{
          if(file_exists($path)){
            chmod($path, 0777);
            if( ! $this->uploads($fn, $path, $type)){
              $err = explode("<|>", str_replace("<p>", "", $this->error));
              $error = $err[0];
              // $this->input->set_cookie('msg_error', $error, time()+3600);
              $this->error = $type." : ".$error;
              return false;
            }
            return true;
          }
        }
      }else{
        if(file_exists($path)){
          if( ! $this->uploads($fn, $path, $type)){
            $err = explode("<|>", str_replace("<p>", "", $this->error));
            $error = $err[0];
            // $this->input->set_cookie('msg_error', $error, time()+3600);
            $this->error = $type." : ".$error;
            // die($error);
            return false;
          }
          return true;
        }
      }
    }
  }

  private function uploads($fn, $path, $type){
    //$config['upload_path'] = './assets/images/setting/';
    $config['upload_path'] = $path;
    if($type=="foto"){ // foto
      $config['allowed_types'] = 'jpg|png|jpeg|gif';
      $config['max_size']	= '2048';
    }else{ // video
      $config['allowed_types'] = 'mp4|webm|mkv';
      $config['max_size']	= '10240';
    }
    $config['remove_space'] = TRUE;
    $config['file_name'] = $fn;
    //echo "<pre>";print_r($config);die;

    $this->load->library('upload');
    $this->upload->initialize($config, FALSE);
    if($this->upload->do_upload($type)){ // name input type file
      $file = $this->upload->data();
      
      if($type=="foto"){
        $this->foto = $file['file_name'];
      }else{
        $this->video = $file['file_name'];
      }
      
      return TRUE;
    }else{
      $this->error = $type." : ".$this->upload->display_errors('', '<|>');
      return FALSE;
    }
  }

  public function index(){
    $this->manage();
  }

  public function manage($aksi="", $settingid=0){
    $tombol = $this->input->post("tombol",true);
    $error = "";
		if ($tombol!="") {
      $judul = $this->input->post('judul', true);
      $alamat = $this->input->post('alamat', true);
      $email = $this->input->post('email', true);
      $no_hp = $this->input->post('no_hp', true);
      $facebook = $this->input->post('facebook', true);
      $twitter = $this->input->post('twitter', true);
      $instagram = $this->input->post('instagram', true);
      $google_plus = $this->input->post('google_plus', true);
      $youtube = $this->input->post('youtube', true);
      $skype = $this->input->post('skype', true);
      
      $tanggal = date("Y-m-d H:i:s");
      $date = date_create($tanggal);
      $tanggals = date_format($date,"YmdHis");

      $ext_foto = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
      $fn_foto = "foto-".$tanggals.".".$ext_foto;
      $path_img = "./assets/upload/setting/foto/".date("Y")."/";

      if ($aksi=="tambah") {
        if(!$this->valid_upload($fn_foto, $path_img, "foto")){
          $error = $this->error;          
        }

        if($error==""){
          $post = array(
            'tanggal' => $tanggal,
            'logo' => $fn_foto,
            'title' => $judul,
            'alamat' => $alamat,
            'email' => $email,
            'no_hp' => $no_hp,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'google_plus' => $google_plus,
            'youtube' => $youtube,
            'skype' => $skype
          );
          $this->BKPRMI_Model->setting_insert($post);
          redirect("setting");
        }
      }

      if ($aksi=="ubah") {
        // die($ext_foto);
        if($ext_foto!="") {
          if(!$this->valid_upload($fn_foto, $path_img, "foto")){
            $error = $this->error;          
          }

          if($error==""){
            $post = array(
              'tanggal' => $tanggal,
              'title' => $judul,
              'alamat' => $alamat,
              'email' => $email,
              'no_hp' => $no_hp,
              'facebook' => $facebook,
              'twitter' => $twitter,
              'instagram' => $instagram,
              'google_plus' => $google_plus,
              'youtube' => $youtube,
              'skype' => $skype
            );
            $this->BKPRMI_Model->setting_update_gambar($settingid, $fn_foto);
            $this->BKPRMI_Model->setting_update($settingid, $post);
            redirect("setting");
          }
        } else {
          $post = array(
            'tanggal' => $tanggal,
            'title' => $judul,
            'alamat' => $alamat,
            'email' => $email,
            'no_hp' => $no_hp,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'instagram' => $instagram,
            'google_plus' => $google_plus,
            'youtube' => $youtube,
            'skype' => $skype
          );
          $this->BKPRMI_Model->setting_update($settingid, $post);
          redirect("setting");
        }
      }
      

      if ($aksi=="hapus") { 
        $this->BKPRMI_Model->setting_delete($settingid);
        // $this->input->set_cookie('msg_success', "Data kegiatan berhasil dihapus", time()+3600);
        redirect("setting");
      }
    }

    if($aksi==""){ 
      $data['site_form'] = "list";
      $data['data_setting'] = $this->BKPRMI_Model->get_setting();

    }else{
      $data['site_form'] = "form";
      $data['data_setting'] = $this->BKPRMI_Model->get_setting_by_id($settingid);
    }

    $data['aksi'] = $aksi;
    $data['settingid'] = $settingid;
    $data['error'] = $error;
    $this->template->backend("setting/index", $data);
  }
}