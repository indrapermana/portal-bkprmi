<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Validation{
    protected $_ci;
	public $error;

    function __construct(){
        $this->_ci =& get_instance();	
    }

	public function valid_email($filed)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $filed)) ? FALSE : TRUE;
	}

	public function numeric($str){
		return (preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $str)) ? TRUE : FALSE;
	}

	function checkData($filed, $label = "", $rules = ""){
		$rule = implode('|', $rules);
		if (in_array('isset', $rule, TRUE) OR in_array('required', $rule)){
			if ( ! is_array($filed)){
				$cek = (trim($filed) == '') ? FALSE : TRUE;
				if ($cek){
					return TRUE;
				}else{
					$this->error = $label." tidak boleh di kosongkan";
					return FALSE;
				}
			}else{
				if (! empty($filed)){
					return TRUE;
				}else{
					$this->error = $label." tidak boleh di kosongkan";
					return FALSE;
				}
			}
		}

		if(in_array('numeric', $rule)){
			if (preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $filed)){
				return TRUE;
			}else{
				$this->error = $label." hanya boleh di isi dengan angka";
				return FALSE;
			}
		}

		if(in_array('alpha_space', $rule)){
			if (! preg_match("/^([a-z ])+$/i", $filed)){
				$this->error = $label." hanya boleh di isi dengan huruf dan spasi";
				return FALSE;
			}else{
				return TRUE;
			}
		}

		if(in_array('yyyymmdd', $rule)){
			if( ! strstr($filed, ".")){
				$this->error = $label." harus menggunakan format '<b>dd.mm.yyyy</b>'";
				return FALSE;
			}else{
				$dess = explode(".", $filed);
				$year = strlen($dess[2]);
				$month = strlen($dess[1]);
				$day = strlen($dess[0]);
				
				if($year == 4 && $month == 2 && $day == 2){
					return TRUE;
				}else{
				$this->error = $label." harus menggunakan format '<b>dd.mm.yyyy</b>'";
					return FALSE;
				}
			}
		}
		
		if(in_array('hhmm', $rule)){
			if( ! strstr($filed, ";")){
				$this->error = $label." harus menggunakan format '<b>hh;mm</b>'";
				return FALSE;
			}else{
				$dess = explode(";", $filed);
				$m = strlen($dess[1]);
				$h = strlen($dess[0]);
				
				if($m == 2 && $h == 2){
					return TRUE;
				}else{
					$this->error = $label." harus menggunakan format '<b>hh;mm</b>'";
					return FALSE;
				}
			}
		}
		
		if(in_array('encode_tags', $rule)){
			return str_replace(array('<', '>'),  array('&lt;', '&gt;'), $filed);
		}
	}
}
