<?php

class BKPRMI_Model extends CI_Model {
  
  function __construct(){
    parent::__construct();
  }

  // Login
  function login($user, $pass){
    $mpass = $this->user_pass($pass);
    // die($mpass);
    $sql = "select id, nama, email, level, status from t_users 
      where username = ? and password = ? and status = 1";
    $query = $this->db->query($sql,array($user,$mpass));
    return $query;
  }

  // Password Login
  function user_pass($pass) {
    return md5("p35T4$$".$pass."##w1R4u5^#4");
  }

  /* ------------ User Start ----------- */
  function get_user(){
    $sql = "select * from t_users order by id desc";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_user_by_id($id){
    $sql = "select * from t_users where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result();
  }

  function get_user_by_level($level){
    $sql = "select * from t_users where level = ? order by id desc";
    $query = $this->db->query($sql, array($level));
    return $query->result();
  }

  function check_email($email){
    $sql = "select * from t_users where email = ? and level = 2";
    $query = $this->db->query($sql, array($email));
    return $query->result();
  }

  //activate user account
  function verifyEmailID($key){
    $data = array('status' => 1);
    $this->db->where('kode_aktivasi', $key);
    return $this->db->update('t_users', $data);
  }

  function user_insert($username, $passwd, $nama, $email, $telepon){
    $passwd = $this->user_pass($passwd);
    $sql = "insert into t_users (id, username, password, nama, email, no_hp, reg_date, kode_aktivasi, level, status) values (null, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($username, $passwd, $nama, $email, $telepon, date('Y-m-d H:i:s'), substr(md5($email), 0, 10), 2, 0));
  
    $sql = "select last_insert_id() as id";
		$query = $this->db->query($sql);
		return $query->row()->id;
  }

  function user_update($id, $username, $nama, $email, $telepon){
    $sql = "update t_users set username = ?, nama = ?, email = ?, no_hp = ? where id = ?";
    $this->db->query($sql, array($username, $nama, $email, $telepon, $id));
  }

  function user_change_password($id, $password){
    $password = $this->user_pass($password);
    $sql = "update t_users set password = ? where id = ?";
    $this->db->query($sql, array($password, $id));
  }

  function user_change_status($id, $status){
    $sql = "update t_users set status = ? where id = ?";
    $this->db->query($sql, array($status, $id));
  }
  /* ------------ User End ----------- */

  /* ------------ Kategori Start ----------- */
  function get_menu(){
    $sql = "select * from t_menu";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_menu_by_id($id){
    $sql = "select * from t_menu where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function get_menu_by_status($status){
    $sql = "select * from t_menu where status = ?";
    $query = $this->db->query($sql, array($status));
    return $query->result();
  }

  function get_menu_content($status){
    $sql = "select * from t_menu where parent_id = 0 and page_type in (1,2,3) and status = ?";
    $query = $this->db->query($sql, array($status));
    return $query->result();
  }

  function get_menu_by_parent($parent, $array = true){
    $sql = "select * from t_menu where parent_id = ? and status = 1 order by urutan asc";
    $query = $this->db->query($sql, array($parent));
    if($array){
      return $query->result_array();
    }else{
      return $query->result();
    }
  }

  function menu_insert($parent, $urutan, $nama_menu, $slug, $page_type, $page_url, $status){
    $sql = "insert into t_menu (id, parent_id, urutan, nama_menu, slug_menu, page_type, page_url, status) values (null, ?, ?, ?, ?, ?, ?, ?)";
    $query = $this->db->query($sql, array($parent, $urutan, $nama_menu, $slug, $page_type, $page_url, $status));
  }

  function menu_update($id, $parent, $urutan, $nama_menu, $slug, $page_type, $page_url, $status){
    $sql = "update t_menu set parent_id = ?, urutan = ?, nama_menu = ?, slug_menu = ?, page_type = ?, page_url = ?, status = ? where id = ?";
    $query = $this->db->query($sql, array($parent, $urutan, $nama_menu, $slug, $page_type, $page_url, $status, $id));
  }

  function menu_delete($id){
    $sql = "delete from t_menu where id = ?";
    $query = $this->db->query($sql, array($id));
  }
  /* ------------ Kategori End ----------- */

  /* ------------ Content Start ----------- */
  function get_content(){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1, a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
    e.slug_menu AS slug_sub_menu_2, c.page_type, c.page_url, d.page_url AS page_url_sub_1, e.page_url AS page_url_sub_2, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca, a.publish
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    ORDER BY a.id DESC";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_content_by_id($id){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1,
      a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
      e.slug_menu AS slug_sub_menu_2, c.page_type, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE a.id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function get_content_by_slug($slug){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1, a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
      e.slug_menu AS slug_sub_menu_2, c.page_type, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE a.slug = ?";
    $query = $this->db->query($sql, array($slug));
    return $query->row();
  }

  function get_content_by_slug_menu($slug_menu, $start = "0", $per_page = "10"){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1, a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
      e.slug_menu AS slug_sub_menu_2, c.page_type, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE c.slug_menu = ? OR d.slug_menu = ? OR e.slug_menu = ?
    LIMIT ?, ?";
    $query = $this->db->query($sql, array($slug_menu, $slug_menu, $slug_menu, $start, $per_page));
    return $query->result();
  }

  function get_content_count_by_slug_menu($slug_menu){
    $sql = "SELECT COUNT(a.*) AS total
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE c.slug_menu = ? OR d.slug_menu = ? OR e.slug_menu = ?";
    $query = $this->db->query($sql, array($slug_menu, $slug_menu, $slug_menu));
    return $query->row()->total;
  }

  function get_content_by_page_url($slug_menu, $start = "0", $per_page = "10"){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1, a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
      e.slug_menu AS slug_sub_menu_2, c.page_type, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE c.page_url = ? OR d.page_url = ? OR e.page_url = ?
    LIMIT ?, ?";
    $query = $this->db->query($sql, array($slug_menu, $slug_menu, $slug_menu, $start, $per_page));
    return $query->result();
  }

  function get_content_count_by_page_url($slug_menu){
    $sql = "SELECT COUNT(*) AS total
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE c.page_url = ? OR d.page_url = ? OR e.page_url = ?";
    $query = $this->db->query($sql, array($slug_menu, $slug_menu, $slug_menu));
    return $query->row()->total;
  }

  function get_content_by_status($status){
    $sql = "SELECT a.id, b.nama, c.nama_menu, d.nama_menu AS sub_menu_1, e.nama_menu AS sub_menu_2, a.judul, 
      a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1, e.slug_menu AS slug_sub_menu_2, 
      a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id 
    WHERE a.status = ? order by a.id desc";
    $query = $this->db->query($sql, array($status));
    return $query->result();
  }

  function get_content_by_kategori($kategori){
    $sql = "SELECT a.id, b.nama, c.nama_menu, d.nama_menu AS sub_menu_1, e.nama_menu AS sub_menu_2, a.judul, 
      a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1, e.slug_menu AS slug_sub_menu_2, 
      a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE c.id = ? order by a.id desc";
    $query = $this->db->query($sql, array($kategori));
    return $query->result();
  }

  function get_content_by_page_type($type){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1, a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
    e.slug_menu AS slug_sub_menu_2, c.page_type, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE c.page_type = ? ORDER BY a.tanggal DESC LIMIT 5";
    $query = $this->db->query($sql, array($type));
    return $query->result();
  }

  function get_content_by_popular(){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1,
      a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
      e.slug_menu AS slug_sub_menu_2, c.page_type, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE c.page_type = '1' and a.publish = '1' ORDER BY a.dibaca DESC LIMIT 5";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_content_by_user($user){
    $sql = "SELECT a.id, b.nama, a.menu_id, c.nama_menu, a.sub_menu1_id, d.nama_menu AS sub_menu_1, a.sub_menu2_id, e.nama_menu AS sub_menu_2, a.judul, a.slug, c.slug_menu, d.slug_menu AS slug_sub_menu_1,
    e.slug_menu AS slug_sub_menu_2, c.page_type, c.page_url, d.page_url AS page_url_sub_1, e.page_url AS page_url_sub_2, a.tanggal, a.deskripsi, a.foto, a.video, a.keterangan_foto, a.dibaca, a.publish
    FROM t_konten a 
      INNER JOIN t_users b ON a.user_id=b.id
      INNER JOIN t_menu c ON a.menu_id=c.id
      LEFT JOIN t_menu d ON a.sub_menu1_id=d.id
      LEFT JOIN t_menu e ON a.sub_menu2_id=e.id
    WHERE b.id = ? order by a.id desc";
    $query = $this->db->query($sql, array($user));
    return $query->result();
  }

  function content_dibaca($id){
    $query = $this->db->where('id', $id)->get("t_konten")->row();
    $jml = (int) $query->dibaca + 1;

    if($query->publish=="1"){
      $this->db->where('id', $this->db->escape_like_str($id));
      return $this->db->update("t_konten", ['dibaca' => $jml]) ? true : false;
    }else{
      return false;
    }
  }

  function content_insert($user, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $foto, $video, $keterangan_foto, $status){
    $tanggal = ($tanggal==null)? date("Y-m-d H:i:s"): $tanggal;
    $sql = "insert into t_konten (id, user_id, menu_id, sub_menu1_id, sub_menu2_id, judul, slug, tanggal, deskripsi, foto, video, keterangan_foto, publish) values (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($user, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $foto, $video, $keterangan_foto, $status));

    $sql = "select last_insert_id() as id";
    $query = $this->db->query($sql);
    return $query->row()->id;
  }

  function content_update_gambar($id, $gambar){
    $sql = "update t_konten set foto = ? where id = ?";
    $this->db->query($sql, array($gambar, $id));
  }

  function content_update_dibaca($id, $dibaca){
    $sql = "update t_konten set dibaca = ? where id = ?";
    $this->db->query($sql, array($dibaca, $id));
  }

  function content_update($id, $menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $video, $keterangan_foto, $status){
    $tanggal = ($tanggal==null)? date("Y-m-d H:i:s"): $tanggal;
    $sql = "update t_konten set menu_id = ?, sub_menu1_id = ?, sub_menu2_id = ?, judul = ?, slug = ?, tanggal = ?, deskripsi = ?, video = ?, keterangan_foto = ?, publish = ? where id = ?";
    $this->db->query($sql, array($menu, $sub_menu1, $sub_menu2, $judul, $slug, $tanggal, $deskripsi, $video, $keterangan_foto, $status, $id));
  }

  function content_delete($id){
    $sql = "delete from t_konten where id = ?";
    $this->db->query($sql, array($id));
  }
  /* ------------ Content End ------------- */

  /* ------------ kontak Start --------------- */
  function get_kontak(){
    $sql = "select * from t_kontak";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_kontak_by_id($id){
    $sql = "select * from t_kontak where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result();
  }

  function get_kontak_by_ip_address($ip){
    $sql = "select * from t_kontak where ip_address = ?";
    $query = $this->db->query($sql, array($ip));
    return $query->result();
  }

  function kontak_insert($ip, $nama, $email, $website, $pesan){
    $sql = "insert into t_kontak (id, ip_address, nama, email, website, pesan) values (null, ?, ?, ?, ?, ?)";
    $query = $this->db->query($sql, array($ip, $nama, $email, $website, $pesan));
    return $query->result();
  }

  function kontak_update($id, $nama, $email, $website, $pesan){
    $sql = "update t_kontak set nama = ?, email = ?, website = ?, pesan = ? where id = ?";
    $query = $this->db->query($sql, array($nama, $email, $website, $pesan, $id));
    return $query->result();
  }

  function kontak_delete($id){
    $sql = "delete fron t_kontak where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result();
  }
  /* ------------ kontak End ----------------- */

  /* ------------ Tag Start --------------- */
  function get_tag(){
    $sql = "select * from t_tag";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_tag_by_id($id){
    $sql = "select * from t_tag where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function get_tag_by_tag($tag){
    $sql = "select * from t_tag where tag = ?";
    $query = $this->db->query($sql, array($tag));
    return $query->row();
  }

  function tag_insert($tag){
    $sql = "insert into t_tag (id, tag) values (null, ?)";
    $query = $this->db->query($sql, array($tag));

    $sql = "select last_insert_id() as id";
    $query = $this->db->query($sql);
    return $query->row()->id;
  }

  function tag_update($id, $tag){
    $sql = "update t_tag set tag = ? where id = ?";
    $query = $this->db->query($sql, array($tag, $id));
  }

  function tag_delete($id){
    $sql = "delete from t_tag where id = ?)";
    $query = $this->db->query($sql, array($id));
  }
  /* ------------ Tag End --------------- */

  /* ------------ Relasi Tag Start --------------- */
  function get_tag_relasi(){
    $sql = "SELECT a.id, b.tag, c.judul, e.nama_menu, f.nama_menu AS sub_menu_1, g.nama_menu AS sub_menu_2 
    FROM t_tag_relasi a 
      INNER JOIN t_tag b ON a.tag_id=b.id 
      INNER JOIN t_konten c ON a.konten_id=c.id
      INNER JOIN t_users d ON c.user_id=d.id
      INNER JOIN t_menu e ON c.menu_id=e.id
      LEFT JOIN t_menu f ON c.sub_menu1_id=f.id
      LEFT JOIN t_menu g ON c.sub_menu2_id=g.id";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_tag_relasi_count_by_tag($tag){
    $sql = "SELECT COUNT(*) AS total 
    FROM t_tag_relasi a
      INNER JOIN t_tag b ON a.tag_id=b.id 
      INNER JOIN t_konten c ON a.konten_id=c.id
    WHERE b.tag = ?";
    $query = $this->db->query($sql, array($tag));
    return $query->row()->total;
  }

  function get_tag_relasi_by_tag($tag, $start, $limit){
    $sql = "SELECT a.id, b.tag, a.konten_id, c.judul, c.slug, c.tanggal, c.deskripsi, c.foto,
      c.video, c.keterangan_foto, c.dibaca, d.nama, c.menu_id, e.nama_menu, e.slug_menu, c.sub_menu1_id,
      f.nama_menu AS sub_menu_1, f.slug_menu AS slug_sub_menu_1, c.sub_menu2_id, 
      g.nama_menu AS sub_menu_2, g.slug_menu AS slug_sub_menu_2, e.page_type 
    FROM t_tag_relasi a
      INNER JOIN t_tag b ON a.tag_id=b.id 
      INNER JOIN t_konten c ON a.konten_id=c.id
      INNER JOIN t_users d ON c.user_id=d.id
      INNER JOIN t_menu e ON c.menu_id=e.id
      LEFT JOIN t_menu f ON c.sub_menu1_id=f.id
      LEFT JOIN t_menu g ON c.sub_menu2_id=g.id 
    WHERE b.tag = ? LIMIT ?, ?";
    $query = $this->db->query($sql, array($tag, $start, $limit));
    return $query->result();
  }

  function get_tag_popular(){
    $sql = "SELECT COUNT(a.tag_id) as jml, b.tag FROM t_tag_relasi a INNER JOIN t_tag b ON a.tag_id=b.id GROUP BY b.tag ORDER BY COUNT(a.tag_id) DESC LIMIT 10";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_tag_relasi_by_konten_id($konten_id){
    $sql = "SELECT b.tag FROM t_tag_relasi a INNER JOIN t_tag b ON a.tag_id=b.id WHERE a.konten_id = ?";
    $query = $this->db->query($sql, array($konten_id));
    return $query->result();
  }

  function get_tag_relasi_by_tag_id_and_konten_id($tag_id, $konten_id){
    $sql = "select * from t_tag_relasi where tag_id = ? and konten_id = ?";
    $query = $this->db->query($sql, array($tag_id, $konten_id));
    return $query->result();
  }

  function tag_relasi_insert($tag, $konten){
    $sql = "insert into t_tag_relasi (id, tag_id, konten_id) values (null, ?, ?)";
    $query = $this->db->query($sql, array($tag, $konten));
  }

  function tag_relasi_update($id, $tag, $konten){
    $sql = "update t_tag_relasi set tag_id = ?, konten = ? where id = ?";
    $query = $this->db->query($sql, array($tag, $konten, $id));
  }

  function tag_relasi_delete($id){
    $sql = "delete from t_tag_relasi where id = ?";
    $query = $this->db->query($sql, array($id));
  }

  function tag_relasi_delete_by_konten_id($konten_id){
    $sql = "delete from t_tag_relasi where konten_id = ?";
    $query = $this->db->query($sql, array($konten_id));
  }

  function tag_relasi_delete_by_tag_id($tag_id){
    $sql = "delete from t_tag_relasi where tag_id = ?";
    $query = $this->db->query($sql, array($tag_id));
  }

  function tag_relasi_delete_by_tag_id_and_konten_id($tag_id, $konten_id){
    $sql = "delete from t_tag_relasi where tag_id = ? and konten_id = ?";
    $query = $this->db->query($sql, array($tag_id, $konten_id));
  }
  /* ------------ Tag End --------------- */

  /* ------------ Slider Start --------------- */
  function get_slider(){
    $sql = "select * from t_slider";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_slider_by_id($id){
    $sql = "select * from t_slider where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function get_slider_by_status($status){
    $sql = "select * from t_slider where publish = ?";
    $query = $this->db->query($sql, array($status));
    return $query->result();
  }

  function slider_insert($judul, $keterangan, $foto, $video, $tanggal, $status){
    $sql = "insert into t_slider (id, judul, keterangan, foto, video, tanggal, publish) values (null, ?, ?, ?, ?, ?, ?)";
    $query = $this->db->query($sql, array($judul, $keterangan, $foto, $video, $tanggal, $status));
  }

  function slider_update_gambar($id, $foto){
    $sql = "update t_slider set foto = ? where id = ?";
    $query = $this->db->query($sql, array($foto, $id));
  }

  function slider_update_video($id, $video){
    $sql = "update t_slider set video = ? where id = ?";
    $query = $this->db->query($sql, array($video, $id));
  }

  function slider_update($id, $judul, $keterangan, $status){
    $sql = "update t_slider set judul = ?, keterangan = ?, publish = ? where id = ?";
    $query = $this->db->query($sql, array($judul, $keterangan, $status, $id));
  }

  function slider_delete($id){
    $sql = "delete from t_slider where id = ?";
    $query = $this->db->query($sql, array($id));
  }
  /* ------------ Slider End ----------------- */

  /* ------------ Banner Start ----------------- */
  function get_banner(){
    $sql = "select * from t_banner";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_banner_by_id($id){
    $sql = "select * from t_banner where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function get_banner_by_status($status){
    $sql = "select * from t_banner where publish = ?";
    $query = $this->db->query($sql, array($status));
    return $query->result();
  }
  
  function banner_insert($judul, $foto, $url, $tanggal, $status){
    $sql = "insert into t_banner (id, judul, foto, url, tanggal, publish) values (null, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($judul, $foto, $url, $tanggal, $status));
  }

  function banner_update_gambar($id, $foto){
    $sql = "update t_banner set foto = ? where id = ?";
    $query = $this->db->query($sql, array($foto, $id));
  }

  function banner_update($id, $judul, $foto, $url, $status){
    $sql = "update t_banner set judul = ?, foto = ?, url = ?, publish = ? where id = ?";
    $this->db->query($sql, array($judul, $foto, $url, $status, $id));
  }

  function banner_delete($id){
    $sql = "delete from t_banner where id = ?";
    $this->db->query($sql, array($id));
  }
  /* ------------ Banner End ------------------- */

  /* ------------ Setting Start ----------- */
  function get_setting(){
    $sql = "select * from t_setting order by id desc";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_setting_by_id($id){
    $sql = "select * from t_setting where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function setting_insert($data){
    return $this->db->insert('t_setting', $data)? true : false;
    //return $this->db->insert_id();
  }

  function setting_update_gambar($id, $foto){
    $sql = "update t_banner set logo = ? where id = ?";
    $query = $this->db->query($sql, array($foto, $id));
  }

  function setting_update($id, $data){
    return $this->db->update('t_setting', $data)? true : false;
  }

  function setting_delete($id){
    $sql = "delete from t_setting where id = ?";
    $this->db->query($sql, array($id));
  }
  /* ------------ Setting End ------------- */

  /* ------------ Comment Start --------------- */
  function get_comment(){
    $sql = "select * from t_komentar";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_comment_by_content($konten_id){
    $sql = "select * from t_komentar where konten_id = ?";
    $query = $this->db->query($sql, array($konten_id));
    return $query->result();
  }

  function get_comment_by_parent($parent){
    $sql = "select * from t_komentar where parent_id = ?";
    $query = $this->db->query($sql, array($parent));
    return $query->result();
  }

  function comment_insert($konten_id, $parent, $nama, $email, $website, $komentar){
    $sql = "insert into t_komentar (id, konten_id, parent_id, nama, email, website, komentar) values (null, ?, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($konten_id, $parent, $nama, $email, $website, $komentar));
  }

  function comment_update($id, $nama, $email, $website, $komentar){
    $sql = "update t_komentar set nama = ?, email = ?, website = ?, komentar = ?) where id = ?";
    $this->db->query($sql, array($nama, $email, $website, $komentar, $id));
  }

  function comment_delete($id){
    $sql = "delete from t_komentar where id = ?";
    $this->db->query($sql, array($id));
  }
  /* ------------ Comment End ----------------- */

  /* ------------ Log Start --------------- */
  function get_log(){
    $sql = "select a.id, b.nama, c.judul, a.ip_address, a.keterangan, a.tanggal from t_log a left join t_users b on a.user_id=b.id left join t_konten c on a.konten_id=c.id order by a.id desc";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_log_by_id($id){
    $sql = "select a.id, b.nama, c.judul, a.ip_address, a.keterangan, a.tanggal from t_log a left join t_users b on a.user_id=b.id left join t_konten c on a.konten_id=c.id where a.id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function get_log_by_user($user_id){
    $sql = "select a.id, b.nama, c.judul, a.ip_address, a.keterangan, a.tanggal from t_log a left join t_users b on a.user_id=b.id left join t_konten c on a.konten_id=c.id where c.user_id = ? order by a.id desc";
    $query = $this->db->query($sql, array($user_id));
    return $query->result();
  }

  function get_log_by_ip($ip){
    $sql = "select a.id, b.nama, c.judul, a.ip_address, a.keterangan, a.tanggal from t_log a left join t_users b on a.user_id=b.id left join t_konten c on a.konten_id=c.id where a.ip_address = ? order by a.id desc";
    $query = $this->db->query($sql, array($ip));
    return $query->result();
  }

  function get_log_by_konten($konten_id){
    $sql = "select a.id, b.nama, c.judul, a.ip_address, a.keterangan, a.tanggal from t_log a left join t_users b on a.user_id=b.id left join t_konten c on a.konten_id=c.id where a.konten_id = ? order by a.id desc";
    $query = $this->db->query($sql, array($konten_id));
    return $query->result();
  }

  function log_insert($user_id, $konten_id, $ip_address, $keterangan){
    $sql = "insert into t_log (id, user_id, konten_id, ip_address, keterangan) values (null, ?, ?, ?, ?)";
    $query = $this->db->query($sql, array($user_id, $konten_id, $ip_address, $keterangan));
  }

  function log_update($id, $user_id, $konten_id, $ip_address, $keterangan){
    $sql = "update t_log user_id = ?, konten_id = ?, ip_address = ?, keterangan = ? where id = ?";
    $query = $this->db->query($sql, array($user_id, $konten_id, $ip_address, $keterangan, $id));
  }

  function log_delete($id){
    $sql = "delete from t_log where id = ?";
    $query = $this->db->query($sql, array($id));
  }
  /* ------------ Log End ----------------- */
}