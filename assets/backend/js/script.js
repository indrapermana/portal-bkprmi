$(document).ready(function(){

  $('#datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
  });

  var p1 = $("#img-preview");
	$("#upload-file-img").change(function(){
		p1.fadeOut();
	
		var oFReader = new FileReader();
		oFReader.readAsDataURL(document.getElementById("upload-file-img").files[0]);
	
		oFReader.onload = function (oFREvent) {
      // console.log(oFREvent.target.result);
      console.log(p1.attr('src'));
			p1.attr('src', oFREvent.target.result).fadeIn();
		};
  });
  
  var p2 = $("#video-preview");
  var p3 = $("#video-preview-foto");
	$("#upload-file-video").change(function(){
		p2.fadeOut();
	
		var oFReaderV = new FileReader();
		oFReaderV.readAsDataURL(document.getElementById("upload-file-video").files[0]);
	
		oFReaderV.onload = function (oFREventV) {
      console.log(oFREventV.target.result);
      p2.attr('src', oFREventV.target.result).fadeIn();
      p3.attr('poster', p1.attr('src')).fadeIn();
		};
	});

  var page_type = $('#page_type').val();
  if(page_type==="2" || page_type==="3"){
    $("#form-video").hide();
    $("#form-foto").hide();
    $("#form-keterangan-foto").hide();
    $("#form-tanggal").hide();
  }

  $('#form-sub_kategori1').hide();
  $('#form-sub_kategori2').hide();
  $('#kategori').change(function(){
    kategori = $(this).val();
    
    // get page_type
    var page_type = kategori.split("|")[1];
    $('#page_type').val(page_type);
    if(page_type==="2" || page_type==="3"){
      $("#form-video").hide();
      $("#form-foto").hide();
      $("#form-keterangan-foto").hide();
      $("#form-tanggal").hide();
    }
    if(page_type==="1"){
      $("#form-video").show();
      $("#form-foto").show();
      $("#form-keterangan-foto").show();
      $("#form-tanggal").show();
    }

    // get sub menu / sub kategori
    $('#sub_kategori1').html('<option value="">Sub Kategori</option>');
    $.get(base_url+"menu/getsubmenu/"+kategori, function(r){
      r = JSON.parse(r);
      // console.log(r.length);
      
      if(r.length > 0){
        $('#form-sub_kategori1').show();
        $.each(r, function(index, data) {
          $('#sub_kategori1').append('<option value="'+data.id+'">'+data.nama_menu+'</option>');
        });
      }
    }); 
  });

  $('#sub_kategori1').change(function(){
    sub_kategori = $(this).val();
    // get sub menu / sub kategori
    $('#sub_kategori2').html('<option value="">Sub Kategori</option>');
    $.get(base_url+"menu/getsubmenu/"+sub_kategori, function(r){
      console.log(r);
      r = JSON.parse(r);
      if(r.length > 0){
        $('#form-sub_kategori2').show();
        $.each(r, function(index, data) {
          $('#sub_kategori2').append('<option value="'+data.id+'">'+data.nama_menu+'</option>');
        });
      }
    }); 
  });
});